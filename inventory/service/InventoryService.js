angular.module('inventory').service('InventoryService', function($resource, $cookies, config, $log) {

    $log.debug("inventory services");

    var getInventoryAssign = $resource(config.baseUrl + '/inventoryassignment/assign/:min/:max', { min: '@min', max: '@max' }, {
        'update': { method: 'PUT' }
    });

    var getInventoryadd = $resource(config.baseUrl + '/inventoryassignment/assign', {}, {
        'update': { method: 'PUT' }
    });

    var inventoryassignment = $resource(config.baseUrl + '/inventoryassignment/:task', { task: '@task' }, {
        'update': { method: 'PUT' }
    });

    var StudentList = $resource(config.baseUrl + '/inventoryassignment/studentList', {},{

    });
    
    var getInventoryStudent = $resource(config.baseUrl + '/inventoryassignment/student/:min/:max',{ min: '@min', max: '@max'},{
        'update': { method: 'PUT'}
    })

    var addinventoryStudentList = $resource(config.baseUrl + '/inventoryassignment/student', {},{

    })

    var inventoryassignmentstudent = $resource(config.baseUrl + '/inventoryassignment/student/:task', { task: '@task'},{
        'update':{ method: 'PUT'}
    })

  
   this.addinventoryStudent = function(data){
    console.log("student data is saving");
    return addinventoryStudentList.save(data).$promise;
   }

    this.getInvStudent =function(params){
        console.log("inventory student List");
        return getInventoryStudent.get(params).$promise;
    }

    this.getStudentsList =function(data){
        console.log("inventoryassignment StudentList",data);
        return StudentList.save(data).$promise;
    }

    this.editinventorystudent =function(data){
        console.log("edit student data",data);
        $log.debug(data);
        return inventoryassignmentstudent.update({ task:"edit"},data).$promise;
    }

    this.deleteStudent = function(data){
        $log.debug('delete student services',data);
        return inventoryassignmentstudent.update({ task:"delete"},data).$promise;
    }

   this.get = function(params) {
        console.log("inventory services");
        return getInventoryAssign.get(params).$promise;
    }

    this.addinventoryassignment = function(data) {
        $log.debug("addinventoryassignment services request data : ", data);
        return getInventoryadd.save(data).$promise;
    }

    this.editinventoryassignment = function(data) {
        $log.debug(data);
        return inventoryassignment.update({ task: "edit" }, data).$promise;
    }

    this.delete = function(data) {
        $log.debug("delete function");
        $log.debug(data);
        return inventoryassignment.update({ task: "delete" }, data).$promise;
    }

    var getInventory1 = $resource(config.baseUrl + '/inventory/List', {}, {
        'update': { method: 'PUT' }
    });

    this.getInventory1 = function() {
        $log.debug("getting InventoryList");
        return getInventory1.get().$promise;
    }

    var InventoryStudent = $resource(config.baseUrl + '/inventoryassignment/List', {},{

    })
  
       this.getStudent =function(){
        $log.debug("getting inventory Student List");
        return InventoryStudent.get().$promise;
       }
});
