angular.module('inventory').controller('InventoryCtrl', function($scope, $cookies, registration, template, User, HierarchyService, $filter, $stateParams, $location, $log, InventoryService, $state, $rootScope) {

    var inventoryTree = null;
    $scope.empId = $stateParams.empId;
    $scope.employee = null;
    $scope.people = [];
    $scope.childOfChildList = {};
    $scope.inventories = {};
    // $rootScope.InventoryInfo1 = [];
    $scope.template = template;
    $scope.inventoryAssignments = {};
    $scope.inventory = {};
    $scope.totalInventoryassign = 0;
    $scope.inventoryassignPerPage = 5;
    $scope.inventoryassignStudentPage = 5;
    $scope.pageIndex = 1;
    $scope.currentPage = 1;
    $scope.pageSize = 5;
    $scope.isPageChanged = true;
    $scope.maxVal = 5;
    $scope.minVal = 5;
    $scope.maxSize = 5;
    $scope.InventoryInfoStudent = {};
    $scope.InventoryStduentList = [];
    $rootScope.InventoryInfoAssign = [];
    $scope.InventoryInfoStudent = [];
    $scope.pagination = {
        current: 1
    };

    $scope.inventoryassignPageChangestudent = function(newPage) {
        console.log("newPage : ", newPage);
        $scope.isPageChanged = true;
        $scope.pageIndex = newPage;
        console.log("page number ", newPage);
        getInventoryAssign(newPage);
    };

    function getInventoryAssign(pageNumber) {
        var minVal = ($scope.inventoryassignPerPage * (pageNumber - 1));
        var maxVal = pageNumber * $scope.inventoryassignPerPage;
        console.log("Get function called from controller")
        InventoryService.get({ "min": minVal, "max": maxVal }).then(function(data) {
                console.log("i am gettig data", data);
                $scope.InventoryInfoAssign = data.data.inventoryList.inventoryList;
                $scope.Permission = $cookies.getObject("modulePermission");
                $scope.totalinventory = data.data.inventoryList.totalinventoryassign;
                console.log("Praveen Am", $scope.InventoryInfoAssign);
            },
            function(err) {
                console.log(err);
            });
    }
    getInventoryAssign(1);

    $scope.size = function(data) {
        $scope.inventoryassignPerPage = data;
    }

    $scope.inventoryassignPageChange = function(newPage) {
        console.log("newPage : ", newPage);
        $scope.isPageChanged = true;
        $scope.pageIndex = newPage;
        console.log("page number ", newPage);
        getInventoryAssignStudent(newPage);
    };

    function getInventoryAssignStudent(pageNumber) {
        var minVal = ($scope.inventoryassignStudentPage * (pageNumber - 1));
        var maxVal = pageNumber * $scope.inventoryassignStudentPage;
        console.log("Student function called")
        InventoryService.getInvStudent({ "min": minVal, "max": maxVal }).then(function(data) {
                console.log("i am getting student data", data);
                $scope.InventoryInfoStudent = data.data.inventoryStudentList.inventoryStudentList;
                $scope.Permission = $cookies.getObject("modulePermission");
                $scope.totalinventoryStudent = data.data.inventoryStudentList.totalinventoryassignstudent;
                console.log("Praveen A", $scope.InventoryInfoStudent);
            },
            function(err) {
                console.log(err);
            });
    }

    getInventoryAssignStudent(1);

    $scope.User =User.userInfo.profile.role;
    console.log("whos this", $scope.User);
     if($scope.User == "STUDENT")
     {
        $scope.student=true;
        console.log('----------------');
     }
    
    $scope.addinventoryAssignments = function() {
        $scope.inventoryAssignments = {};
        $location.path('/inventoryassignments/add');
    }

    $scope.editInventoryAssignments = function(inven) {
        $scope.inventoryAssignments = angular.extend({}, inven);
        $log.debug(inven);
        $location.path('/inventoryassignments/edit');
    }

    $scope.addinventoryAssignmentsStudent = function() {
        $scope.inventoryassignments = {};
        $location.path('inventoryassignments/addstudent')
    }


    $scope.editinventoryAssignmentStudent = function(InventoryAssignmentsStduent) {
        $scope.inventoryAssignments = {};
        $scope.InventoryAssignmentsStduent = angular.extend({}, InventoryAssignmentsStduent);
        $location.path('/inventoryassignments/editStudent');
    }

    $scope.inventory = {};
    $scope.addinventory = function(event, inventoryAssignments) {
        $log.debug("inventoryAssignments : ", inventoryAssignments);
        console.log("vishal", inventoryAssignments)
        $scope.inventoryAssignments.leftQuantity = $scope.inventoryAssignments.quantity;
        console.log('praveen', $scope.inventoryAssignments);
        var assignments = {};
        var assignments = angular.extend($scope.assign, $scope.inventoryAssignments);
        console.log('assignments', assignments);
        if (inventoryAssignments.$valid) {
            InventoryService.addinventoryassignment(assignments).then(function(data) {
                $log.debug("after the save function");
                $rootScope.InventoryInfoAssign.push($scope.inventoryAssignments);
                $state.go("inventoryAssignments.list");
            }, function(err) {
                $scope.addFormSubmitError = err.data.message;
                $log.debug('Error while adding Inventory :', err);
            });
        } else {
            angular.forEach(inventoryAssignments.$error.required, function(field) {
                field.$setDirty();
            });
        }
    }

    $scope.addinventoryStudent = function(event, inventoryAssignments) {
        $log.debug("inventoryAssignments :", inventoryAssignments);
        var assignmentsStudent = {};
        var assignmentsStudent = angular.extend($scope.inventoryassignments, $scope.InventoryAssignmentsStduent);
        console.log('assignmentsStudent', assignmentsStudent);
        console.log("Praveen", assignmentsStudent)
        if (inventoryAssignments.$valid) {
            InventoryService.addinventoryStudent(assignmentsStudent).then(function(data) {
                $log.debug("after student save function");
                $scope.InventoryInfoStudent.push($scope.assignmentsStudent);
                $state.go("inventoryAssignments.student");
            }, function(err) {
                $scope.addFormSubmitError = err.data.message;
                $log.debug('Error while adding InventortStudent :', err);
            });
        } else {
            angular.forEach(inventoryAssignments.$error.required, function(field) {
                field.$setDirty();
            });
        }
    }

    $scope.editinventory = function(event, inventoryAssignments) {
        $log.debug(inventoryAssignments);
        if (inventoryAssignments.$valid) {

            InventoryService.editinventoryassignment($scope.inventoryAssignments).then(function(data) {
                $log.debug("after edit function");
                for (var i = 0; i < $rootScope.InventoryInfoAssign.length; i++) {
                    if ($rootScope.InventoryInfoAssign[i].entryTime === $scope.inventoryAssignments.entryTime) {
                        $rootScope.InventoryInfoAssign.splice(i, 1);
                        $rootScope.InventoryInfoAssign.push($scope.inventoryAssignments);
                    }
                }
                $state.go("inventoryAssignments.list");
            });
        } else {
            angular.forEach(inventoryAssignments.$error.required, function(field) {
                field.$setDirty();
            });
        }
    }

    $scope.setCurrentInventory = function(Inventory) {
        $log.debug(Inventory);
        $scope.currentInventory = Inventory;
    }

    $scope.delete = function() {

        InventoryService.delete($scope.currentInventory).then(function(data) {
            $log.debug("response data is : ", data);
            for (var i = 0; i < $rootScope.InventoryInfoAssign.length; i++) {
                if ($rootScope.InventoryInfoAssign[i].entryTime === $scope.currentInventory.entryTime) {
                    $rootScope.InventoryInfoAssign.splice(i, 1);
                }
            }
            $state.go("inventoryAssignments.list");
            $scope.currentInventory = null;
            $('#confirmDelete').modal('hide');

        });
    }


    $scope.editinventoryAssgnStudent = function(event, inventoryassignmentsStudent) {
        $log.debug(inventoryassignmentsStudent);

        if (inventoryassignmentsStudent.$valid) {
            console.log('EDIT DATA',$scope.InventoryAssignmentsStduent)
            InventoryService.editinventorystudent($scope.InventoryAssignmentsStduent).then(function(data) {
                $log.debug("after edit function student");
                for (var i = 0; i < $scope.InventoryInfoStudent.length; i++) {
                    if ($scope.InventoryInfoStudent[i].entryTime === $scope.currentInventoryStudent.entryTime) {
                        $scope.InventoryInfoStudent.splice(i, 1);
                    }
                }
                $state.go("inventoryAssignments.student");
            });
        } else {
            angular.forEach(inventoryassignmentsStudent.$error.required, function(field) {
                field.$setDirty();
            });
        }
    }


    $scope.setCurrentInventoryStudent = function(InventoryAssignments) {
        console.log("currentInventoryStudent",InventoryAssignments)
        $log.debug(InventoryAssignments);
        $scope.currentInventoryStudent = InventoryAssignments;
    }

    $scope.deleteAssignStudent = function() {

        InventoryService.deleteStudent($scope.currentInventoryStudent).then(function(data) {
            $log.debug("respond data is :", data);
            for (var i = 0; i < $scope.InventoryInfoStudent.length; i++) {
                if ($scope.InventoryInfoStudent[i].entryTime === $scope.currentInventoryStudent.entryTime) {
                    $scope.InventoryInfoStudent.splice(i, 1);
                }
            }
            $state.go("inventoryAssignments.student");
            $scope.currentInventoryStudent = null;
            /*  $('#confirmDelete').modal('hide');*/
        });

    }

    InventoryService.getInventory1().then(function(data) {
        console.log("data", data);
        $scope.InventoryList = data.inventoryList;
    });

    InventoryService.getStudent().then(function(data) {
        console.log("student data", data);
        $scope.InventoryStudentList = data.inventoryStudentList;
    })


    $scope.getInventoryByHierarchy = function(hierarchy) {
        console.log("selected hierarchy is : ", hierarchy);
        $scope.chosenHierarchy = hierarchy.hid;
        $location.path('/inventoryassignments/view');
    }


    function getHierarchys() {
        HierarchyService.get().then(function(data) {
            $scope.HierarchyInfo = data.hierarchyList;
            console.log(data)
            console.log($scope.HierarchyInfo)
        }, function(err) {
            console.log(err);
        });
    }
    getHierarchys()



    $scope.getStaffs = function() {
        console.log('teacher', User.userInfo.profile.hierarchy)

        registration.getStaffList(User.userInfo.profile.hierarchy).then(function(data) {
            $log.debug("getStaffList success block : ", data);
            $scope.staffList = data.staffList;
            console.log("staffList:", $scope.staffList);
        }, function(err) {
            $log.debug(err);
        });
    }

    $scope.assign = {};
    $scope.selectTeacher = function(teacher) {
        $log.debug('searchTeacher:', teacher);
        $scope.assign.teacherName = teacher.firstName;
        $scope.assign.smartId = teacher.teacherId;
        $scope.assign.section = teacher.section;
        $scope.assign.standard = teacher.standard;
    }

    $scope.getStudents = function() {
        console.log("student", User.userInfo.profile.hierarchy)

        InventoryService.getStudentsList(User.userInfo.profile.hierarchy).then(function(data) {
            $log.debug("getStudentsList success block : ", data);
            $scope.studentList = data.studentList;
            console.log("studentList:", $scope.studentList);
        }, function(err) {
            $log.debug(err);
        });
    }

    $scope.inventoryassignments = {};

    $scope.selectStudent = function(student) {
        $log.debug('searchStudent:', student);
        $scope.inventoryassignments.studentName = student.firstName;
        $scope.inventoryassignments.smartId = student.studentId;
        $scope.inventoryassignments.section = student.section;
        $scope.inventoryassignments.standard = student.standard;
    }

    
});
