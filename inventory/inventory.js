angular.module('inventory', ['ui.bootstrap', 'ui.utils', 'ui.router']);

angular.module('inventory').config(function($stateProvider) {

    $stateProvider.state('inventoryAssignments', {
            url: '/inventoryassignments',
            parent: 'app',
            templateUrl: 'inventory/partial/home.html',
            controller: 'InventoryCtrl',
            resolve: {
                template: function($q, User) {
                    var defer = $q.defer();
                    User.getUserInfo(function(user) {
                        var template = null;

                        if (user.profile.role.toLowerCase() === 'teacher') {

                            template = 'inventory/home.html';

                        } else if (user.profile.role.toLowerCase() === 'admin' || user.profile.role.toLowerCase() === 'finance') {
                            console.log('user is admin')
                            template = 'inventory/partial/landing.html';
                        } else if (user.profile.role.toLowerCase() === 'student') {
                            template = 'inventory/partial/studentinv-list.html';
                        } else {
                            template = 'inventory/partial/inventory.html';
                        }

                        defer.resolve(template);
                    });
                    return defer.promise;
                }
            }
        })
        .state('inventoryAssignments.view', {
            url: '/view',
            templateUrl: 'inventory/partial/inventory.html'
        })
        .state('inventoryAssignments.list', {
            url: '/list',
            templateUrl: 'inventory/partial/inventory-list.html'
        })
        .state('inventoryAssignments.add', {
            url: '/add',
            templateUrl: 'inventory/partial/add-inventory.html'
        })
        .state('inventoryAssignments.edit', {
            url: '/edit',
            templateUrl: 'inventory/partial/edit-inventory.html'
        })
        .state('inventoryAssignments.tree', {
            url: '/inventorytree',
            templateUrl: 'inventory/partial/inventoryStructure.html'
        })
        .state('inventoryAssignments.child', {
            url: '/child',
            templateUrl: 'inventory/partial/inventorychild.html'
        })
        .state('inventoryAssignments.student', {
            url: '/student',
            templateUrl: 'inventory/partial/studentinv-list.html'
        })
        .state('inventoryAssignments.addstudent', {
            url: '/addstudent',
            templateUrl: 'inventory/partial/add-student.html'
        })
        .state('inventoryAssignments.editStudent',{
            url:'/editStudent',
            templateUrl:'inventory/partial/edit-student.html'
        })
});
