angular.module('gsmart').service('Notice', function($log, $resource, $rootScope, config, User) {

    var notices = [];
    this.max = 0;

    $log.debug(User.id)
    var noticeAPI = $resource(config.baseUrl + '/notice/generic/Generic', {}, {
    });

    this.get = function() {
        $log.debug();
        return noticeAPI.get().$promise;
    }

    var noticeDashBoard = $resource(config.baseUrl + '/profile/profileCount');

    this.getCount = function() {
        return noticeDashBoard.get().$promise;
    }

        
    /* var noticeAPI2 = $resource(config.baseUrl + '/Notice/viewSpecificNotice/48750001', {}, {
         view: { method: 'POST', isArray: true }
     });

     this.getNotice = function (offset, noOfElements, callback) {
        if (notices[offset - 1]) {
            callback(notices.slice(offset-1, offset + noOfElements - 1));
        }
         else {
             noticeAPI.view({}, function (response) {
                 notices = response;
                 this.max = notices.length;
                 console.log('All notices :', notices);

                 console.log('max :', notices.length);

                 console.log('from', offset - 1, 'to', offset + noOfElements - 1);
                 console.log(notices.slice(offset-1, offset + noOfElements - 1));
                 callback(notices.slice(offset-1, offset + noOfElements - 1));
             }.bind(this));
         }
     }


     this.getNoticeSpl = function (offset, noOfElements, callback) {
         if (notices[offset - 1]) {
             console.log('not called')
             callback(notices.slice(offset-1, offset + noOfElements - 1));
         }
         else {
             console.log('called')
             noticeAPI2.view({}, function (response) {
                 notices = response;
                 this.max = notices.length;
                
                 callback(notices.slice(offset-1, offset + noOfElements - 1));
             }.bind(this));
         }
     }*/

    $rootScope.$on('loginChanged', function() {
        notices = [];
    })
});
