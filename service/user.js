angular.module('gsmart').service('User',function($log, $resource, $cookies, $rootScope, config) {
	this.userInfo = null;
	this.identified = false;
	this.id = null;
	var context = this;
	
	var authAPI = $resource(config.baseUrl + '/login/auth', {}, {
		authUser : {
			method : 'POST'
		}
	});

	this.authenticate = function (id, password, callback) {
		authAPI.authUser({smartId: id, password: password}, function (response) {
			if (response.data) {
				context.id = id;
				this.id=id;
				$cookies.put('userId', id);
				$cookies.put('password', password);
				context.userInfo = response.data;
				$log.debug(context.userInfo);
				callback(null, response);
			} else {
				callback(response)
			}
		});
	}
	
	this.getUserInfo = function (callback) {
		if (this.userInfo) {
			$log.debug(this.userInfo);
			callback(this.userInfo);
		}
		else {
			if ($cookies.get('userId') && $cookies.get('password')) {
				this.id = $cookies.get('userId');
				this.authenticate($cookies.get('userId'), $cookies.get('password'), function (err, data) {
				    $log.debug(data);
				    context.userInfo = data.data;
					callback(data);
				});
			}
			else {
				callback(null);
			}
		}
	};
	
	this.unidentify = function () {
		$cookies.remove('userId');
		$cookies.remove('password');
		this.userInfo = null;
		this.id = null;
		$rootScope.$broadcast('loginChanged');
	};
});