angular.module('gsmart').service('Dashboard', function($resource, $rootScope, config) {

    var attendance = $resource(config.baseUrl + '/dashboard/attendance/:date', { date: '@date' }, {});

    var inventory = $resource(config.baseUrl + '/dashboard/inventory/:academicYear', { academicYear: '@academicYear' }, {});

    var fee = $resource(config.baseUrl + '/dashboard/fee/:academicYear', { academicYear: '@academicYear' }, {});

    this.getAttendance = function(date) {
        return attendance.get({ date: date }).$promise;
    }

    this.getInventory = function(academicYear) {
        return inventory.get({ academicYear: academicYear }).$promise;
    }


    this.getFee = function(academicYear) {
        return fee.get({ academicYear: academicYear }).$promise;
    }



});
