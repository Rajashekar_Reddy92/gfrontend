angular.module('gsmart').service('SetPasswordService',function($resource, config) {
	var setPassword = $resource(config.baseUrl +'/password/:ts/', {ts: '@task'}, {
		'save' : { method: 'POST'}
	});

	this.save = function (data) {
		return setPassword.save(data).$promise;
	}
});