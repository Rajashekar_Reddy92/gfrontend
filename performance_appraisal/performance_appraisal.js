angular.module('performanceAppraisal', ['ui.bootstrap','ui.utils','ui.router']);

angular.module('performanceAppraisal').config(function($stateProvider) {

     $stateProvider.state('performance', {
        url: '/performance',
        parent:'app',
        /*templateUrl: 'performance_appraisal/partial/performance-appraisal.html',*/
       
        templateUrl: 'performance_appraisal/partial/home.html',
        controller: 'hierarchyDecidCtrl',
        resolve: {
                template: function($q, User) {
                    var defer = $q.defer();
                    User.getUserInfo(function(user) {
                        var template = null;
                         
                         if (user.profile.role.toLowerCase() ==='admin' || user.profile.role.toLowerCase() ==='director')
                        {
                            console.log('user is admin')
                            template = 'performance_appraisal/partial/landing.html';
                        }
                        else
                        {
                            template='performance_appraisal/partial/decider.html';
                        }

                        defer.resolve(template);
                    });
                    return defer.promise;
                }   
            }



    })
    .state('performance.decider', {
        url: '/decider',
       
        templateUrl: 'performance_appraisal/partial/performance-appraisal.html',
         controller: 'PerformanceAppraisalCtrl'
        
    })
     .state('performance.decider.myperformance', {
        url: '/myperformance',
         
        templateUrl: 'performance_appraisal/partial/myperformance.html'
       
        })

     .state('performance.decider.myteamperformance', {
        url: '/myteamperformance',
        
        templateUrl: 'performance_appraisal/partial/myteamperformance.html'

        })

     .state('performance.decider.setgoal', {
        url: '/setgoal',
         
        templateUrl: 'performance_appraisal/partial/setgoal.html'
        })
        
    .state('performance.decider.editgoal', {
        url: '/editgoal',
       
        templateUrl: 'performance_appraisal/partial/editgoal.html'
        })
     .state('performance.decider.appraisalmanager', {
        url: '/appraisalmanager',
         
        templateUrl: 'performance_appraisal/partial/appraisalmanager.html'
        });
        

});
