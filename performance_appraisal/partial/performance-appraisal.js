angular.module('performanceAppraisal').controller('PerformanceAppraisalCtrl', function($scope,$cookies, $stateParams, User, $state, performanceAppraisalService, $location) {

    console.log("PerformanceAppraisalCtrl called");

    $scope.appraisal = {};

    var year = new Date().getFullYear() + "-" + (new Date().getFullYear() + 1)

    $scope.year5 = [year];
    $scope.year1=year;

    var parentId = $stateParams.empId ? $stateParams.empId : User.id;



    $scope.showAcademicYearList = function() {
        var year = new Date().getFullYear();
        var currYear;
        $scope.yearList = [];
        for (var i = year; i > year - 10; i--) {
            currYear = i;
            $scope.yearList.push(currYear + "-" + (currYear + 1));

        }
    }

    $scope.showAcademicYearList();


    $scope.getMyPerformance = function(year) {
        console.log(year);

        performanceAppraisalService.myPerformance(year, parentId,$cookies.get("currenthierarchy")).then(function(data) {

            console.log(data);



            $scope.appraisalListinfo = data.appraisalList;
            $scope.performancerecordOwnListinfo = data.performancerecord.owncomments;
            $scope.performancerecordManagerListinfo = data.performancerecord.managercomments;

            console.log($scope.appraisalListinfo);
             console.log($scope.performancerecordOwnListinfo);
             console.log($scope.performancerecordManagerListinfo);
            $state.go('performance.decider.myperformance');
        }, function(err) {
            console.log(err);
        });
    }

    $scope.commentobj = {}
    $scope.addComment = function(comments) {
        console.log(year);
        console.log(comments);
        $scope.commentobj.comments = comments;
        $scope.commentobj.year = year;
        console.log($scope.commentobj);
        performanceAppraisalService.addComments($scope.commentobj).then(function(data) {
         $scope.performancerecordOwnListinfo.push($scope.commentobj);


                
            },
            function(err) {
                console.log(err);
            });


    }

    $scope.myPerformance = function() {
        console.log('uydfuiuy')
    $state.go('performance.decider.myperformance');

        $scope.getMyPerformance(year);
    }
     
    //myteam performance
    console.log(year);
    $scope.getMyTeamPerformance = function(year) {
        console.log(year);

        performanceAppraisalService.myTeamPerformance(year, parentId,$cookies.get("currenthierarchy")).then(function(data) {

            console.log(data);



            $scope.teamappraisalListinfo = data.teamAppraisalLiat;
            $scope.teamperformancerecordListinfo = data.performancerecord;

            console.log($scope.teamappraisalListinfo);
            console.log($scope.teamperformancerecordListinfo);




            $state.go('performance.decider.myteamperformance');
        }, function(err) {
            console.log(err);
        });
    }
    $scope.myTeamPerformance = function() {
        console.log('uyttidrysss')

        $scope.getMyTeamPerformance(year);
    }
 /*   myTeamPerformance ();*/
    $scope.setgoal = function() {
        $scope.appraisal = {};
        $state.go('performance.decider.setgoal');
    }

    $scope.editgoal = function(appraisalData) {
        $scope.appraisal = angular.extend({}, appraisalData);
       /* $scope.appraisal=appraisalData;*/
        $state.go('performance.decider.editgoal');
    }
   

    $scope.save = function(event, form) {
        event.preventDefault();

        if (form.$valid) {

            console.log("$scope.appraisal set goals : ", $scope.appraisal, $scope.appraisal.year.toString());
            $scope.appraisal.year = $scope.appraisal.year.toString();
            performanceAppraisalService.saveteamperformance($scope.appraisal).then(function(data) {

               
                $scope.teamappraisalListinfo.push($scope.appraisal);
                $state.go('performance.decider.myteamperformance');
            }, function(err) {
                $scope.addFormSubmitError = err.data.message;
                console.log('Error while adding goals :', err);
            })
        } else {
            angular.forEach(form.$error.required, function(field) {
                field.$setDirty();
            });
        }
    }


   

    $scope.update = function(event, form) {
        event.preventDefault();

        console.log("currentdata:",$scope.appraisal);
        console.log("data:",$scope.teamappraisalListinfo);

        if (form.$valid) {
            performanceAppraisalService.update($scope.appraisal).then(function(data) {
                  for (var i = 0; i < $scope.teamappraisalListinfo.length; i++) {
                    if ($scope.appraisal.entryTime === $scope.teamappraisalListinfo[i].entryTime) {
                        $scope.teamappraisalListinfo.splice(i, 1);
                      $scope.teamappraisalListinfo.push($scope.appraisal);
                      $state.go('performance.decider.myteamperformance');
                    }
                }
               
            });
        } else {
            angular.forEach(form.$error.required, function(field) {
                field.$setDirty();
            });
        }
    }
/*appraisalmanager*/

$scope.childId={}
 $scope.getteammemberperformance=function(smartId,reportingManagerId){
        console.log('sgbygygfyg')
        console.log(smartId);
        console.log(reportingManagerId);
      $scope.childId=smartId;
      $scope.parentId=reportingManagerId;
         performanceAppraisalService.teammemberperformance(year,smartId,reportingManagerId,$cookies.get("currenthierarchy")).then(function(data){
      
     $scope.teammemberscommentinfo=data.performancerecord.teammemberscomments;
     $scope.managercommentinfo=data.performancerecord.managercomments;
        console.log($scope.teammemberscommentinfo);
        console.log($scope.managercommentinfo);          


        $state.go('performance.decider.appraisalmanager');




        }, function(err) {
            console.log(err);
        });
    }




  

  $scope.commentobj1 = {};
$scope.addManagerComment=function(performanceRecord){
    console.log(year);
    console.log(performanceRecord);
    console.log($scope.childId);
        performanceRecord.smartId =$scope.childId;
        performanceRecord.reportingManagerID=$scope.parentId;
        performanceRecord.year = year;
        console.log(performanceRecord);
         performanceAppraisalService.addManagerComments(performanceRecord).then(function(data) {
        $scope.managercommentinfo.push(performanceRecord);


                $state.go('performance.decider.appraisalmanager');
            },
            function(err) {
                console.log(err);
            });


}


});