angular.module('performanceAppraisal').service('performanceAppraisalService',function($resource, config) {

var performance = $resource(config.baseUrl +'/performance/:year/:pid/:hid/', {year:'@year', pid:'@smartId',hid:'@hierarchy'},{
         'update': { method: 'PUT' }
     });
	this.myPerformance=function(year,smartId,hierarchy){
		console.log(smartId);
    console.log(hierarchy);
		console.log("myPerformance service data");
       console.log(year);

		return performance.get({year:year,pid:smartId,hid:hierarchy}).$promise;
	}
	
	var performance1 = $resource(config.baseUrl +'/performance/', {},{
          });
	this.addComments=function(data){
		console.log(data);
		return performance1.save(data).$promise;
	}

 
 var teamperformance = $resource(config.baseUrl +'/performance/record/:year/:pid/:hid/', {year:'@year', pid:'@smartId',hid:'@hierarchy'},{
         'update': { method: 'PUT' }
     });

	this.myTeamPerformance=function(year,smartId,hierarchy){
		console.log(smartId);
		console.log(year);
	return	teamperformance.get({year:year,pid:smartId,hid:hierarchy}).$promise;
	}

var saveperformance= $resource(config.baseUrl +'/performance/om/:ts/', {ts: '@ts'},{
	 'update': { method: 'PUT' }
          });

   this.saveteamperformance=function(data){
   	console.log(data);
   	return  saveperformance.save(data).$promise;

   }

       this.update = function (data) {
       	console.log(data);
        return saveperformance.update({ts: "edit"}, data).$promise;
    }
   
    

   /* appraisalmanager*/

 var teammemberperformance1 = $resource(config.baseUrl +'/performance/manager/:year/:pid/:Rid/:hid/', {year:'@year', pid:'@smartId',Rid:'@reportingManagerId',hid:'@hierarchy'},{
       
     });
    this.teammemberperformance=function(year,smartId,reportingManagerId,hierarchy){
    	
      var params={
        year:year,
        pid:smartId,
        Rid:reportingManagerId,
        hid:hierarchy

      }
    	 	
    return	teammemberperformance1.get(params).$promise;
 	
    }
      

	var performance5 = $resource(config.baseUrl +'/performance/addManagerComment', {},{
          });
   this.addManagerComments=function(data){
   	console.log(data);
   	return performance5.save(data).$promise; 
   }
});