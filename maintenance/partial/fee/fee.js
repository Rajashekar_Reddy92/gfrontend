angular.module('maintenance').controller('FeeMasterCtrl', function($log, $scope, $timeout, $cookies, $location, FeeMasterService) {

    $scope.fee = {};
    $scope.sendfile = {};
    $scope.myFile = {};

    $scope.totalFees = 0;
    $scope.feesPerPage = 10;
    $scope.pageIndex = 1;
    $scope.currentPage = 1;
    $scope.pageSize = 5;
    $scope.isPageChanged = false;
    $scope.maxVal = 10;
    $scope.minVal = 5;
    $scope.maxSize = 5;


    $scope.pagination = {
        current: 1
    };

    $scope.feePageChange = function(newPage) {

        if($scope.feesPerPage){
       console.log("newPage : ", newPage);

        $scope.isPageChanged = true;
        $scope.pageIndex = newPage;
        console.log("page number ", newPage);
        getFee(newPage);
    }
    };


    function getFee(pageNumber) {
        var minVal = ($scope.feesPerPage * (pageNumber - 1));
        var maxVal = pageNumber * $scope.feesPerPage;
        console.log("Get function called from controller")
        FeeMasterService.get({ "min": minVal, "max": maxVal, "hierarchy": $cookies.get("currenthierarchy") }).then(function(data) {
            console.log(data)
            $scope.FeeInfo = data.feeList.feeList;
            $scope.Permission = $cookies.getObject("modulePermission");
            $scope.totalfeelist = data.feeList.totalfeelist;
            console.log($scope.FeeInfo);

        }, function(err) {
            $log.debug(err);
        });
    }

    getFee(1);

    $scope.size = function(data){
        $scope.feesPerPage= data;
    }

    $scope.addfee = function() {
        $scope.fee = {};
        $location.path('/maintenance/feemaster/add');
    }

    $scope.upload = function() {
        $scope.fee = {};
        $location.path('/maintenance/feemaster/upload');
    }


    $scope.download = function() {

        FeeMasterService.downloadFile(function(data) {

            $location.path('/maintenance/feemaster')
        })

    }

    /*    $scope.uploadFile = function() {

            console.log($scope.upload.file);

            $scope.sendfile.file=$scope.upload.file;

            console.log($scope.sendfile);


            FeeMasterService.upload($scope.sendfile, function(data) {
                    $scope.upload = {};
                    getFee();
                    $location.path('/maintenance/fee')
                })
        }*/


    $scope.uploadFile = function() {

        var file = $scope.myFile;
        $log.debug('file is ' + $scope.myFile);
        $log.debug(file);

        FeeMasterService.uploadFileToUrl(file);
    };





    $scope.editFee = function(fe) {
        $scope.fee = angular.extend({}, fe);
        $location.path('/maintenance/feemaster/edit');
    }


    $scope.save = function(event, form) {
        event.preventDefault();

        if (form.$valid) {
            console.log($scope.fee)
            var a = $scope.fee.miscellaneousFee;
            var b = $scope.fee.sportsFee;
            var c = $scope.fee.transportationFee;
            var d = $scope.fee.tuitionFee;
            var e = $scope.fee.idCardFee;

            $scope.fee.totalFee = a + b + c + d + e;



            $log.debug($scope.fee.totalFee);
            FeeMasterService.save($scope.fee, $cookies.get("currenthierarchy")).then(function(data) {
                if (data.status == 200) {
                    $log.debug(data);
                    $scope.FeeMasterFormSubmitMessage = data.message;
                    $scope.ShowFeeMasterFormMessage = true;
                    $timeout(function() {
                        $scope.ShowFeeMasterFormMessage = false;
                    }, 2000);


                    $scope.fee = {};
                    getFee($scope.pageIndex);
                    $location.path('/maintenance/feemaster')

                } else if (data.status == 400) {
                    $scope.addFeeMasterFormSubmitError = data.message;
                } else $scope.addFeeMasterFormSubmitError = data.message;

            }, function(err) {
                $scope.addFeeMasterFormSubmitError = err.data.message;
                $log.debug('Error while adding band :', err);
            })
        } else {
            angular.forEach(form.$error.required, function(field) {
                field.$setDirty();
            });
        }
    }


    $scope.update = function(event, form) {
        event.preventDefault();

        if (form.$valid) {
            var a = $scope.fee.miscellaneousFee;
            var b = $scope.fee.sportsFee;
            var c = $scope.fee.transportationFee;
            var d = $scope.fee.tuitionFee;
            var e = $scope.fee.idCardFee;

            $scope.fee.totalFee = a + b + c + d + e;
            $log.debug($scope.fee.totalFee);
            FeeMasterService.update($scope.fee).then(function(data) {
                if (data.status == 200) {
                    $log.debug(data);
                    $scope.FeeMasterFormSubmitMessage = data.message;
                    $scope.ShowFeeMasterFormMessage = true;
                    $timeout(function() {
                        $scope.ShowFeeMasterFormMessage = false;
                    }, 2000);

                    $scope.fee = {};
                    getFee($scope.pageIndex);
                    $location.path('/maintenance/feemaster')

                } else if (data.status == 400) {
                    $scope.editFeeMasterFormSubmitError = data.message;
                } else $scope.editFeeMasterFormSubmitError = data.message;

            }, function(err) {
                $log.debug('Error while updating fee :', err);
            })
        } else {
            angular.forEach(form.$error.required, function(field) {
                field.$setDirty();
            });
        }
    }

    $scope.delete = function() {

        FeeMasterService.delete($scope.currentFee).then(function(data) {
            /* for (var i = 0; i < $scope.FeeInfo.length; i++) {
                 if ($scope.FeeInfo[i].timeStamp == $scope.currentFee.timeStamp) {
                     $scope.FeeInfo.splice(i, 1);
                 }
             }*/
            $scope.FeeMasterFormSubmitMessage = data.message;
            $scope.ShowFeeMasterFormMessage = true;
            $timeout(function() {
                $scope.ShowFeeMasterFormMessage = false;
            }, 2000);

            getFee($scope.pageIndex);
            $location.path('/maintenance/feemaster')
            $scope.currentFee = null;
            // $('#confirmDelete').modal('hide');
        }, function(err) {
            $log.debug('Error while deleting record :', err);
        })
    }

    $scope.setCurrentFee = function(Fee) {
        $scope.currentFee = Fee;
    }
});
