angular.module('gsmart').controller('AssignCtrl', function($scope, User, $timeout, HierarchyService, $location, $log, assign, $cookies, registration) {

    $scope.assign = {};
    $scope.totalAssign = 0;
    $scope.assignPerPage = 7;
    $scope.pageIndex = 1;
    $scope.currentPage = 1;
    $scope.pageSize = 5;
    $scope.isPageChanged = false;
    $scope.maxVal = 10;
    $scope.minVal = 5;
    $scope.maxSize = 5;

    console.log('assign controller')
        /*Page change on click on the page number*/
    $scope.pagination = {
        current: 1
    };
    $scope.assignPageChange = function(newPage) {
        if($scope.assignPerPage){
        console.log("newPage : ", newPage);
        $scope.isPageChanged = true;
        $scope.pageIndex = newPage;
        console.log("page number ", newPage);
        getAsssigns(newPage);
    }
    };

    function getAsssigns(pageNumber) {
        var minVal = ($scope.assignPerPage * (pageNumber - 1));
        var maxVal = pageNumber * $scope.assignPerPage;
        console.log("Get function called from controller")
        assign.get({ "min": minVal, "max": maxVal, "hierarchy": $cookies.get("currenthierarchy") }).then(function(data) {

            console.log(data)
            $scope.AssisgnInfo = data.assignList.assignList;
            $scope.Permission = $cookies.getObject("modulePermission");
            console.log('modulePermission1234s', $scope.Permission)
            $scope.totalassign = data.assignList.totalassign;
            console.log($scope.AssisgnInfo);
        }, function(err) {

            $log.debug(err);
        });
    }

    getAsssigns(1);

    $scope.size = function(data){
        $scope.assignPerPage = data;
    }

    $scope.search = function(data) {
        if(data){
        $scope.assignPerPage = $scope.totalassign;
    }else{
        $scope.assignPerPage = 7;
    }
    }

    /* function getAsssignsList() {
     assign.getAsssignList($cookies.get("currenthierarchy")).then(function(data) {

         console.log(data)
         $scope.AssisgnListInfo = data.assignList;
         console.log($scope.AssisgnListInfo);
     }, function(err) {

         $log.debug(err);
     });
 }
 getAsssignsList();
*/
    /*
        HierarchyService.getAllHierarchys().then(function(data) {
            $log.debug("institutionsList : ", data);
            $scope.institutionsList = data.hierarchyList;
            console.log('hierarchyList', $scope.institutionsList)
            $scope.Permissions = data.modulePermission;
        }, function(err) {
            $log.debug(err);
        });*/

    $scope.addassign = function() {
        HierarchyService.get().then(function(data) {
            $log.debug("institutionsList : ", data);
            /*$scope.institutionsList = data.hierarchyList;
            $scope.Permission = data.modulePermission;*/
        }, function(err) {
            $log.debug(err);
        });

        $scope.assign = {};
        $scope.addAssignFormSubmitError = null;
        $location.path('maintenance/assign/add');
    }

    $scope.editassign = function(assign) {
        $scope.assign = angular.extend({}, assign);
        /*$log.debug("$scope.assign : ", $scope.institutionsList, assign);
$scope.getStaffs(assign.hierarchy);
for (var i = 0; i < $scope.institutionsList.length; i++) {
    if ($scope.assign.hierarchy.school == $scope.institutionsList[i].school) {
        $scope.assign.institution = $scope.institutionsList[i];
        $scope.assign.school = $scope.institutionsList[i];
    }
}
*/
        $scope.editAssignFormSubmitError = null;
        $location.path('maintenance/assign/edit');
    }

    $scope.save = function(event, form) {
        event.preventDefault();

        if (form.$valid) {
            $log.debug("save method", $scope.assign)
            $scope.assign.hierarchy = $scope.assign.school;

            delete $scope.assign.Institution;
            delete $scope.assign.school;
            $log.debug("save method", $scope.assign)

            assign.save($scope.assign, $cookies.get("currenthierarchy")).then(function(data) {
                if (data.status == 200) {
                    console.log(data);
                    $scope.assign = {};
                    $scope.AssignFormSubmitMessage = data.message;
                    $scope.ShowAssignFormMessage = true;
                    $timeout(function() {
                        $scope.ShowAssignFormMessage = false;
                    }, 2000);

                    getAsssigns($scope.pageIndex);
                    $location.path('/maintenance/assign')

                } else if (data.status == 400) {
                    $scope.addAssignFormSubmitError = data.message;
                } else $scope.addAssignFormSubmitError = data.message;


            }, function(err) {
                $scope.addAssignFormSubmitError = err.data.message;
                $log.debug('Error while adding assign :', err);
            })
        } else {
            angular.forEach(form.$error.required, function(field) {
                field.$setDirty();
            });
        }
    }

    $scope.update = function(event, form) {
        event.preventDefault();
        if (form.$valid) {
            $log.debug('edit method', $scope.assign)
            delete $scope.assign.institution;
            delete $scope.assign.school;
            assign.update($scope.assign).then(function(data) {
                if (data.status == 200) {
                    console.log(data);
                    $scope.assign = {};
                    $scope.AssignFormSubmitMessage = data.message;
                    $scope.ShowAssignFormMessage = true;
                    $timeout(function() {
                        $scope.ShowAssignFormMessage = false;
                    }, 2000);

                    getAsssigns($scope.pageIndex);
                    $location.path('/maintenance/assign')

                } else if (data.status == 400) {
                    $scope.editAssignFormSubmitError = data.message;
                } else $scope.editAssignFormSubmitError = data.message;
            }, function(err) {
                $scope.editAssignFormSubmitError = err.data.message;
                $log.debug('Error while updating assign :', err);
            })

        } else {
            angular.forEach(editAssignForm.$error.required, function(field) {
                field.$setDirty();
            });
        }
    }

    $scope.delete = function() {

        $log.debug('delete method', $scope.currentAssign)
        assign.delete($scope.currentAssign).then(function(data) {
            for (var i = 0; i < $scope.length; i++) {
                if ($scope.AssisgnInfo[i].timeStamp == $scope.currentAssign.timeStamp) {
                    $scope.AssisgnInfo.splice(i, 1);
                }
            }
            $scope.AssignFormSubmitMessage = data.message;
            $scope.ShowAssignFormMessage = true;
            $timeout(function() {
                $scope.ShowAssignFormMessage = false;
            }, 2000);

            getAsssigns($scope.pageIndex);
            $location.path('/maintenance/assign')
            $scope.currentAssign = null;
            //$('#confirmDelete').modal('hide');
        }, function(err) {
            $log.debug('Error while deleting record :', err);
        })
    }

    $scope.setCurrentAssign = function(assign) {
        $scope.currentAssign = assign;
    }

    $scope.getSchools = function(institution) {
        $log.debug("selected institution is : ", institution);
    }

    $scope.getStaffs = function() {

        if (User.userInfo.profile.role.toLowerCase() === "admin" || User.userInfo.profile.role.toLowerCase() === "director") {
            var institution = $cookies.get("Hierarchy");
            console.log('user is director or admin', institution)
        } else {
            var institution = User.userInfo.profile.hierarchy;
            console.log('user is others', institution)
        }
        console.log("hhhh", institution)
        registration.getStaffList(institution).then(function(data) {
            $log.debug("getStaffList success block : ", data);
            $scope.staffList = data.staffList;
        }, function(err) {
            $log.debug(err);
        });
    }

    // $scope.setTeacher = function(argument) {
    //     $scope.teacher = _.find($scope.staffList, { smartId: argument.smartId });
    //     $scope.assign.teacherName = $scope.teacher.firstName;
    // }

    $scope.selectTeacher = function(teacher) {
        $log.debug('searchTeacher:', teacher);
        $scope.assign.teacherName = teacher.firstName;
        $scope.assign.teacherSmartId = teacher.smartId;
        $scope.assign.hodSmartId = teacher.reportingManagerId;
        //$('#myModal').modal('hide');
    }
});
