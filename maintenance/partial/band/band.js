angular.module('maintenance').controller('BandCtrl', function($log, $timeout, $cookies, $scope, $location, BandService) {

    // $scope.view = 'list';
    $scope.band = {};
    $scope.totalBands = 0;
    $scope.bandsPerPage = 7;
    $scope.pageIndex = 1;
    $scope.currentPage = 1;
    $scope.pageSize = 5;
    $scope.isPageChanged = true;
    $scope.maxVal = 10;
    $scope.minVal = 5;
    $scope.maxSize = 5;
    $scope.filteredList = '';
    $scope.searchText == '';

    console.log('bandcontroller')

    /*Page change on click on the page number*/
    $scope.pagination = {
        current: 1
    };
    $scope.bandPageChange = function(newPage) {
        console.log("newPage : ", newPage);
        if($scope.bandsPerPage){
        $scope.isPageChanged = true;
        $scope.pageIndex = newPage;
        console.log("page number ", newPage);
        getBands(newPage);
    }
    };


    
    function getBands(pageNumber) {
        var minVal = ($scope.bandsPerPage * (pageNumber - 1));
        var maxVal = pageNumber * $scope.bandsPerPage;
        console.log("Get function called from controller")
        BandService.get({ "min": minVal, "max": maxVal, "hierarchy": $cookies.get("currenthierarchy") }).then(function(data) {
            console.log(data)

            $scope.BandInfo = data.bandList.bandList;

            $scope.Permission = $cookies.getObject("modulePermission");

            $scope.totalBands = data.bandList.totalBands;
            console.log($scope.BandInfo);
        }, function(err) {
            console.log("error block : ", err);
        }, function(err) {

            console.log(err);
        });
    }
   getBands(1);

    $scope.search = function(data) {
            $scope.bandsPerPage = $scope.totalBands;
    }

    $scope.size = function(data){
        $scope.bandsPerPage = data;
    }

    $scope.addband = function() {
        $scope.band = {};

        $scope.addBandFormSubmitError = null;
        $location.path('maintenance/band/add');
    }

    $scope.editband = function(ban) {
        $scope.band = angular.extend({}, ban);
        $scope.editBandFormSubmitError = null;
        $location.path('maintenance/band/edit');
    }

    $scope.save = function(event, form) {
        event.preventDefault();
        if (form.$valid) {
            BandService.save($scope.band).then(function(data) {
                if (data.status == 200) {

                    console.log(data);
                    $scope.BandFormSubmitMessage = data.message;
                    $scope.ShowBandFormMessage = true;
                    $timeout(function() {
                        $scope.ShowBandFormMessage = false;
                    }, 2000);

                    $scope.band = {};
                    getBands($scope.pageIndex);
                    $location.path('/maintenance/band');
                } else if (data.status == 400) {
                    $scope.addBandFormSubmitError = data.message;
                } else $scope.addBandFormSubmitError = data.message;


            }, function(err) {
                $scope.addBandFormSubmitError = err.data.message;
                $log.debug('Error while adding band :', err);
            })
        } else {
            angular.forEach(form.$error.required, function(field) {
                field.$setDirty();
            });
        }
    }

    $scope.update = function(event, form) {
        event.preventDefault();

        if (form.$valid) {
            BandService.update($scope.band).then(function(data) {
                if (data.status == 200) {
                    $log.debug(data);
                    $scope.band = {};
                    $scope.BandFormSubmitMessage = data.message;
                    $scope.ShowBandFormMessage = true;
                    $timeout(function() {
                        $scope.ShowBandFormMessage = false;
                    }, 2000);
                    getBands($scope.pageIndex);
                    $location.path('/maintenance/band')

                } else if (data.status == 400) {
                    $scope.editBandFormSubmitError = data.message;
                } else $scope.editBandFormSubmitError = data.message;
            }, function(err) {
                $scope.editBandFormSubmitError = err.data.message;
                $log.debug('Error while updating band :', err);
            })
        } else {
            angular.forEach(form.$error.required, function(field) {
                field.$setDirty();
            });
        }
    }

    $scope.disabled = true;


    $scope.delete = function() {
        //$scope.currentBand = band;

        console.log('daya', $scope.currentBand)
        BandService.delete($scope.currentBand).then(function(data) {
            if (data.status == 200) {
                // $scope.disabled=true;
                console.log(data)
                for (var i = 0; i < $scope.BandInfo.length; i++) {
                    if ($scope.BandInfo[i].timeStamp == $scope.currentBand.timeStamp) {
                        $scope.BandInfo.splice(i, 1);

                    }
                }
            }

            // }else if(data.status==500){
            //     // console.log("$('#myModal') : ", $('#myModal'));
            //     // $('#myModal').modal('show');
            //     $scope.ReportList=data.data;
            //     $scope.message=data.message
            //     $scope.disabled=false;
            // }
            $scope.BandFormSubmitMessage = data.message;
            $scope.ShowBandFormMessage = true;
            $timeout(function() {
                $scope.ShowBandFormMessage = false;
            }, 2000);
            getBands($scope.pageIndex);
            $location.path('/maintenance/band')
            $scope.currentBand = null;


            // $('#confirmDelete').modal('hide');
        }, function(err) {
            $log.debug('Error while deleting record :', err);
        })
    }

    $scope.setCurrentBand = function(Band) {
        $scope.currentBand = Band;
    }






});
