 angular.module('maintenance').controller('HierarchyCtrl', function($log, $timeout, $state, $scope, $location, $cookies, HierarchyService, User) {
     $scope.hierarchy = {};

     $scope.totalHierarchy = 0;
     $scope.hierarchyPerPage = 7;
     $scope.pageIndex = 1;
     $scope.currentPage = 1;
     $scope.pageSize = 5;
     $scope.isPageChanged = true;
     $scope.maxVal = 10;
     $scope.minVal = 5;
     $scope.maxSize = 5;

     console.log("HierarchyCtrl");

     /*Page change on click on the page number*/
     $scope.pagination = {
         current: 1
     };


     $scope.hierarchyPageChange  = function(newPage) {
        if($scope.hierarchyPerPage){
        console.log("newPage : ", newPage);
        $scope.isPageChanged = true;
        $scope.pageIndex = newPage;
        console.log("page number ", newPage);
        getHierarchys(newPage);
    }
    };

     
     function getHierarchys(pageNumber) {
         var minVal = ($scope.hierarchyPerPage * (pageNumber - 1));
         var maxVal = pageNumber * $scope.hierarchyPerPage;
         console.log("Get function called from controller")
         HierarchyService.get({ "min": minVal, "max": maxVal, "hierarchy": $cookies.get("currenthierarchy") }).then(function(data) {
             console.log(data)
             $scope.HierarchyInfo = data.hierarchyList.hierarchyList;
             $scope.Permission = $cookies.getObject("modulePermission");
             $scope.totalhierarchy = data.hierarchyList.totalhierarchy;
             console.log($scope.HierarchyInfo);

         }, function(err) {

             console.log(err);
         });
     }
     getHierarchys(1);

     $scope.size = function(data){
        $scope.hierarchyPerPage = data;
     }


     $scope.search = function(data){
        $scope.hierarchyPerPage = $scope.totalhierarchy;
     }
    

     $scope.addHierarchy = function() {
         $scope.hierarchy = {};
         $scope.addHierarchyFormSubmitError = null;
         $location.path('maintenance/hierarchy/add');
     }

     $scope.editHierarchy = function(hierarchy) {
         $scope.hierarchy = angular.extend({}, hierarchy);
         $log.debug($scope.hierarchy);
         $scope.editHierarchyFormSubmitError = null;
         $location.path('maintenance/hierarchy/edit');
         // $state.transitionTo('maintenance.hierarchy.edit', {notify: false});
     }


     $scope.delete = function() {
         $log.debug("deleting");
         HierarchyService.delete($scope.currentHierarchy).then(function(data) {
             for (var i = 0; i < $scope.HierarchyInfo.length; i++) {
                 if ($scope.HierarchyInfo[i].timeStamp == $scope.currentHierarchy.timeStamp) {
                     $scope.HierarchyInfo.splice(i, 0);
                 }
             }


             $scope.hierarchy = {};
             $scope.HierarchyFormSubmitMessage = data.message;
             $scope.ShowHierarchyFormMessage = true;
             $timeout(function() {
                 $scope.ShowHierarchyFormMessage = false;
             }, 2000);
             getHierarchys($scope.pageIndex);
             $location.path('maintenance/hierarchy');
             $scope.currentHierarchy = null;
             // $('#deleteConfirmationModal').modal('hide');
         }, function(err) {
             $log.debug('Error while deleting record :', err);
         });
     }


     $scope.setCurrentHierarchy = function(hierarchy) {
         $scope.currentHierarchy = hierarchy;
     }



     $scope.save = function(event, form) {
         event.preventDefault();

         if (form.$valid) {
             $log.debug('in side save (controller)....')
             HierarchyService.save($scope.hierarchy).then(function(data) {

                 if (data.status == 200) {
                     $log.debug(data);
                     $scope.hierarchy = {};
                     $scope.HierarchyFormSubmitMessage = data.message;
                     $scope.ShowHierarchyFormMessage = true;
                     $timeout(function() {
                         $scope.ShowHierarchyFormMessage = false;
                     }, 2000);
                     getHierarchys($scope.pageIndex);
                     $location.path('/maintenance/hierarchy')

                 } else if (data.status == 400) {
                     $scope.addHierarchyFormSubmitError = data.message;
                 } else $scope.addHierarchyFormSubmitError = data.message;

             }, function(err) {
                 $scope.addHierarchyFormSubmitError = err.data.message;
                 $log.debug('Error while adding hierarchy :', err);
             })
         } else {
             angular.forEach(form.$error.required, function(field) {
                 field.$setDirty();
             });
         }
     }

     $scope.update = function(event, form) {
         event.preventDefault();
         $log.debug('in side update (controller).....')
         if (form.$valid) {
             HierarchyService.update($scope.hierarchy).then(function(data) {
                 if (data.status == 200) {
                     console.log(data);
                     $scope.hierarchy = {};
                     $scope.HierarchyFormSubmitMessage = data.message;
                     $scope.ShowHierarchyFormMessage = true;
                     $timeout(function() {
                         $scope.ShowHierarchyFormMessage = false;
                     }, 2000);
                     getHierarchys($scope.pageIndex);
                     $location.path('/maintenance/hierarchy')

                 } else if (data.status == 400) {
                     $scope.editHierarchyFormSubmitError = data.message;
                 } else $scope.editHierarchyFormSubmitError = data.message;
             }, function(err) {
                 $scope.editHierarchyFormSubmitError = err.data.message;
                 $log.debug('Error while updating hierarchy :', err);
             })
         } else {
             angular.forEach(form.$error.required, function(field) {
                 field.$setDirty();
             });
         }
     }


 });
