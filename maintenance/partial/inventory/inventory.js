angular.module('maintenance').controller('InventoryMasterCtrl', function($log, $rootScope, $timeout, $scope, $cookies, $location, InventoryMasterService) {

    $scope.inventory = {};

    $scope.totalInventory = 0;
    $scope.inventoryPerPage = 10;
    $scope.pageIndex = 1;
    $scope.currentPage = 1;
    $scope.pageSize = 5;
    $scope.isPageChanged = true;
    /*$scope.maxVal = 10;
    $scope.minVal = 5;*/
    $scope.maxSize = 5;

    /*Page change on click on the page number*/
    $scope.pagination = {
        current: 1
    };
    $scope.inventoryPageChange = function(newPage) {
        if($scope.inventoryPerPage){
        console.log("newPage : ", newPage);
        $scope.isPageChanged = true;
        $scope.pageIndex = newPage;
        console.log("page number ", newPage);
        getInventory(newPage);
    }
    };


    function getInventory(pageNumber) {
        var minVal = ($scope.inventoryPerPage * (pageNumber - 1));
        var maxVal = pageNumber * $scope.inventoryPerPage;
        console.log("Get function called from controller")
        InventoryMasterService.get({ "min": minVal, "max": maxVal, "hierarchy": $cookies.get("currenthierarchy") }).then(function(data) {
            console.log(data)
            $scope.InventoryInfo = data.inventoryList.inventoryList;
            $scope.Permission = $cookies.getObject("modulePermission");
            $scope.totalinventory = data.inventoryList.totalinventory;
            console.log($scope.InventoryInfo);

        }, function(err) {
            $log.debug(err);
        });
    }

    getInventory(1);

    $scope.size = function(data){
        $scope.inventoryPerPage = data;
    }

    $scope.search = function(data){
        if(data){
        $scope.inventoryPerPage = $scope.totalinventory;
    }else{
        $scope.inventoryPerPage = 7;
    }
    }

    $scope.cancelInventory = function() {
        getInventory();
        $location.path('/maintenance/inventory');
    }
    $scope.addinventory = function() {
        $scope.inventory = {};
        console.log( $scope.inventory);
        $scope.addInventoryFormSubmitError = null;
        $location.path('/maintenance/inventory/add');
    }

    $scope.editInventory = function(inventory) {
        $scope.inventory = angular.extend({}, inventory);
        console.log($scope.inventory)
        $scope.editInventoryFormSubmitError = null;
        $location.path('/maintenance/inventory/edit');
    }

    $scope.save = function(event, form) {
        event.preventDefault();

        if (form.$valid) {
            $scope.inventory.leftQuantity = $scope.inventory.quantity;
            InventoryMasterService.save($scope.inventory, $cookies.get("currenthierarchy")).then(function(data) {
                if (data.status == 200) {
                    $scope.inventory = {};
                    $scope.InventoryFormSubmitMessage = data.message;
                    $scope.ShowInventoryFormMessage = true;
                    $timeout(function() {
                        $scope.ShowInventoryFormMessage = false;
                    }, 2000);
                    getInventory($scope.pageIndex);
                    $location.path('/maintenance/inventory');

                } else if (data.status == 400) {
                    $scope.addInventoryFormSubmitError = data.message;
                } else $scope.addInventoryFormSubmitError = data.message;

            }, function(err) {
                $scope.addInventoryFormSubmitError = err.data.message;
                console.log('Error while adding Inventory :', err);
            })
        } else {
            angular.forEach(form.$error.required, function(field) {
                field.$setDirty();
            });
        }
    }

    $scope.update = function(event, form) {
        event.preventDefault();

        if (form.$valid) {
            // $scope.inventory.leftQuantity=$scope.inventory.quantity;
            InventoryMasterService.update($scope.inventory).then(function(data) {
                if (data.status == 200) {
                    $scope.inventory = {};
                    $scope.InventoryFormSubmitMessage = data.message;
                    $scope.ShowInventoryFormMessage = true;
                    $timeout(function() {
                        $scope.ShowInventoryFormMessage = false;
                    }, 2000);
                    getInventory($scope.pageIndex);
                    $location.path('/maintenance/inventory');

                } else if (data.status == 400) {
                    $scope.editInventoryFormSubmitError = data.message;
                } else $scope.editInventoryFormSubmitError = data.message;

            }, function(err) {
                $log.debug('Error while updating inventory :', err);
            })
        } else {
            angular.forEach(form.$error.required, function(field) {
                field.$setDirty();
            });
        }
    }

    $scope.delete = function() {

        InventoryMasterService.delete($scope.currentInventory).then(function(data) {
            /* for (var i = 0; i < $scope.InventoryInfo.length; i++) {
                 if ($scope.InventoryInfo[i].timeStamp == $scope.currentInventory.timeStamp) {
                     $scope.InventoryInfo.splice(i, 1);
                 }
             }*/
            $scope.InventoryFormSubmitMessage = data.message;
            $scope.ShowInventoryFormMessage = true;
            $timeout(function() {
                $scope.ShowInventoryFormMessage = false;
            }, 2000);
            getInventory($scope.pageIndex);
            $location.path('/maintenance/inventory')
            $scope.currentInventory = null;
            // $('#confirmDelete').modal('hide');
        }, function(err) {
            $log.debug('Error while deleting record :', err);
        })
    }


    $scope.setCurrentInventory = function(Inventory) {
        $scope.currentInventory = Inventory;
    }
});
