angular.module('maintenance').controller('LeavemasterCtrl', function($log, $timeout, $scope, $cookies, $location, LeaveMasterService) {

    $scope.leavemaster = {};

    $scope.totalLeaves = 0;
    $scope.leavePerPage = 10;
    $scope.pageIndex = 1;
    $scope.currentPage = 1;
    $scope.pageSize = 5;
    $scope.isPageChanged = true;
    $scope.maxVal = 10;
    $scope.minVal = 5;
    $scope.maxSize = 5;
    /*Page change on click on the page number*/
    $scope.pagination = {
        current: 1
    };
    $scope.leavePageChange = function(newPage) {
        if($scope.leavePerPage){
        console.log("newPage : ", newPage);
        $scope.isPageChanged = true;
        $scope.pageIndex = newPage;
        console.log("page number ", newPage);
        getLeaveMaster(newPage);
    }
    };


    function getLeaveMaster(pageNumber) {
        var minVal = ($scope.leavePerPage * (pageNumber - 1));
        var maxVal = pageNumber * $scope.leavePerPage;
        console.log("Get function called from controller")
        LeaveMasterService.get({ "min": minVal, "max": maxVal, "hierarchy": $cookies.get("currenthierarchy") }).then(function(data) {
            console.log(data)
            $scope.LeaveMasterInfo = data.leaveMasterList.leavemasterlist;
            $scope.Permission = $cookies.getObject("modulePermission");
            $scope.totallist = data.leaveMasterList.totallist;
            console.log($scope.LeaveMasterInfo);

        }, function(err) {


            $log.debug(err);
        });
    }

    getLeaveMaster(1);

    $scope.size = function(data){
        $scope.leavePerPage = data;
    }

    $scope.search = function(data){
        $scope.leavePerPage = $scope.totallist;
    }

    $scope.cancelLeaveMaster=function(){
        getLeaveMaster($scope.pageIndex);
        $location.path('/maintenance/leavemaster');
    }

    $scope.addleavemaster = function() {
        $scope.leavemaster = {};
        $scope.addLeaveMasterFormSubmitError = null;
        $location.path('/maintenance/leavemaster/add');
    }

    $scope.editLeavemaster = function(leavemaster) {
        $scope.leavemaster = leavemaster;
        $scope.editLeaveMasterFormSubmitError = null;
        $location.path('/maintenance/leavemaster/edit');
    }

    $scope.save = function(event, form) {
        $log.debug(form);
        event.preventDefault();

        if (form.$valid) {
            LeaveMasterService.save($scope.leavemaster, $cookies.get("currenthierarchy")).then(function(data) {

                if (data.status == 200) {
                    $log.debug(data);
                    $scope.leavemaster = {};
                    $scope.LeavemasterFormSubmitMessage = data.message;
                    $scope.ShowLeaveMasterFormMessage = true;
                    $timeout(function() {
                        $scope.ShowLeaveMasterFormMessage = false;
                    }, 2000);
                    getLeaveMaster($scope.pageIndex);
                    $location.path('/maintenance/leavemaster');
                } else if (data.status == 400) {
                    $scope.addLeaveMasterFormSubmitError = data.message;
                } else $scope.addLeaveMasterFormSubmitError = data.message;


            }, function(err) {
                $scope.addLeaveMasterFormSubmitError = err.data.message;
                $log.debug('Error while adding leavemaster:', err);
            })
        } else {
            angular.forEach(form.$error.required, function(field) {
                field.$setDirty();
            });
        }
    }

    $scope.update = function(event, form) {
        event.preventDefault();

        if (form.$valid) {
            console.log('updatingleavemaster', $scope.leavemaster)
            LeaveMasterService.update($scope.leavemaster).then(function(data) {
                if (data.status == 200) {
                    $log.debug(data);
                    $scope.leavemaster = {};
                    $scope.LeavemasterFormSubmitMessage = data.message;
                    $scope.ShowLeaveMasterFormMessage = true;
                    $timeout(function() {
                        $scope.ShowLeaveMasterFormMessage = false;
                    }, 2000);
                    getLeaveMaster($scope.pageIndex);
                    $location.path('/maintenance/leavemaster');
                } else if (data.status == 400) {
                    $scope.editLeaveMasterFormSubmitError = data.message;
                } else $scope.editLeaveMasterFormSubmitError = data.message;
            }, function(err) {
                $log.debug('Error while updatingleavemaster :', err);
                $scope.editLeaveMasterFormSubmitError = err.data.message;
            })
        } else {
            angular.forEach(form.$error.required, function(field) {
                field.$setDirty();
            });
        }
    }

    $scope.delete = function() {

        LeaveMasterService.delete($scope.currentleavemaster).then(function(data) {
            /*for (var i = 0; i < $scope.InventoryInfo.length; i++) {
                if ($scope.InventoryInfo[i].timeStamp == $scope.currentInventory.timeStamp) {
                    $scope.InventoryInfo.splice(i, 1);
                }*/


            $scope.leavemaster = {};
            $scope.LeavemasterFormSubmitMessage = data.message;
            $scope.ShowLeaveMasterFormMessage = true;
            $timeout(function() {
                $scope.ShowLeaveMasterFormMessage = false;
            }, 2000);
            getLeaveMaster($scope.pageIndex);
            $location.path('/maintenance/leavemaster');


            $scope.currentleavemaster = null;
            // $('#confirmDelete').modal('hide');
        }, function(err) {
            $log.debug('Error while deleting record :', err);
        })
    }


    $scope.setCurrentLeaveMaster = function(LeaveMaster) {
        $scope.currentleavemaster = LeaveMaster;
    }


});
