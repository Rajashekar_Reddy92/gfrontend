angular.module('maintenance').controller('RfidCtrl', function($log, $scope, $rootScope, $location, RfidService, $rootScope) {

    $scope.activeTab = 1;
    $scope.setActiveTab = function(tabToSet) {
        $log.debug(tabToSet);
        $scope.activeTab = tabToSet;
        $log.debug($scope.activeTab);

    }
    $scope.rfid = {};
    $scope.totalWithrfid = 0;
    $scope.totalWithoutrfid=0;
    $scope.rfidPerPage = 10;
    $scope.rfidPerPage1 = 10;
    $scope.pageIndex = 1;
    $scope.currentPage = 1;
    $scope.pageSize = 5;
    $scope.isPageChanged = true;
    $scope.maxVal = 10;
    $scope.minVal = 5;
    $scope.maxSize = 5;

    /*Page change on click on the page number*/
    $scope.pagination = {
        current: 1
    };


    $scope.pwithrfidPageChange = function(newPage) {
        if($scope.rfidPerPage){
        $log.debug("newPage : ", newPage);
        $scope.isPageChanged = true;
        $scope.pageIndex = newPage;
        $log.debug("page number ", newPage);
        getProfilesWithRfid(newPage);
    }
    };


    function getProfilesWithRfid(pageNumber) {
        var minVal = ($scope.rfidPerPage * (pageNumber -1));
        var maxVal = pageNumber * $scope.rfidPerPage;
        console.log(minVal,maxVal);
        $log.debug("Get function called from controller")
        RfidService.getwithrfid({"min" : minVal, "max" : maxVal}).then(function(data) {
        $log.debug(data)
        $scope.ProfileInfo1 = data.profileListWithRfid.profileListWithRfid; 
        $log.debug(data.profileListWithRfid)
        $scope.rfid = data.modulePermissions;
        $scope.totalwithrfid = data.profileListWithRfid.totalwithrfid;
        }, function(err) {
            console.log("error block : ", err);
             }, function(err) {

            console.log(err);
        });
    }

    getProfilesWithRfid(1);

    $scope.size = function(data){
        $scope.rfidPerPage = data;
    }

    $scope.search1 = function(data){
        $scope.rfidPerPage = $scope.totalwithrfid;
    }


    $scope.pwithoutrfidPageChange = function(newPage) {
        if($scope.rfidPerPage1){
        $log.debug("newPage : ", newPage);
        $scope.isPageChanged = true;
        $scope.pageIndex = newPage;
        $log.debug("page number ", newPage);
        getProfilesWithoutRfid(newPage);
    }
    };

    function getProfilesWithoutRfid(pageNumber) {
        var minVal = ($scope.rfidPerPage1 * (pageNumber -1));
        var maxVal = pageNumber * $scope.rfidPerPage1;
        $log.debug("Get function called from controller")
        RfidService.getwithoutrfid({"min" : minVal, "max" : maxVal}).then(function(data) {

        $log.debug(data)
        $scope.ProfileInfo = data.profileListWithoutRfid.profileListWithoutRfid; 
        $log.debug(data.profileListWithoutRfid)
        $scope.rfid = data.modulePermissions;
        $scope.totalrfid = data.profileListWithoutRfid.totalrfid;
        }, function(err) {
            console.log("error block : ", err);
             }, function(err) {

            console.log(err);
        });
    }

    getProfilesWithoutRfid(1);

    $scope.size1 = function(data){
        $scope.rfidPerPage1 = data;
    }

    $scope.search2 = function(data){
        $scope.rfidPerPage1 = $scope.totalrfid;
    }


    $scope.searchProfilesWithRfid = function(profile) {

        console.log(profile);
        RfidService.profilesListWithRfid(profile).then(function(data) {
            console.log("profilesListWithRfid : ", data);
            $scope.ProfileInfo1 = data.profilesListWithRfid;
            $scope.Permission = data.modulePermission;
            console.log($scope.ProfileInfo1);
        }, function(err) {
            log.debug(err);

        })

    }

$scope.searchProfilesWithoutRfid = function(profile) {

    console.log(profile);

    RfidService.profilesListWithoutRfid(profile).then(function(data) {
        console.log("profilesListWithoutRfid : ", data);
        $scope.ProfileInfo = data.profilesListWithoutRfid;
        $scope.Permission = data.modulePermission;
        console.log($scope.ProfileInfo);
    }, function(err) {

        console.log('Error while searching :', err);
    })
}
/*
$scope.$watch('profile', function(profile) {
        alert('hey, name has changed!');
    });
*/
    $scope.search = {};
    $scope.$watch('search.withRfid', function() {
        console.log('watch method called ',$scope.search.withRfid);
        if (!$scope.search.withRfid || ($scope.search.withRfid && $scope.search.withRfid.length == 0)) { 
            console.log('watch method called inside if',$scope.search.withRfid);
            getProfilesWithRfid(1);
        }
    });

    $scope.$watch('search.withoutRfid', function() {
        console.log('watch method called ',$scope.search.withoutRfid);
        if (!$scope.search.withoutRfid || ($scope.search.withoutRfid && $scope.search.withoutRfid.length == 0)) { 
            console.log('watch method called inside if',$scope.search.withoutRfid);
            getProfilesWithoutRfid(1);
        }
    });

$scope.save = function(rfid) {
                console.log("rfid");
            
        $log.debug(rfid);
        $log.debug(rfid.rfId);
          RfidService.save(rfid).then(function(data) {
            getProfilesWithoutRfid(1);
              $log.debug("saved")
                $location.path('/maintenance/rfid');
            })

}


$scope.update = function(rfid) {
        $log.debug("rfid");
        $log.debug(rfid);
            RfidService.update(rfid).then(function(data) {
              
              
              $log.debug("saved")
                $location.path('/maintenance/rfid');
            })
       
        
    }

});
/*app.controller('TextSimpleCtrl', function($scope) {
  $scope.user = {
    name: 'awesome user'
  };  
});*/
