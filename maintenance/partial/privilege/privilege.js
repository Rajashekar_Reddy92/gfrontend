
angular.module('maintenance').controller('PrivilegeCtrl',function($log, $location ,$scope, PrivilegeService, BandService){

$scope.profile = {};
    $scope.totalBands = 0;
    $scope.privilegePerPage = 7;
    $scope.pageIndex = 1;
    $scope.currentPage = 1;
    $scope.pageSize = 5;
    $scope.isPageChanged = true;
    $scope.maxVal = 10;
    $scope.minVal = 5;
    $scope.maxSize = 5;

    $scope.pagination = {
        current: 1
    };

    $scope.PrivilegePageChange = function(newPage) {
        console.log("newPage : ", newPage);
        if($scope.bandsPerPage){
        $scope.isPageChanged = true;
        $scope.pageIndex = newPage;
        console.log("page number ", newPage);
        getBands(newPage);
    }
    };

 function getBands() {
       $log.debug("Get function called from controller")
        BandService.get().then(function(data) {
            $log.debug(data)
            $scope.BandInfo = data.bandList;
            $scope.Permission = data.modulePermission;

        }, function(err) {
            $log.debug(err);
        });
    }

    getBands();

 $scope.search = function(event,form) {
        event.preventDefault();

        if (form.$valid) {
            
            PrivilegeService.search($scope.profile).then(function(data) {
            	$log.debug(data);
            $scope.ProfileInfo = data.profileList;
            $scope.Permission = data.modulePermission;
                $log.debug($scope.ProfileInfo);
                $location.path('/maintenance/privilege/add')
            }, function(err) {
               
                $log.debug('Error while adding band :', err);
            })
        } else {
            angular.forEach(form.$error.required, function(field) {
                field.$setDirty();
            });
        }
    }

 $scope.add = function(data){
    $scope.profile = data;
    $log.debug("after search ",$scope.profile.role);
    $location.path('/maintenance/privilege/edit')
 };
 
 $scope.update = function(role) {
       
         if (true) {
            $log.debug(role.role);
            $scope.profile.role=role.role;
            PrivilegeService.edit($scope.profile).then(function(data) {
                $scope.profile = {};   
                  
                $location.path('/maintenance/privilege/add')
            }, function(err) {
               
                $log.debug('Error while updating profile :', err);
            })
        } else {
            angular.forEach(form.$error.required, function(field) {
                field.$setDirty();
            });
        }
    };
});
