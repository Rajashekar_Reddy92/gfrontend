angular.module('maintenance').controller('PermissionsCtrl', function($log, $cookies, $timeout, $scope, $location, permissionsService, Maintenance, moduleservice, BandService) {

    $scope.permissions = {};

    $scope.totalPermissions = 0;
    $scope.rolesPerPage = 10;
    $scope.pageIndex = 1;
    $scope.currentPage = 1;
    $scope.pageSize = 5;
    $scope.isPageChanged = false;
    $scope.maxVal = 10;
    $scope.minVal = 5;
    $scope.maxSize = 5;

    $scope.pagination = {
        current: 1
    };
    $scope.rolesPageChange = function(newPage) {
        if($scope.rolesPerPage){
        console.log("newPage : ", newPage);
        $scope.isPageChanged = true;
        $scope.pageIndex = newPage;
        console.log("page number ", newPage);
        getPermission(newPage);
    }
    };


    function getPermission(pageNumber) {
        var minVal = ($scope.rolesPerPage * (pageNumber - 1));
        var maxVal = pageNumber * $scope.rolesPerPage;
        console.log("Get function called from controller")
        permissionsService.get({ "min": minVal, "max": maxVal }).then(function(data) {
            console.log(data)
            $scope.PermissionsInfo = data.rolePermissionList.rolePermissions;
            $scope.Permission = $cookies.getObject("modulePermission");
            $scope.totalpermission = data.rolePermissionList.totalpermission;
            console.log($scope.PermissionsInfo);
        }, function(err) {
            console.log("error block : ", err);
        }, function(err) {

            console.log(err);
        });
    }

    getPermission(1);
     
    $scope.size = function(data){
        $scope.rolesPerPage = data;
    }

    $scope.search1 = function(data){
            $scope.rolesPerPage = $scope.totalpermission;
    }

    BandService.getBand().then(function(data) {
        //if (data.status == 200) {
        $log.debug("data from the backend is : ", data)
        $scope.BandInfo = data.bandList;
        $log.debug($scope.BandInfo);
        // else $scope.getPermissionError=data.message;

    }, function(err) {
        $log.debug(err);
    });

    moduleservice.get().then(function(data) {
        // if (data.status == 200) {
        $scope.ModInfo = data.modulesList;
        $log.debug($scope.ModInfo)
            //  }

    }, function(err) {
        $log.debug(err);
    });




    $scope.isRequired = false;
    $scope.onFieldChange = function() {
        if ($scope.permissions.moduleName == "Maintenance") {
            $scope.isRequired = true;
        } else {
            $scope.isRequired = false;
        }
    }

    $scope.addPermissions = function() {
        $log.debug('hoii')
        $scope.permissions = {};
        $scope.addFormSubmitError = null;
        $location.path('maintenance/rolepermission/add');
    }

    $scope.editPermissions = function(permissions) {
        //console.log("permissions :" + permissions + " $scope.permissions :" + $scope.permissions);
        $scope.permissions = angular.extend({}, permissions);
        $log.debug($scope.permissions);
        $scope.editFormSubmitError = null;

        $location.path('maintenance/rolepermission/edit');
    }

    $scope.save = function(event, form) {
        event.preventDefault();
        $log.debug($scope.permissions)
        if (form.$valid) {
            permissionsService.save($scope.permissions).then(function(data) {
                if (data.message == 'success') {
                    $log.debug('$scope.permissions')

                    $scope.permissions = {};
                    $scope.PermissionFormSubmitMessage = data.message;
                    $scope.ShowPermissionFormMessage = true;
                    $timeout(function() {
                        $scope.ShowPermissionFormMessage = false;
                    }, 2000);
                    getPermission($scope.pageIndex);
                    $location.path('/maintenance/rolepermission')
                } else $scope.addFormSubmitError = data.message;

            }, function(err) {
                $scope.addFormSubmitError = err.data.message;
                $log.debug('Error while adding permissions :', err);
            })
        } else {
            angular.forEach(form.$error.required, function(field) {
                field.$setDirty();
            });
        }
    }

    $scope.showSubmodule = function(argument) {
        $log.debug("showSubmodule : ", argument)
        if (argument.modules == "Maintenance") {
            $scope.showField = true;
        } else {
            $scope.showField = false;
        }
    }

    $scope.updaterolepermission = function(event, form) {
        event.preventDefault();
        $log.debug('update method')

        if (form.$valid) {
            $log.debug('up', $scope.permissions);
            permissionsService.updaterole($scope.permissions).then(function(data) {
                if (data.message == 'success') {
                    $scope.permissions = {};
                    $scope.PermissionFormSubmitMessage = data.message;
                    $scope.ShowPermissionFormMessage = true;
                    $timeout(function() {
                        $scope.ShowPermissionFormMessage = false;
                    }, 2000);
                    getPermission($scope.pageIndex);
                    $location.path('/maintenance/rolepermission')
                    $log.debug(data);
                } else $scope.editFormSubmitError = data.message;

            }, function(err) {
                $scope.editFormSubmitError = err.data.message;
                $log.debug('Error while adding permissions :', err);
            })
        } else {
            angular.forEach(form.$error.required, function(field) {
                field.$setDirty();
            });
        }
    }

    $scope.delete = function() {

        permissionsService.delete($scope.currentPermissions).then(function(data) {
            /*  for (var i = 0; i < $scope.PermissionsInfo.length; i++) {
                  if ($scope.PermissionsInfo[i].timeStamp == $scope.currentPermissions.timeStamp) {
                      $scope.PermissionsInfo.splice(i, 1);
                  }
              }*/
            $scope.PermissionFormSubmitMessage = data.message;
            $scope.ShowPermissionFormMessage = true;
            $timeout(function() {
                $scope.ShowPermissionFormMessage = false;
            }, 2000);
            getPermission($scope.pageIndex);
            $location.path('/maintenance/rolepermission');
            $scope.currentPermissions = null;
            // $('#deleteConfirmationModal').modal('hide');
        }, function(err) {
            $log.debug('Error while deleting record :', err);
        })
    }

    $scope.setCurrentPermissions = function(permissions) {
        $scope.currentPermissions = permissions;
    }


});
