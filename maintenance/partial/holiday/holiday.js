angular.module('maintenance').controller('HolidayCtrl', function($log, $cookies, $state, $timeout, $window, $scope, $rootScope, $location, weekdaysService, holidayService, utils) {
    $scope.weekdays = {};
    //weekdays code 
    $scope.days = [
        { day: "SUNDAY" },
        { day: "MONDAY" },
        { day: "TUESDAY" },
        { day: "WEDNESDAY" },
        { day: "THURSDAY" },
        { day: "FRIDAY" },
        { day: "SATURDAY" }

    ];

$scope.weekdayInfo = [];

    function getWeekdays() {
        console.log("Get function called from weekday controller")
        weekdaysService.getWeekdays($cookies.get("currenthierarchy")).then(function(data) {
            console.log("data is getting***************");
            console.log(data);
            $scope.weekdayInfo = data.data;
            console.log('$scope.weekdayInfo',$scope.weekdayInfo);
            $scope.Permission = $cookies.getObject("modulePermission");
            console.log($scope.PermissionWeeekdays);

        });
    }
    getWeekdays();


    $scope.test = {};
    $scope.testweekdays = function() {
        console.log($scope.test);


        weekdaysService.save($scope.test, $cookies.get("currenthierarchy")).then(function(data) {
            if (data.status == 200) {

                console.log('saved data', data)
                $scope.weekdays = {};
                $scope.addWeekdaysFormSubmitError = data.message;
                $scope.ShowWeekdaysFormMessage = true;
                $timeout(function() {
                    $scope.ShowWeekdaysFormMessage = false;
                }, 2000);
                getWeekdays();
                $state.go('maintenance.holiday');
            } else if (data.status == 400) {
                console.log("400", data)

                $scope.addWeekdaysFormSubmitError = data.message;
                $scope.ShowWeekdaysFormMessage = true;
                $timeout(function() {
                    $scope.ShowWeekdaysFormMessage = false;
                }, 2000);

            } else $scope.addWeekdaysFormSubmitError = data.message;
            $scope.ShowWeekdaysFormMessage = true;
            $timeout(function() {
                $scope.ShowWeekdaysFormMessage = false;
            }, 2000);

        }, function(err) {
            $log.debug('Error while adding weekdays :', err);
        })
    }

    $scope.weekdaysDelete = function() {
        console.log('delete weekdays', $scope.currentWeekday);
        weekdaysService.delete($scope.currentWeekday).then(function(data) {
            console.log('deleting weekdays');
            for (var i = 0; i < $scope.weekdayInfo.length; i++) {
                if ($scope.weekdayInfo[i].timeStamp == $scope.currentWeekday.timeStamp) {
                    $scope.weekdayInfo.splice(i, 1);
                }
            }
            getWeekdays();
            $location.path('/maintenance/holiday')
            $scope.currentWeekday = null;
            // $('#confirmDelete').modal('hide');
        }, function(err) {
            console.log('Error while deleting record :', err);
        });

    }

    $scope.setCurrentWeekday = function(weekdays) {
        console.log('deleteConfirmationModal');
        $scope.currentWeekday = weekdays;
    }




    //holiday information

    /*   $scope.view = 'list';*/
    $scope.holiday = {};

    $scope.totalHolidays = 0;
    $scope.holidaysPerPage = 10;
    $scope.pageIndex = 1;
    $scope.currentPage = 1;
    $scope.pageSize = 5;
    $scope.isPageChanged = true;
    $scope.maxVal = 10;
    $scope.minVal = 5;
    $scope.maxSize = 5;


    /*Page change on click on the page number*/
    $scope.pagination = {
        current: 1
    };


    $scope.holidayPageChange = function(newPage) {
        if ($scope.holidaysPerPage) {
            console.log("newPage : ", newPage);
            $scope.isPageChanged = true;
            $scope.pageIndex = newPage;
            console.log("page number ", newPage);
            getHolidays(newPage);
        }
    };



    function getHolidays(pageNumber) {
        var minVal = ($scope.holidaysPerPage * (pageNumber - 1));
        var maxVal = pageNumber * $scope.holidaysPerPage;
        holidayService.get({ "min": minVal, "max": maxVal, "hierarchy": $cookies.get("currenthierarchy") }).then(function(data) {
            console.log(data);
            $scope.HolidayInfo = data.holidayList.holidayList;
            $scope.Permission = $cookies.getObject("modulePermission");
            $scope.totalholidaylist = data.holidayList.totalholidaylist;
            $scope.HolidayInfo.forEach(function(holiday) {
                holiday.displayDate = utils.getDateString(holiday.holidayDate);
                holiday.holidayDate = new Date(holiday.holidayDate);
            })
        }, function(err) {
            console.log("error block : ", err);
        });
    }
    getHolidays(1);

    $scope.size = function(data) {
        $scope.holidaysPerPage = data;
    }

    $scope.search = function(data) {
        $scope.holidaysPerPage = $scope.totalholidaylist;
    }

    $scope.today = function() {
        $scope.holiday.holidayDate = new Date();
    };

    $scope.addHoliday = function() {
        $scope.holiday = {};
        $scope.addHolidayFormSubmitError = null;
        $location.path('maintenance/holiday/add');
    }

    $scope.editHoliday = function(holiday) {
        $scope.holiday = angular.extend({}, holiday);
        $log.debug('holiday', $scope.holiday)
        $scope.showdp = false;
        $scope.editHolidayFormSubmitError = null;

        $location.path('maintenance/holiday/edit');
    }


    $scope.save = function(event, form) {
        event.preventDefault();
        if (form.$valid) {
            var holiday = angular.extend({}, $scope.holiday);
            delete holiday.displayDate;
            holiday.holidayDate = holiday.holidayDate.toString();
            holidayService.save($scope.holiday, $cookies.get("currenthierarchy")).then(function(data) {

                if (data.status == 200) {

                    $scope.holiday = {};
                    $scope.HolidayFormSubmitMessage = data.message;
                    $scope.ShowHolidayFormMessage = true;
                    $timeout(function() {
                        $scope.ShowHolidayFormMessage = false;
                    }, 2000);
                    getHolidays($scope.pageIndex);
                    $location.path('/maintenance/holiday')

                    // }else  $scope.addHolidayFormSubmitError=data.message;

                } else if (data.status == 400) {

                    $scope.addHolidayFormSubmitError = data.message;

                } else $scope.addHolidayFormSubmitError = data.message;

            }, function(err) {
                $log.debug('Error while adding holiday :', err);
            })
        } else {
            angular.forEach(form.$error.required, function(field) {
                field.$setDirty();
            });
        }
    }

    $scope.update = function(event, form) {
        event.preventDefault();
        if (form.$valid) {
            var holiday = angular.extend({}, $scope.holiday);

            delete holiday.displayDate;
            holiday.holidayDate = holiday.holidayDate;

            holidayService.update(holiday).then(function(data) {
                if (data.status == 200) {

                    $scope.holiday = {};
                    $scope.HolidayFormSubmitMessage = data.message;
                    $scope.ShowHolidayFormMessage = true;
                    $timeout(function() {
                        $scope.ShowHolidayFormMessage = false;
                    }, 2000);
                    getHolidays($scope.pageIndex);
                    $location.path('/maintenance/holiday')
                } else if (data.status == 400) {

                    $scope.editHolidayFormSubmitError = data.message;

                } else $scope.editHolidayFormSubmitError = data.message;

            }, function(err) {
                $log.debug('Error while updating holiday :', err);
            })
        } else {
            angular.forEach(form.$error.required, function(field) {
                field.$setDirty();
            });
        }
    }


    /*$scope.delete = function() {

        $log.debug("in side holiday delete....");
        console.log("uytrop", $scope.currentHoliday)

        holidayService.delete($scope.currentHoliday).then(function(data) {
            $log.debug('further inside');
            $scope.HolidayFormSubmitMessage = data.message;
            $scope.ShowHolidayFormMessage = true;
            $timeout(function() {
                $scope.ShowHolidayFormMessage = false;
            }, 2000);
            getHolidays($scope.pageIndex);
            $location.path('/maintenance/holiday')
            $scope.currentHoliday = null;
            //$('#deleteConfirmationModal').modal('hide');
        }, function(err) {
            $log.debug('Error while deleting record :', err);
        })
    }

    $scope.setCurrentHoliday = function(holiday) {
        console.log('holiday', holiday)
        $scope.currentHoliday = holiday;
    }*/

     $scope.delete = function() {
         console.log("in side holiday delete....");
        var holiday = angular.extend({}, $scope.currentHoliday);
        delete holiday.displayDate; 
        // holiday.holidayDate = holiday.holidayDate.toString();

        holidayService.delete(holiday).then(function(data) {
            console.log('further inside');
            getHolidays($scope.pageIndex);
            $location.path('/maintenance/holiday')
            $scope.currentHoliday = null;
            //$('#deleteConfirmationModal').modal('hide');
        }, function(err) {
            console.log('Error while deleting record :', err);
        })
    }

    $scope.setCurrentHoliday = function(holiday) {
        $scope.currentHoliday = holiday;
    }

    $scope.dateformat = "dd/MM/yyyy";

    $scope.showcalendar = function($event) {
        $scope.popup.showdp = true;
        console.log('calender')
    };

    $scope.popup = {
        showdp: false
    };
    $log.debug('ru');
    $scope.today();

    // $scope.disabled = function(date, mode) {
    //     console.log("disable inside")
    //     return (mode === 'day' && (date.testweekdays() === 0 || date.testweekdays() === 6));
    // };

     $scope.options = {
        dateDisabled: disabled,
        showWeeks: true,
        startingDay: 1

    };

   //$scope.disabled = function(date, mode){
    function disabled(data) {
        var date = data.date,
            mode = data.mode;

       var weekdayexp = new Array(7);
            weekdayexp[0] = "SUNDAY";
            weekdayexp[1] = "MONDAY";
            weekdayexp[2] = "TUESDAY";
            weekdayexp[3] = "WEDNESDAY";
            weekdayexp[4] = "THURSDAY";
            weekdayexp[5] = "FRIDAY";
            weekdayexp[6] = "SATURDAY";

            var isHoliday = false;
            for(var i = 0; i < $scope.weekdayInfo.length ; i++) {
              if(areDatesEqual($scope.weekdayInfo[i].weekDay, weekdayexp[date.getDay()])) {
                isHoliday = true;
              }
            }

            return ( mode === 'day' && isHoliday );
          };
          function areDatesEqual(date1, date2) {
                console.log('date1',date1)
                console.log('date2',date2)
                return date1 === date2;

              }
        //return mode === 'day' && (date.getDay() === 0);
});
