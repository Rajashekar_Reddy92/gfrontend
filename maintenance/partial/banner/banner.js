angular.module('maintenance').controller('BannerCtrl', function($scope, $location, BannerService, $rootScope, $filter, Notice) {

    $scope.banner = {};

    function getBanner() {
        console.log("Get function called from controller")
        BannerService.getBanner().then(function(data) {
            console.log('banner data :', data);
            $scope.banner.image = $scope.banner.image;
            $scope.bannerInfo = data.bannerList;
            $scope.Permission = data.modulePermission;

            console.log($scope.bannerInfo);
        }, function(err) {
            console.log("error while fetching data: ", err);
        }, function(err) {

            console.log(err);
        });
    }

    getBanner();

    $scope.addBanner = function() {
        $scope.banner = {};
        $scope.addBannerFormSubmitError = null;
        $location.path('/maintenance/banner/add');
    }

    $scope.editBanner = function(banner) {
        console.log("banner :" + banner + " $scope.banner :" + $scope.banner);
        $scope.banner = angular.extend({}, banner);
        console.log($scope.banner);
        $location.path('maintenance/banner/edit');
    }

    $scope.save = function(event, form) {
            console.log('inside banner controller');
            event.preventDefault();

            if (form.$valid) {
                console.log("form is : ", form);
                $scope.banner.image = $scope.banner.image.base64;
                /*$scope.banner = {};*/
                console.log('inside form validition', $scope.banner);
                BannerService.uploadBanner($scope.banner).then(function(data) {
                    console.log("response data of addBanner : ", data);
                    $scope.bannerInfo.push($scope.banner);
                    $location.path('/maintenance/banner')
                });
            } else {
                angular.forEach(form.$error.required, function(field) {
                    field.$setDirty();
                });
            }
        }
        /*
             $scope.update = function(event, form) {
                event.preventDefault();
                console.log('inside banner update-controller')
                if (form.$valid) {
                     $scope.banner.image = $scope.banner.image.base64;
                    BannerService.update($scope.banner).then(function(data) {
                        // $scope.banner = {};
                        $location.path('/maintenance/banner')
                        getBanner();
                        
                    }, function(err) {
                        $scope.editBannerFormSubmitError = err.data.message;
                        
                    })
                } else {
                    angular.forEach(form.$error.required, function(field) {
                        field.$setDirty();
                    });
                }
            }
        */
    $scope.delete = function() {
        console.log("inside banner delete-controller");

        BannerService.delete($scope.currentBanner).then(function(data) {
            console.log("inside delete request");
            for (var i = 0; i < $scope.bannerInfo.length; i++) {
                if ($scope.bannerInfo[i].timeStamp == $scope.currentBanner.timeStamp) {
                    $scope.bannerInfo.splice(i, 1);
                }
            }

            getBanner();

            $scope.showModel = false;

            $scope.currentBanner = null;
        }, function(err) {
            console.log('Error while deleting record :', err);
        });
    }
    $scope.setCurrentBanner = function(banner) {
        $scope.showModel = true;
        $scope.currentBanner = banner;
    }


});
