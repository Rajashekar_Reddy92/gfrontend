angular.module('maintenance', ['appConfig', 'ui.bootstrap', 'ui.utils', 'ui.router', 'utils', 'xeditable']);

angular.module('maintenance').config(function($stateProvider) {


    $stateProvider.state('maintenance', {
        url: '/maintenance',
        parent: 'app',
        templateUrl: 'maintenance/partial/home.html',
        controller: 'Maintenance',
        resolve: {
                template: function($q, User) {
                    var defer = $q.defer();
                    User.getUserInfo(function(user) {
                        var template = null;
                        
                     if (user.profile.role.toLowerCase() ==='admin' || user.profile.role.toLowerCase() ==='director')
                        {
                            console.log('user is admin')
                            template = 'maintenance/partial/landing.html';
                        }
                        else
                        {
                            template='maintenance/partial/maintenance.html';
                        }

                        defer.resolve(template);
                    });
                    return defer.promise;
                }   
            }



    })
    .state('maintenance.view', {
        url: '/view',
        templateUrl: 'maintenance/partial/maintenance.html'
    })
    $stateProvider.state('maintenance.band', {
            url: '/band',
            templateUrl: 'maintenance/partial/band/band.html',
            controller: 'BandCtrl'
        })
        .state('maintenance.band.add', {
            url: '/add',
            templateUrl: 'maintenance/partial/band/add-band.html'
        })
        .state('maintenance.band.edit', {
            url: '/edit',
            templateUrl: 'maintenance/partial/band/edit-band.html'
        })


    $stateProvider.state('maintenance.assign', {
            url: '/assign',
            templateUrl: 'maintenance/partial/assign/assign.html',
            controller: 'AssignCtrl'
        })
        .state('maintenance.assign.add', {
            url: '/add',
            templateUrl: 'maintenance/partial/assign/add-assign.html'
        })
        .state('maintenance.assign.edit', {
            url: '/edit',
            templateUrl: 'maintenance/partial/assign/edit-assign.html'
        })


    $stateProvider.state('maintenance.hierarchy', {
            url: '/hierarchy',
            templateUrl: 'maintenance/partial/hierarchy/hierarchy.html',
            controller: 'HierarchyCtrl'
        })
        .state('maintenance.hierarchy.add', {
            url: '/add',
            templateUrl: 'maintenance/partial/hierarchy/add-hierarchy.html'
        })
        .state('maintenance.hierarchy.edit', {
            url: '/edit',
            templateUrl: 'maintenance/partial/hierarchy/edit-hierarchy.html'
        })

    $stateProvider.state('maintenance.holiday', {
            url: '/holiday',
            templateUrl: 'maintenance/partial/holiday/holiday.html',
            controller: 'HolidayCtrl'
        })
        .state('maintenance.holiday.add', {
            url: '/add',
            templateUrl: 'maintenance/partial/holiday/add-holiday.html'
        })
        .state('maintenance.holiday.edit', {
            url: '/edit',
            templateUrl: 'maintenance/partial/holiday/edit-holiday.html'
        })

    $stateProvider.state('maintenance.rolepermission', {
            url: '/rolepermission',
            templateUrl: 'maintenance/partial/permissions/permissions.html',
            controller: 'PermissionsCtrl'
        })
        .state('maintenance.rolepermission.add', {
            url: '/add',
            templateUrl: 'maintenance/partial/permissions/add-permissions.html'
        })
        .state('maintenance.rolepermission.edit', {
            url: '/edit',
            templateUrl: 'maintenance/partial/permissions/edit-permissions.html'
        })

    $stateProvider.state('maintenance.inventory', {
            url: '/inventory',
            templateUrl: 'maintenance/partial/inventory/inventory.html',
            controller: 'InventoryMasterCtrl'
        })
        .state('maintenance.inventory.add', {
            url: '/add',
            templateUrl: 'maintenance/partial/inventory/add-inventory.html'
        })
        .state('maintenance.inventory.edit', {
            url: '/edit',
            templateUrl: 'maintenance/partial/inventory/edit-inventory.html'
        })

    $stateProvider.state('maintenance.fee', {
            url: '/feemaster',
            templateUrl: 'maintenance/partial/fee/fee.html',
            controller: 'FeeMasterCtrl'
        })
        .state('maintenance.fee.add', {
            url: '/add',
            templateUrl: 'maintenance/partial/fee/add-fee.html'
        })
        .state('maintenance.fee.upload', {
            url: '/upload',
            templateUrl: 'maintenance/partial/fee/upload.html'
        })
        .state('maintenance.fee.edit', {
            url: '/edit',
            templateUrl: 'maintenance/partial/fee/edit-fee.html'

        })


    $stateProvider.state('maintenance.leavemaster', {
            url: '/leavemaster',
            templateUrl: 'maintenance/partial/leavemaster/leavemaster.html',
            controller: 'LeavemasterCtrl'
        })
        .state('maintenance.leavemaster.add', {
            url: '/add',
            templateUrl: 'maintenance/partial/leavemaster/add-leavemaster.html'
        })
        .state('maintenance.leavemaster.edit', {
            url: '/edit',
            templateUrl: 'maintenance/partial/leavemaster/edit-leavemaster.html'

        })


    $stateProvider.state('maintenance.privilege', {
            url: '/privilege',
            templateUrl: 'maintenance/partial/privilege/privilege.html',
            controller: 'PrivilegeCtrl'
        })
        .state('maintenance.privilege.add', {
            url: '/add',
            templateUrl: 'maintenance/partial/privilege/add-privilege.html'
        })
        .state('maintenance.privilege.edit', {
            url: '/edit',
            templateUrl: 'maintenance/partial/privilege/edit-privilege.html'

        })

    $stateProvider.state('maintenance.rfid', {
            url: '/rfid',
            templateUrl: 'maintenance/partial/rfid/rfid.html',
            controller: 'RfidCtrl'
    });
    $stateProvider.state('maintenance.banner', {
            url: '/banner',
            templateUrl: 'maintenance/partial/banner/banner.html',
            controller: 'BannerCtrl'
        })
        .state('maintenance.banner.add', {
            url: '/add',
            templateUrl: 'maintenance/partial/banner/add-banner.html'
        })
        .state('maintenance.banner.edit', {
            url: '/edit',
            templateUrl: 'maintenance/partial/banner/edit-banner.html'
        });
});

angular.module('maintenance').config(function($resourceProvider) {
    $resourceProvider.defaults.stripTrailingSlashes = false;
})
