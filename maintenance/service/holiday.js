angular.module('maintenance').service('holidayService', function($log, $resource, config) {
    var holiday = $resource(config.baseUrl + '/holiday/:ts', { ts: '@task' }, {
        'update': { method: 'PUT' }
    });

    var getHolidays = $resource(config.baseUrl + '/holiday/:min/:max/:hierarchy', { min: '@min', max: '@max', hierarchy: '@hierarchy' }, {
        'update': { method: 'PUT' }
    });

    var holidayHierarchy = $resource(config.baseUrl + '/holiday/hierarchy/:hierarchy', { ts: '@task', hierarchy: '@hierarchy' }, {
        'update': { method: 'PUT' }
    });


    this.get = function(params) {
        console.log("data is saved");
        console.log(params)
        return getHolidays.get(params).$promise;

    }

    this.save = function(data, hierarchy) {
        var params = {
            hierarchy: hierarchy
        }
        $log.debug("data is saved");
        return holidayHierarchy.save(params, data).$promise;
    }






    /*this.delete1 = function(data) {
    console.log('holidayService');
    return holiday.update({ ts: "delete1" }, data).$promise;
}
*/
    /* var weekdays = $resource(config.baseUrl + '/weekdays/', {}, {
         'update': { method: 'PUT' }
     });
     this.getWeekdays = function() {
         return weekdays.get().$promise;
     }*/

    /*this.delete = function(data) {
    return weekdays.update({ ts: "delete" }, data).$promise;
}
*/




    this.update = function(data) {
        $log.debug("data is editing");
        return holiday.update({ ts: "edit" }, data).$promise;
    }

    this.delete = function(data) {
        console.log('holiday data', data)
        return holiday.update({ ts: "delete" }, data).$promise;

    }


});
