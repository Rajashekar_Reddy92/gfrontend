angular.module('maintenance').service('BandService', function($resource, $http, config) {
    
   // $http.defaults.headers.common.Authorization = 'TOken token';

    var band = $resource(config.baseUrl +'/band/:task/', {task: '@task'}, {
         'update': { method: 'PUT'}});


    var getBands = $resource(config.baseUrl +'/band/:min/:max/:hierarchy', {min: '@min', max: '@max' ,hierarchy: '@hierarchy'}, {
         'update': { method: 'PUT'}});

    this.get = function (params) {  
        console.log('params',params)
        return getBands.get(params).$promise;
    }
    this.getBand=function(){
        return band.get().$promise;
    }
    
    this.save = function (data) {
        return band.save(data).$promise;
    }

    this.update = function (data) {
        return band.update({task: "edit"}, data).$promise;
    }

    this.delete = function (data) {
        return band.update({task: "delete"}, data).$promise;
    }

}); 
