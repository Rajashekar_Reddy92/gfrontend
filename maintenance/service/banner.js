angular.module('maintenance').service('BannerService', function($resource, $http, config) {
    
   // $http.defaults.headers.common.Authorization = 'TOken token';

    var getbanner = $resource(config.baseUrl + '/banner/:task', {task: '@task'}, {
        'update': { method: 'PUT'}});
    console.log('inside getbanner service');

    this.getBanner= function () {
        return getbanner.get().$promise;
    }

    this.uploadBanner = function(data) {
        console.log('inside banner service');
        return getbanner.save(data).$promise;
    }
    this.update = function (data) {
        return getbanner.update({task: "edit"}, data).$promise;
    }

    this.delete = function (data) {
        return getbanner.update({task: "delete"}, data).$promise;
    }
   
}); 