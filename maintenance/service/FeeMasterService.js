angular.module('maintenance').service('FeeMasterService', function($http, $resource, config, Upload) {
    var fee = $resource(config.baseUrl + '/feeMaster/:ts', { ts: '@task' }, {
        'update': { method: 'PUT' }
    });
     var getFee = $resource(config.baseUrl +'/feeMaster/:min/:max/:hierarchy', {min: '@min', max: '@max', hierarchy: '@hierarchy'}, {
         'update': { method: 'PUT'}});

     var feeHierarchy = $resource(config.baseUrl + '/feeMaster/hierarchy/:hierarchy', { hierarchy: "@hierarchy" }, {
        'update': { method: 'PUT' }
    });

    this.get = function(params) {
        return getFee.get(params).$promise;
    }

    this.save = function(data,hierarchy) {
        var params={
            hierarchy:hierarchy
        }
        return feeHierarchy.save(params,data).$promise;
    }

    this.update = function(data) {
        return fee.update({ ts: "edit" }, data).$promise;
    }

    this.delete = function(data) {
        return fee.update({ ts: "delete" }, data).$promise;
    }

    this.upload = function(file) {
        return Upload.upload({
            url: config.baseUrl + '/feeMaster/upload/uguv',
            method: 'POST',
            data: { file: file }


        }).$promise;
    }




this.uploadFileToUrl = function(file){
               var fd = new FormData();
               fd.append('file', file);
            
               $http.post(config.baseUrl + '/feeMaster/upload/001', fd, {
                  transformRequest: angular.identity,
                  headers: {'Content-Type': undefined}
               })
            
               .success(function(){

                console.log('success');
               })
            
               .error(function(){

                console.log('error');
               });
            }




    this.downloadFile = function () {
    $http({
        method: 'GET',
        url: config.baseUrl + '/feeMaster/download/feeDetails'
    }).success(function (data, status, headers) {
        headers = headers();
 
        var filename = 'feeDetails';
        var contentType = "application/octet-stream";
 
        var linkElement = document.createElement('a');
        console.log(data)
        console.log(data.Object)
        try {
            var blob = new Blob([data.Object.file], { type: contentType });
            var url = window.URL.createObjectURL(blob);
 
            linkElement.setAttribute('href', url);
            linkElement.setAttribute("download", filename);
 
            var clickEvent = new MouseEvent("click", {
                "view": window,
                "bubbles": true,
                "cancelable": false
            });
            linkElement.dispatchEvent(clickEvent);
        } catch (ex) {
            console.log(ex);
        }
    }).error(function (data) {
        console.log(data);
    });
};












});