angular.module('maintenance').service('Maintenance', function($log, $resource, config) {
    
    var subModules = $resource(config.baseUrl +'/rolepermission/subModules', {});

    this.getSubModules= function () {
        $log.debug("get function");
        return subModules.query().$promise;
    }

});
