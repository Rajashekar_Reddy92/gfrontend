angular.module('maintenance').service('HierarchyService', function($log, $resource, config) {
    var hierarchy = $resource(config.baseUrl +'/hierarchy/:ts/', {ts: '@task'}, {
         'update': { method: 'PUT' }
     });
    var getHierarchys = $resource(config.baseUrl +'/hierarchy/:min/:max/:hierarchy', {min: '@min', max: '@max',hierarchy: '@hierarchy'}, {
         'update': { method: 'PUT'}});
 
    this.get= function (params) {
        $log.debug('inside the service get');
        
        return getHierarchys.get(params).$promise;
    }
    this.getAllHierarchys=function(){

    return hierarchy.get().$promise;
    }


    this.save = function (data) {
        return hierarchy.save(data).$promise;     
    }

    this.update = function (data) {

        return hierarchy.update({ts: "edit"}, data).$promise;
    }

    this.delete = function (data) {
        return hierarchy.update({ts: "delete"},data).$promise;
    }

});