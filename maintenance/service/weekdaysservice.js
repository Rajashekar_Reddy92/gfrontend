angular.module('maintenance').service('weekdaysService', function($resource, config, $http) {
    var weekdays = $resource(config.baseUrl + '/weekdays/:ts/', { ts: '@task' }, {
        'update': { method: 'PUT' }
    });

    var weekdayshierarchy = $resource(config.baseUrl + '/weekdays/:hierarchy/', { hierarchy: '@hierarchy' }, {
        'update': { method: 'PUT' }
    });

    this.getWeekdays = function(hierarchy) {
        var params = {
            hierarchy: hierarchy
        }
        console.log("get methode for weekdays*****");
        return weekdayshierarchy.get(params).$promise;
    }
    this.save = function(data, hierarchy) {
        var params = {
            hierarchy: hierarchy
        }
        console.log("data is saved..........");
        return weekdayshierarchy.save(params, data).$promise;
    }
    this.delete = function(data) {
        console.log('weekdaysService');
        return weekdays.update({ ts: "delete" }, data).$promise;
    }
});

/*var weekdays = $resource(config.baseUrl + '/weekdays/', {}, {
        'update': { method: 'PUT' }
    });
    this.getWeekdays = function() {
        return weekdays.get().$promise;
    }*/
