angular.module('maintenance').service('InventoryMasterService', function($resource, config) {
    var inventory = $resource(config.baseUrl +'/inventory/:ts', {ts: '@ts'}, {
         'update': { method: 'PUT' }
     });

    var getInventory = $resource(config.baseUrl +'/inventory/:min/:max/:hierarchy', {min: '@min', max: '@max',hierarchy: '@hierarchy'}, {
         'update': { method: 'PUT'}});

     var inventoryHierarchy = $resource(config.baseUrl +'/inventory/hierarchy/:hierarchy', {hierarchy: '@hierarchy'}, {
         'update': { method: 'PUT' }
     });
 
    this.get= function (params) {
        return getInventory.get(params).$promise;
    }

    this.save = function (data,hierarchy) {
      var params={
        hierarchy:hierarchy
      }
      console.log(data);
        return inventoryHierarchy.save(params,data).$promise;
    }

    this.update = function (data) {
        console.log(data);
        return inventory.update({ts: "edit"}, data).$promise;
    }

    this.delete = function (data) {
        return inventory.update({ts: "delete"}, data).$promise;
    }
});
    