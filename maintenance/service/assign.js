angular.module('gsmart').service('assign', function($resource, $http, config, $log) {

    var assign = $resource(config.baseUrl + '/assign/:task', { task: '@task' }, {
        'update': { method: 'PUT' }
    });

 var assignHierarchy = $resource(config.baseUrl + '/assign/hierarchy/:hierarchy', { hierarchy: '@hierarchy' }, {
        'update': { method: 'PUT' }
    });
    var getAsssigns = $resource(config.baseUrl +'/assign/:min/:max/:hierarchy', {min: '@min', max: '@max', hierarchy: '@hierarchy'}, {
         'update': { method: 'PUT'}});

    this.get= function (params) {  
        $log.debug('assign service')
        return getAsssigns.get(params).$promise;
    }
    this.getAsssignList=function(hierarchy){
        var params={
            task:hierarchy
        }

         return assign.get(params).$promise;

    }

    this.save = function(data,hierarchy) {
        var params={
            hierarchy:hierarchy
        }
        return assignHierarchy.save(params,data).$promise;
    }

    this.update = function(data) {
        return assign.update({ task: "edit" }, data).$promise;
    }

    this.delete = function(data) {
        return assign.update({ task: "delete" }, data).$promise;
    }
});
