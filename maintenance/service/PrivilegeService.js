angular.module('maintenance').service('PrivilegeService',function($resource, config) {

	
	var privilege = $resource(config.baseUrl +'/privilege/:ts/', {ts: '@task'}, {
		'update': { method: 'PUT' }	
	});

	 this.search = function (data) {
        return privilege.save(data).$promise;
    }

    this.edit = function (data) {
        return privilege.update({ts: "edit"},data).$promise;
    }
});