angular.module('maintenance').service('LeaveMasterService',function($resource, config) {
    var leavemaster = $resource(config.baseUrl +'/leavemaster/:task', {task: '@task'}, {
         'update': { method: 'PUT' }
     });

    var getLeaveMaster = $resource(config.baseUrl +'/leavemaster/:min/:max/:hierarchy', {task: '@task', min: '@min', max: '@max',hierarchy: '@hierarchy'}, {
         'update': { method: 'PUT'}});

     var leavemasterHierarchy = $resource(config.baseUrl +'/leavemaster/hierarchy/:hierarchy', {hierarchy: '@hierarchy'}, {
         'update': { method: 'PUT' }
     });
 
    this.get= function (params) {
        return getLeaveMaster.get(params).$promise;
    }

    this.getLeaveMasterList=function(){
        return getLeaveMaster.get().$promise;
    }

    this.save = function (data,hierarchy) {
        var params={
            hierarchy:hierarchy
        }
        return leavemasterHierarchy.save(params,data).$promise;
    }

    this.update = function (data) {
        return leavemaster.update({task: "edit"}, data).$promise;
    }

    this.delete = function (data) {
        return leavemaster.update({task: "delete"}, data).$promise;
    }
});