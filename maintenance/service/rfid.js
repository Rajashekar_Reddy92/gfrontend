angular.module('maintenance').service('RfidService',function($log, $resource, $http, config) {

    var rfid =  $resource(config.baseUrl +'/rfid/:task/', {task: '@task'}, {
         'update': { method: 'POST'}});


    var getProfilesWithRfid = $resource(config.baseUrl +'/rfid/ProfilesWithRfid/:min/:max', {min: '@min', max: '@max'}, {
         'get': { method: 'GET'}});

    this.getwithrfid = function (params) {  
        return getProfilesWithRfid.get(params).$promise;
    }
     
    var getProfilesWithoutRfid = $resource(config.baseUrl +'/rfid/ProfilesWithoutRfid/:min/:max', {min: '@min', max: '@max'}, {
         'get': { method: 'GET'}});

    this.getwithoutrfid = function (params) {  
        return getProfilesWithoutRfid.get(params).$promise;
    }

    this.save = function (data) {
        $log.debug(data);
        return rfid.save(data).$promise;
    }
     this.update = function (data) {
        $log.debug(data);
        return rfid.update(data).$promise;
    }
    

    var withoutrfid = $resource(config.baseUrl + '/rfid/profilesListWithoutRfid/:firstName', {firstName: '@firstName'}, {});

    this.profilesListWithoutRfid = function(firstName) {
        console.log(firstName);
        return withoutrfid.save({firstName:firstName}).$promise;
    }


    var withrfid = $resource(config.baseUrl + '/rfid/profilesListWithRfid/:firstName', {firstName: '@firstName'}, {});

    this.profilesListWithRfid = function(firstName) {
        console.log(firstName);
        return withrfid.save({firstName:firstName}).$promise;
    }
    // this.update = function (data) {
    //     return rfid.update({task: "edit"}, data).$promise;
    // }



   
});