angular.module('maintenance').service('permissionsService', function($log, $resource, config) {
    var permission = $resource(config.baseUrl +'/rolepermission/:task/', {task: '@task'}, {
          'update': { method: 'PUT' }
     });

     var getPermission = $resource(config.baseUrl +'/rolepermission/:min/:max', {min: '@min', max: '@max'}, {
         'update': { method: 'PUT'}});
 
    this.get= function (params) {
        return getPermission.get(params).$promise;
    }


    this.save = function (data) {
        $log.debug(data);
        return permission.save(data).$promise;
    }

    this.updaterole = function (data) {
        $log.debug(data);
        return permission.update({task: "edit"}, data).$promise;
    }

    this.delete = function (data) {
        return permission.update({task: "delete"}, data).$promise;
    }
});
