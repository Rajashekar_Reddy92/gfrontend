angular.module('notice', ['ui.bootstrap', 'ui.utils', 'ui.router']);

angular.module('notice').config(function($stateProvider) {


    $stateProvider.state('notice', {
            url: '/notice',
            parent: 'app',
            templateUrl: 'notice/partial/home.html',
            controller: 'NoticeCtrl',
            resolve: {
                template: function($q, User) {
                    var defer = $q.defer();
                    User.getUserInfo(function(user) {
                        console.log(user)
                        var template = null;

                        
                        if (user.profile.role.toLowerCase() === 'admin' || user.profile.role.toLowerCase() ==='director') {

                            
                            template = 'notice/partial/noticelanding.html';
                        }
                        else if(user.profile.role.toLowerCase() === 'principal' || user.profile.role.toLowerCase() === 'hod' || user.profile.role.toLowerCase() === 'hr') {
                            
                            template = 'notice/partial/staff-notice.html';
                            console.log('staff notices:',template);
                        } 
                        else {
                            
                            template = 'notice/partial/child-notice.html';
                        }
                        defer.resolve(template);
                    });
                    return defer.promise;
                }   
            }
        })
        .state('notice.decider', {
            url: '/decider',
            templateUrl: 'notice/partial/decider.html'
        }) 
        .state('notice.decider1', {
            url: '/decider1',
            templateUrl: 'notice/partial/decider1.html'
        }) 
        .state('notice.hier', {
            url: '/hier',
            templateUrl: 'notice/partial/noticelanding.html'
        }) 
        .state('notice.list', {
            url: '/list',
            templateUrl: 'notice/partial/notice.html'
        })
        .state('notice.add', {
            url: '/add',
            templateUrl: 'notice/partial/add-notice.html'
        })
        .state('notice.edit', {
            url: '/edit',
            templateUrl: 'notice/partial/edit-notice.html'
        })
        .state('notice.org', {
            url: '/org',
            templateUrl: 'notice/partial/notice-orgstructure.html'
        })
        .state('notice.org2', {
            url: '/:empId',
            templateUrl: 'notice/partial/notice-orgstructure.html'
        })
        .state('notice.child', {
            url: '/child/:empId',
            templateUrl: 'notice/partial/child-notice.html',
            controller:'NoticeOrgCtrl'
        })
     
});