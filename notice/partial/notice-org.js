angular.module('notice').controller('NoticeOrgCtrl', function($log, $scope,$rootScope, User, $stateParams, NoticeService, $location, $cookies) {

    $scope.childNotices = {};


    var parentId = $stateParams.empId ? $stateParams.empId : User.id;



    NoticeService.get1(parentId).then(function(data) {
        $log.debug(parentId);
        $log.debug(data);
        $rootScope.Notice=data
       /* $scope.currentEmp = data.selfProfile;
        $scope.people = data.childList;
        console.log($scope.currentEmp);

        console.log($scope.people);*/
    }, function(err) {
        $log.debug('Error while fetching orgStructure :', err);
    });



    NoticeService.childNoticeDetails(parentId, function(childNotice) {
        $log.debug(parentId);
        if (childNotices) {
            $log.debug(childNotice);
            $scope.childNotice = childNotice;
        }

    })




    $scope.showOrganizationTree = function(empId) {
        $log.debug($location.path('/notice/org/' + empId));
        $location.path('/notice/' + empId);
    }



 $scope.download=function (file) {

      var image_data = atob(file.split(',')[1]);
      console.log('file',image_data)
    // Use typed arrays to convert the binary data to a Blob
    var arraybuffer = new ArrayBuffer(image_data.length);
    var view = new Uint8Array(arraybuffer);
    for (var i=0; i<image_data.length; i++) {
        view[i] = image_data.charCodeAt(i) & 0xff;
    }
    try {
        // This is the recommended method:
        var blob = new Blob([arraybuffer], {type: 'application/octet-stream'});
    } catch (e) {
        // The BlobBuilder API has been deprecated in favour of Blob, but older
        // browsers don't know about the Blob constructor
        // IE10 also supports BlobBuilder, but since the `Blob` constructor
        //  also works, there's no need to add `MSBlobBuilder`.
        var bb = new (window.WebKitBlobBuilder || window.MozBlobBuilder);
        bb.append(arraybuffer);
        var blob = bb.getBlob('application/octet-stream'); // <-- Here's the Blob
    }

    // Use the URL object to create a temporary URL
    var url = (window.webkitURL || window.URL).createObjectURL(blob);
    location.href = url; // <-- Download!
    //   console.log('file',file);
    // var data = 'some data here...',

    //     blob = new Blob([file], { type: 'text/plain' }),
    //     url = $window.URL || $window.webkitURL;
    // $scope.fileUrl = url.createObjectURL(blob);
    };




   $scope.uploadFile = function(files) {
        console.log('files',files);

           $scope.notice.format=files[0].type;
           files[0].type; //MIME type
          files[0].size; //File size in bytes
      }; 


});