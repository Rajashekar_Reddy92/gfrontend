angular.module('notice').controller('NoticeCtrl', function($scope, $log, $rootScope, HierarchyService, User, $stateParams, NoticeService, $state, $location, $cookies, template) {


    $scope.template = template;
    var noticeTree = null;
    $scope.empId = $stateParams.empId;
    $scope.employee = null;
    $scope.people = [];
    $scope.childOfChildList = {};
    $scope.notices = {};
    $scope.MyNoticeInfo = [];
    $scope.activeTab = 1;

    User.getUserInfo(function(userInfo) {
        $log.debug("Home cntrl");
        $log.debug(userInfo);
        $scope.modules = userInfo.permissions;
        $scope.details = userInfo.profile;
    });




    /*$scope.getNotices();*/
    function getHierarchys() {
        HierarchyService.get().then(function(data) {
            $scope.HierarchyInfo = data.hierarchyList;
            $scope.Permission = data.modulePermission;
            console.log(data)
            console.log('$scope.HierarchyInfo');
        }, function(err) {
            console.log(err);
        });
    }

    getHierarchys()

    $scope.noticeType = [{ "type": "Generic" }, { "type": "Specific" }];

    $scope.restrictNotice = function(item) {
        $log.debug("restricted item is : ", item, $scope.details);

    }

    $scope.setActiveTab = function(tabToSet) {
        $log.debug(tabToSet);
        $scope.activeTab = tabToSet;
        $log.debug($scope.activeTab);
    }

    $scope.notice = {};


    $scope.getSpecificNotices = function() {
        console.log(User.id);
        NoticeService.get(User.id).then(function(data) {
            console.log("hai", data);
            $scope.NoticeInfo = data.data;
         //   var x=  $scope.NoticeInfo.length;
            for(var i = 0; i < $scope.NoticeInfo.length; i++) {
                $scope.NoticeInfo[i].entryTime = (new Date(parseInt($scope.NoticeInfo[i].entryTime)).toString()).substr(0,15);

            }

        }, function(err) {

            $log.debug(err);
        });
    }

    $scope.getSpecificNotices();

    $scope.getMyNotices = function() {
        $log.debug("get Mynotices");
      NoticeService.getmynotice(User.userInfo.profile.smartId).then(function(data) {
            $log.debug('getMyNotices:', data);
            $scope.MyNoticeInfo = data.data;

            $scope.Permission = $cookies.getObject("modulePermission");
            //$scope.MyNoticeInfo.image = $scope.MyNoticeInfo.image;

        }, function(err) {
            $log.debug(err);
        })
    }

    $scope.addNotice = function() {
        $log.debug('addNotice called')
        $scope.notice = {};
        /* $scope.addNoticeFormError = null;
         */
        $scope.loading = true;
        if ($scope.details.role.toLowerCase() == 'principal') {
            $location.path('/notice/add');
        } else if ($scope.details.role.toLowerCase() == 'admin' || $scope.details.role.toLowerCase() == 'director') {
            for (var i = 0; i < $scope.noticeType.length; i++) {
                if ($scope.noticeType[i].type.toLowerCase() == 'specific') {
                    $scope.noticeType.splice(i, 1);
                }
            }
            $location.path('/notice/add');
            $scope.loading = false;
        } else {
            for (var i = 0; i < $scope.noticeType.length; i++) {
                if ($scope.noticeType[i].type.toLowerCase() == 'generic') {
                    $scope.noticeType.splice(i, 1);
                }
            }
            $location.path('/notice/add');
            $scope.loading = false;
        }
    }

    $scope.editNotice = function(Notice) {
        $scope.Notice = angular.extend({}, Notice);
        $location.path('/notice/edit');
    }


    $scope.savenotice = function(image,form) {

    
        event.preventDefault();
        if (image != null) {
            $scope.notice.image = image.base64;
        }

        $scope.notice.smartId = User.id;
        $scope.notice.type = $scope.notice.types;
        delete $scope.notice.types;
        console.log($scope.notice);

        NoticeService.save($scope.notice).then(function(data) {
            console.log("saving data",data);
           // $scope.MyNoticeInfo.push($scope.notice);
           /* $state.go("notice");
*/       if ($scope.details.role.toLowerCase() == 'admin') {
                console.log("in side saving .....")
            $location.path('notice/hier');
        } else{
             $location.path('/notice/decider')

        }   
        
        }, function(err) {
            $scope.addNoticeFormError = err.data.message;
            $log.debug('Error while adding Notice :', err);

        });

    }


    $scope.update = function(event, form, image) {
        event.preventDefault();
        if (form.$valid) {

            $log.debug($scope.Notice);
            console.log('format', $scope.notice.format)
            if (image != null) {
                $scope.Notice.image = image.base64;
            }
            $scope.Notice.format = $scope.notice.format;
            $scope.loading = true;

            NoticeService.update($scope.Notice).then(function(data) {
                /*$log.debug("after edit");*/
               /* for (var i = 0; i < $scope.MyNoticeInfo.length; i++) {
                    if ($scope.MyNoticeInfo[i].entryTime === $scope.Notice.entryTime) {
                        $log.debug("after editing");
                        $scope.MyNoticeInfo.splice(i, 1);
                    }


                }*/
               if ($scope.details.role.toLowerCase() == 'admin') {
                    console.log("in side saving .....")
                    $location.path('notice/hier');
                } else{
                     $location.path('/notice/decider')
                }   

            });

        } else {
            angular.forEach(form.$error.required, function(field) {
                field.$setDirty();
            });
        }
    }

    $scope.delete = function() {
        /*$log.debug('currentNotice', $scope.currentNotice)*/
        $scope.loading = true;
        NoticeService.delete($scope.currentNotice).then(function(data) {

            if ($scope.details.role.toLowerCase() == 'admin') {
                console.log("in side saving .....")
            $location.path('notice/hier');
        } else{
             $location.path('/notice/decider')

        }   
            $scope.currentNotice = null;
            $('#confirmDelete').modal('hide');

        }, function(err) {
            $log.debug('Error while deleting record :', err);
        })
    }

    $scope.setCurrentNotice = function(Notice) {
        $scope.currentNotice = Notice;
    }


    $scope.download = function(file) {

        var image_data = atob(file.split(',')[1]);
        console.log('file', image_data)
            // Use typed arrays to convert the binary data to a Blob
        var arraybuffer = new ArrayBuffer(image_data.length);
        var view = new Uint8Array(arraybuffer);
        for (var i = 0; i < image_data.length; i++) {
            view[i] = image_data.charCodeAt(i) & 0xff;
        }
        try {
            // This is the recommended method:
            var blob = new Blob([arraybuffer], { type: 'application/octet-stream' });
        } catch (e) {
            // The BlobBuilder API has been deprecated in favour of Blob, but older
            // browsers don't know about the Blob constructor
            // IE10 also supports BlobBuilder, but since the `Blob` constructor
            //  also works, there's no need to add `MSBlobBuilder`.
            var bb = new(window.WebKitBlobBuilder || window.MozBlobBuilder);
            bb.append(arraybuffer);
            var blob = bb.getBlob('application/octet-stream'); // <-- Here's the Blob
        }

        // Use the URL object to create a temporary URL
        var url = (window.webkitURL || window.URL).createObjectURL(blob);
        location.href = url; // <-- Download!
        //   console.log('file',file);
        // var data = 'some data here...',

        //     blob = new Blob([file], { type: 'text/plain' }),
        //     url = $window.URL || $window.webkitURL;
        // $scope.fileUrl = url.createObjectURL(blob);
    };



    $scope.uploadFile = function(files) {
        console.log('files', files);

        $scope.notice.format = files[0].type;
        console.log('format', $scope.notice.format)
        files[0].type; //MIME type
        files[0].size; //File size in bytes
    };



    $scope.getNoticeByHierarchy = function(hierarchy) {
        $scope.hid = hierarchy.hid;
        NoticeService.getNotice($scope.hid).then(function(data){
            console.log(data);
            $scope.MyNoticeInfo1=data.data;
          $scope.Permission = $cookies.getObject("modulePermission");
        $state.go('notice.decider1');
        },function(err){
            console.log(err);
        });
        
    }

});
