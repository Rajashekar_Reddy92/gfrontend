angular.module('maintenance').service('NoticeService', function($log, $resource, config, $q) {

    var organizationNotice = null;
    $log.debug("service called");

    // var Fee2 = $resource(config.baseUrl + '/fee/'+smartId+'/'+academicYear);

    var getnotice = $resource(config.baseUrl + '/notice/viewNotice/:smartId', { smartId: '@smartId' }, {
        'get': { method: 'GET' }
    });
    var notice = $resource(config.baseUrl + '/notice/addNotice', {}, {
        'save': { method: 'POST' }
    });
    var updatenotice = $resource(config.baseUrl + '/notice/editNotice', {}, {
        'update': { method: 'PUT' }
    });
    var getmynotice = $resource(config.baseUrl + '/notice/viewMyNotice/:smartId', { smartId: '@smartId' }, {
        'get': { method: 'GET' }
    });

    this.get = function(hid) {
        console.log("inventory services");
        return Notice.get({ task: hid }).$promise;
    }


    this.getmynotice = function(smartId) {
        $log.debug(smartId);

        return getmynotice.get({ smartId: smartId }).$promise;
    }

    this.get = function(smartId) {
        return getnotice.get({ smartId: smartId }).$promise;
    }

    this.save = function(data) {
        console.log(data);
        return notice.save(data).$promise;
    }

    this.update = function(data) {
        return updatenotice.update(data).$promise;
    }


    var deletenotice = $resource(config.baseUrl + '/notice/deleteNotice', {}, {
        'update': { method: 'PUT' }
    });
    this.delete = function(data) {

        $log.debug("data and smartId is : ", data);

        return deletenotice.update(data).$promise;
    }



    var orgnotice = $resource(config.baseUrl + '/notice/S/:smartId', { smartId: '@smartId' }, {
        'update': { method: 'PUT' }
    });
    /*
        this.get1= function (smartId) {
            return orgnotice.get({smartId:smartId}).$promise;
        }*/




    this.get1 = function(smartId) {
        var defer = $q.defer();
        var params = {
            smartId: smartId
        };

        orgnotice.get(params).$promise.then(function(data) {
            $log.debug('notice services ', data)
            organizationNotices = data;
            defer.resolve(data);
        }, function(err) {
            defer.reject(err);
        });

        return defer.promise;
    }



    this.childNoticeDetails = function(id, callback) {

        $log.debug('parent Id', id)
        var childNoticesDetail = [];
        $log.debug(organizationNotice);

        if (organizationNotice && organizationNotice.childNotice[0].length > 0) {

            for (var i = 0; i < organizationNotice.childNotice[0].length > 0; i++) {

                $log.debug('data' + typeof organizationNotice.childNotice[i].smart_id, organizationNotices.childNotice[i].smart_id);

                $log.debug(organizationNotice.childNotice[i].smart_id == id.toString());

                if (organizationNotice.childNotice[i].smart_id == id.toString()) {

                    $log.debug(id);

                    childNoticesDetail.push(organizationNotice.childNotice[i]);

                    $log.debug(childNoticeDetail);

                    // break;
                }
            }
            callback(childNoticesDetail);
        } else {
            //make the web service call to fetch employee details using id
        }
    }



    /*
        Object.size = function(obj) {
        var size = 0, key;
        for (key in obj) {
            if (obj.hasOwnProperty(key)) size++;
        }
        return size;
    };

    // Get the size of an object
    var size = Object.size(childNotices);*/


    var getnoticeByHierarchy = $resource(config.baseUrl + '/notice/adminNotice/:hid', { hid: '@hid' }, {
        'get': { method: 'GET' }
    });

    this.getNotice = function(hid) {
        return getnoticeByHierarchy.get({ hid: hid }).$promise;
    }

});
