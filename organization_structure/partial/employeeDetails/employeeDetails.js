angular.module('organizationStructure').controller('EmployeeDetailsCtrl', function($log, $scope, User, $stateParams,$location, orgStructure) {
    $scope.empDetails = {};

    $log.debug( $stateParams.empId);
	var childId = $stateParams.empId ? $stateParams.empId : User.id;

    if (childId != User.id) {
        $log.debug(childId);
        orgStructure.getEmployeeDetails(childId, function(empDetails) {

            if (empDetails) {
                $scope.empDetails = empDetails;
            }

        })
    } else {
    	$log.debug(User.userInfo.profile);
        $scope.empDetails = User.userInfo.profile;
        $log.debug($scope.empDetails);
    }




});
