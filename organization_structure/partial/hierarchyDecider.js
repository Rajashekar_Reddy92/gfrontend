angular.module('organizationStructure').controller('hierarchydeciderCtrl', function($scope, template, User, HierarchyService, $filter, $stateParams, $location, $state, $cookies) {
    $scope.template = template;

    function getHierarchys() {
        HierarchyService.get().then(function(data) {
            $scope.HierarchyInfo = data.hierarchyList;
            console.log(data)
            console.log($scope.HierarchyInfo)
        }, function(err) {
            console.log(err);
        });
    }
    getHierarchys();
    if (User.userInfo.profile.role.toLowerCase() === "admin" || User.userInfo.profile.role.toLowerCase() === "director") {


    } else {
        $cookies.put("currenthierarchy", angular.toJson(0));

    }




    $scope.getOrgByHierarchy = function(hierarchy) {
        console.log("selected hierarchy in orgs is : ", hierarchy);
        $cookies.put("currenthierarchy", angular.toJson(hierarchy.hid));
        $location.path('/orgstructure/view');
    }
});
