angular.module('organizationStructure').controller('OrgstructureCtrl', function($scope, template, $log, User, $cookies, $filter, $stateParams, orgStructure, $location) {
    var organizationTree = null;
    $scope.empId = $stateParams.empId;
    $scope.employee = null;
    $scope.people = [];
    $scope.childOfChildList = {};
    $scope.search = {};
    $scope.template = template;

    var parentId = $stateParams.empId ? $stateParams.empId : User.userInfo.profile.smartId;




    var academicYear = new Date().getFullYear() + "-" + (new Date().getFullYear() + 1);

    orgStructure.get(parentId, $cookies.get("currenthierarchy"), academicYear).then(function(data) {
        $log.debug(parentId);
        $scope.currentEmp = data.selfProfile;
        $scope.people = data.childList;
        $log.debug($scope.currentEmp);
    }, function(err) {
        $log.debug('Error while fetching orgStructure :', err);
    });



    $scope.showOrganizationTree = function(empId) {

        $location.path('/orgstructure/view/' + empId);
    }

    $scope.goToSearch = function() {
        $log.debug('goToSearch function in orgstructure controller')
        orgStructure.save(User.userInfo.profile.smartId, $scope.search, academicYear, $cookies.get("currenthierarchy")).then(function(data) {
            $log.debug("data.childList : ", data.childList);
            $scope.searchResults = data.childList;
            $log.debug(data);
            $location.path('/orgstructure/view/search');
            //$location.search($scope.search);
            // search();
        }, function(err) {
            $scope.orgStructureError = err.data.message;
        });
    }

    /* function search() {
     orgStructure.save(User.id, $scope.search).then(function(data) {
         $scope.searchResults = data.childList;
     }, function(err) {
         $scope.orgStructureError = err.data.message;
     });
 }
*/

    $scope.search = $location.search();

    if ($scope.search.name) {
        search();
    } else {
        $location.search({});
    }



});
