angular.module('organizationStructure').service('orgStructure', function($log, $resource, config, $q) {

    var organizationTree = null;

    var organizationStructure = $resource(config.baseUrl + '/org/:id/:hierarchy/:academicYear', { id: '@id', hierarchy: '@hierarchy', academicYear: '@academicYear' }, {});

    this.get = function(id, hierarchy, academicYear) {
        var defer = $q.defer();
        var params = {
            id: id,
            hierarchy: hierarchy,
            academicYear: academicYear
        };

        organizationStructure.get(params).$promise.then(function(data) {
            organizationTree = data;
            defer.resolve(data);
        }, function(err) {
            defer.reject(err);
        });

        return defer.promise;
    }

    this.getEmployeeDetails = function(id, callback) {
        var empDetails = null;
        $log.debug(organizationTree);

        if (organizationTree && organizationTree.childList.length > 0) {
            for (var i = 0; i < organizationTree.childList.length; i++) {
                if (organizationTree.childList[i].smartId.toString() == id) {
                    empDetails = organizationTree.childList[i];
                    break;
                }
            }
            callback(empDetails);
        } else {
            //make the web service call to fetch employee details using id
        }
    }


    var empSearch = $resource(config.baseUrl + '/org/search/:id/:academicYear/:hierarchy', { id: '@id', academicYear: '@academicYear', hierarchy: '@hierarchy' }, {
        'update': { method: 'PUT' }
    });

    this.save = function(id, data, academicYear, hierarchy) {
        var params = { id: id, academicYear: academicYear, hierarchy: hierarchy }
        return empSearch.save(params, data).$promise;
    }
});
