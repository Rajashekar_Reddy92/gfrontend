angular.module('organizationStructure', ['ui.bootstrap','ui.utils','ui.router']);

angular.module('organizationStructure').config(function($stateProvider) {

   $stateProvider.state('organizationStructure', {
        url: '/orgstructure',
        parent: 'app',
        templateUrl: 'organization_structure/partial/home.html',
        controller: 'hierarchydeciderCtrl',
        resolve: {
                template: function($q, User) {
                    var defer = $q.defer();
                    User.getUserInfo(function(user) {
                        var template = null;
                     
                         if (user.profile.role.toLowerCase() ==='admin' || user.profile.role.toLowerCase() ==='director')
                        {
                            console.log('user is admin')
                            template = 'organization_structure/partial/landing.html';
                        }
                        else
                        {
                            template='organization_structure/partial/decider.html';
                        }

                        defer.resolve(template);
                    });
                    return defer.promise;
                }   
            }



    })
    .state('organizationStructure.view', {
        url: '/view',
        templateUrl: 'organization_structure/partial/orgStructure.html',
        controller:'OrgstructureCtrl'
    })    
  
   .state('organizationStructure.view.search', {
        url: '/search',
        templateUrl: 'organization_structure/partial/search.html' 
    })
    .state('organizationStructure.view.employee', {
        url: '/:empId',
        templateUrl: 'organization_structure/partial/orgStructure.html',
         controller:'OrgstructureCtrl'
            
    })
    .state('employeeDetails', {
        url: '/employeeDetails/:empId',
        parent: 'app',
        templateUrl: 'organization_structure/partial/employeeDetails/employeeDetails.html',
        controller: 'EmployeeDetailsCtrl' 
    })
});

