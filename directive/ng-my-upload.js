 angular.module('gsmart').directive('fileModel', function($parse) {
     return {
         restrict: 'A',
         link: function(scope, element, attrs) {
             var model = $parse(attrs.fileModel);
             console.log(model)
             var modelSetter = model.assign;

             element.bind('change', function() {
                 scope.$apply(function() {
                     modelSetter(scope, element[0].files[0]);
                 });

                 console.log(scope);
             });
         }
     };
 });
 angular.module('gsmart').directive('phoneNumber', function() {
     return {
         require: 'ngModel',
         link: function(scope, element, attr, ngModelCtrl) {
             function fromUser(text) {
                 var transformedInput = text.replace(/[^0-9]/g, '');
                 // console.log(transformedInput);
                 if (transformedInput !== text) {
                     ngModelCtrl.$setViewValue(transformedInput);
                     ngModelCtrl.$render();
                 }
                 return transformedInput;
             }
             ngModelCtrl.$parsers.push(fromUser);
         }
     }
 });
 angular.module('gsmart').directive('onlyAlphabets', function() {
     return {
         require: 'ngModel',
         link: function(scope, element, attr, ngModelCtrl) {
             function fromUser(text) {
                 var transformedInput = text.replace(/[^A-Za-z ]/g, '');
                 //console.log(transformedInput);
                 if (transformedInput !== text) {
                     ngModelCtrl.$setViewValue(transformedInput);
                     ngModelCtrl.$render();
                 }
                 return transformedInput;
             }
             ngModelCtrl.$parsers.push(fromUser);
         }
     };
 });


 angular.module('gsmart').directive('capitalizeFirst', function($parse) {
     return {
         require: 'ngModel',
         link: function(scope, element, attrs, modelCtrl) {
             var capitalize = function(inputValue) {
                 if (inputValue === undefined) { inputValue = ''; }
                 var capitalized = inputValue.charAt(0).toUpperCase() +
                     inputValue.substring(1).toLowerCase();
                 if (capitalized !== inputValue) {
                     modelCtrl.$setViewValue(capitalized);
                     modelCtrl.$render();
                 }
                 return capitalized;
             }
             modelCtrl.$parsers.push(capitalize);
             capitalize($parse(attrs.ngModel)(scope)); // capitalize initial value
         }
     };
 });



 angular.module('gsmart').directive('capitalize', function($parse) {
     'use strict';

     return {
         require: 'ngModel',
         restrict: "A",
         link: function(scope, elem, attrs, modelCtrl) {

             /* Watch the model value using a function */
             scope.$watch(function() {
                 return modelCtrl.$modelValue;
             }, function(value) {

                 /**
                  * Skip capitalize when:
                  * - the value is not defined.
                  * - the value is already capitalized.
                  */
                 if (!isDefined(value) || isUpperCase(value)) {
                     return;
                 }

                 /* Save selection position */
                 var start = elem[0].selectionStart;
                 var end = elem[0].selectionEnd;

                 /* uppercase the value */
                 value = value.toUpperCase();

                 /* set the new value in the modelControl */
                 modelCtrl.$setViewValue(value);

                 /* update the view */
                 modelCtrl.$render();

                 /* Reset the position of the cursor */
                 elem[0].setSelectionRange(start, end);
             });

             /**
              * Check if the string is defined, not null (in case of java object usage) and has a length.
              * @param str {string} The string to check
              * @return {boolean} <code>true</code> when the string is defined
              */
             function isDefined(str) {
                 return angular.isDefined(str) && str !== null && str.length > 0;
             }

             function isUpperCase(str) {
                 return str === str.toUpperCase();
             }
         }
     };
 });

 angular.module('gsmart').filter('capitalizeFirstFilter', function() {
     return function(input) {
         return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
     };
 });
