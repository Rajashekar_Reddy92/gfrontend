angular.module('reportCard', ['appConfig', 'ui.bootstrap', 'ui.utils', 'ui.router', 'utils']);

angular.module('reportCard').config(function($stateProvider) {

    // $stateProvider.state('report-card', {
    //         url: '/reportcard',
    //         parent: 'app',
    //         templateUrl: 'report_card/partial/report-card.html',
    //         controller: 'ReportCardCtrl'
    //     })
    //     .state('report-card.viewresult', {
    //         url: '/viewresult',
    //         templateUrl: 'report_card/partial/viewresult.html'
    //     })
    //     .state('report-card.add', {
    //         url: '/add',
    //         templateUrl: 'report_card/partial/add-reportCard.html'
    //     })
    //     .state('report-card.edit', {
    //         url: '/edit',
    //         templateUrl: 'report_card/partial/edit-reportCard.html'
    //     })

    // .state('report-card.org', {
    //         url: '/org',
    //         templateUrl: 'report_card/partial/reportOrgStructure.html'
    //     })

    /* Add New States Above */
    $stateProvider.state('report-card', {
            url: '/reportcard',
            parent: 'app',
            templateUrl: 'report_card/partial/home.html',
            controller: 'ReportCardCtrl',
            resolve: {
                template: function($q, User) {
                    var defer = $q.defer();
                    User.getUserInfo(function(data) {
                        var template = null;
                        if (data.profile.role.toLowerCase() === 'student') {
                            template = 'report_card/partial/report-card.html';
                        } else if (data.profile.role.toLowerCase() === 'teacher') {
                            template = 'report_card/partial/report-card-teacher.html';
                        } else if (data.profile.role.toLowerCase() === 'hod') {
                            template = 'report_card/partial/myTeamReportCard.html';
                        }
                        defer.resolve(template);
                    });
                    return defer.promise;
                }
            }
        })
        .state('report-card.add', {
            url: '/add',
            templateUrl: 'report_card/partial/add-reportCard.html'
        })
        .state('report-card.edit', {
            url: '/edit',
            templateUrl: 'report_card/partial/edit-reportCard.html'
        })
        .state('report-card.viewresult', {
            url: '/viewresult',
            templateUrl: 'report_card/partial/viewresult.html'
        })
        .state('report-card.viewforhod', {
            url: '/viewforhod',
            templateUrl: 'report_card/partial/viewForHOD.html'
        })
        .state('report-card.viewresultforteacher', {
            url: '/viewresultforteacher',
            templateUrl: 'report_card/partial/viewResultForTeacher.html'
        })
        .state('report-card.view', {
            url: '/view',
            templateUrl: 'report_card/partial/report-card-teacher.html'
        })
        .state('report-card.pdf', {
            url: '/pdf',
            templateUrl: 'report_card/partial/pdf.html'
        })

});
