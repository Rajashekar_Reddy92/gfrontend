angular.module('reportCard').controller('ReportCardOrgCtrl', function($scope, $location, $stateParams,reportCardService,User) {

    $scope.childNotices = {};

    var parentId = $stateParams.empId ? $stateParams.empId : User.id;

    

    reportCardService.getOrg(parentId).then(function(data) {
        console.log('entered into report-org controller',data);
        $scope.currentEmp = data.selfProfile;
        console.log('selfProfile data from backend',data.selfProfile );
        $scope.people = data.childList;
        console.log( 'childList data from backend',data.childList );
        $scope.reportcard=data.childReportCards;
        console.log('childReportCards data from backend', data.childReportCards );
    }, function(err) {
        console.log('Error while fetching FeeOrg :', err);
    });
    


    $scope.showOrganizationTree = function(empId) {
        console.log($location.path('/reportcard/' + empId));
        $location.path('/reportcard/' + empId);
    }




});


