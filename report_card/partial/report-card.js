angular.module('reportCard').controller('ReportCardCtrl', function($scope, config, $http, $log, registration, template, $location, reportCardService, User, orgStructure) {
    $scope.reportCard = {};
    $scope.template = template;
    $scope.totalreportcards = 0;
    $scope.reportcardPerPage = 5;
    $scope.pageIndaex = 1;
    $scope.currentPage = 1;
    $scope.pageSize = 5;
    $scope.ispageChanged = true;
    $scope.maxVal = 10;
    $scope.minVal = 5;
    $scope.mixSize = 5;
    $scope.omkar = false;
    $scope.pagination = {
        current: 1
    };


    $scope.getReportCardz = function(acadYear, exmNm) {
            var minVal = ($scope.reportcardPerPage * ($scope.pageIndaex - 1));
            var maxVal = $scope.pageIndaex * $scope.reportcardPerPage;
            console.log("minVal :", minVal, "maxVal :", maxVal, "acadYear :", acadYear, "exmNm :", exmNm);
            reportCardService.getting({ "min": minVal, "max": maxVal }, acadYear.academicYear, exmNm.examName).then(function(data) {
                $scope.ReportCardInfo1 = data.reportCard.reportCardList;
                console.log($scope.ReportCardInfo1);
                $scope.totalReportCardCount = data.reportCard.count;
                $scope.ReportCard = data.modulePremission;
                $scope.AcdYear = data.reportCard.reportCardList[0].academicYear;
                $scope.ExmName = data.reportCard.reportCardList[0].examName;
                $location.path('/reportcard/viewresultforteacher');
            }, function(err) {
                console.log(err);
            });

        }
        //getReportCardz(1);

    $scope.reportcardPageChange = function(newPage) {
        console.log("newPage : ", newPage);
        $scope.isPageChanged = true;
        $scope.pageIndex = newPage;
        console.log("page number ", newPage);
        getReportCardz(newPage);
    };

    $scope.getReportCard = function(acyear, exam) {
        reportCardService.getReport(acyear.academicYear, exam.examName).then(function(data) {
            console.log(data);
            $scope.ReportCardInfo = data.reportCard;
            $scope.Percentage = data.totalGrade.totalGrade;
            $scope.Standard = data.reportCard[0].standard;
            $scope.Section = data.reportCard[0].section;
            $scope.AcdYear = data.reportCard[0].academicYear;
            $scope.ExmName = data.reportCard[0].examName;
            console.log($scope.Percentage);
            $scope.ReportCard = data.modulePremission;
            $location.path('/reportcard/viewresult');
        }, function(err) {
            console.log(err);
        });
    }

    $scope.getReportCardPDF = function(acyear, exam1) {
        console.log("year :", acyear);
        reportCardService.getReportPDF(acyear, exam1).then(function(data) {
            console.log("pdf may b irabahudu ", data)
            $scope.PDF = data.pdf;
        }, function(err) {
            console.log(err);
        });
    }

    reportCardService.getAcademicYear().then(function(data) {
        $scope.Year = data.academicYearAndExam;
        console.log($scope.Year);
    }, function(err) {
        console.log(err);
    });

    // $scop.ExamName = {};
    $scope.year = {};
    $scope.getexams = function(year) {
        reportCardService.getExam(year.academicYear).then(function(data) {
            console.log(data);
            $scope.ExamName = data.examName;
        }, function(err) {
            console.log(err);
        });
    }

    $scope.om = function() {
        console.log("hjiuiuiui");
        $scope.omkar = true;
    }

    $scope.year1 = {};
    $scope.getChildTeacher = function(year1) {
        reportCardService.getTeacher(year1.academicYear).then(function(data) {
            console.log(data);
            $scope.Teachers = data.childTeacherAndStandard;
        }, function(err) {
            console.log(err);
        });
    }

    $scope.getexamsForTeacher = function(year, profile1) {
        console.log(year, profile1);
        reportCardService.getexamsForTeacher(year.academicYear, profile1.smartId).then(function(data) {
            console.log(data);
            $scope.ExamName = data.examName;
        }, function(err) {
            console.log(err);
        });
    }

    $scope.getReportCardForHOD = function(acyear, exam) {
        reportCardService.getReportForHOD(acyear.academicYear, exam.examName, exam.smartId).then(function(data) {
            console.log(data);
            $scope.ReportCardInfo = data.reportCard;
            $scope.Percentage1 = data.totalGrade.totalGrade;
            console.log($scope.ReportCardInfo);
            $scope.Standard = data.reportCard[0].standard;
            $scope.Section = data.reportCard[0].section;
            $scope.AcdYear = data.reportCard[0].academicYear;
            $scope.ExmName = data.reportCard[0].examName;
            $location.path('/reportcard/viewforhod');
        }, function(err) {
            console.log(err);
        });
    }

    $scope.addReportCard = function() {
        $log.debug('in side controller add methode');
        $scope.reportCard = {};
        $location.path('/reportcard/add');
    }

    $scope.editReportCard = function(reportCard) {
        $scope.reportCard = angular.extend({}, reportCard);
        $location.path('/reportcard/edit');
    }


    $scope.delete = function() {
        console.log("for delete");
        reportCardService.delete($scope.currentReportCard).then(function(data) {
            console.log(" mahantesh is trying to best programmer ");
            $scope.reportCard = {};
            // getReportCardz(1);
            $location.path('/reportcard/view');
        }, function(err) {
            console.log('Error while deleting record..:', err);
        });
    }


    $scope.setCurrentReportCard = function(reportCard) {
        console.log("in side currentReportcard ");
        $scope.currentReportCard = reportCard;
    }

    $scope.save = function(event, form) {
        event.preventDefault();
        if (form.$valid) {
            reportCardService.save($scope.reportCard).then(function(data) {
                if (data.message == "success") {
                    $scope.reportCard = {};
                    $location.path('/reportcard/view');
                } else $scope.addReportCardFormSubmitError = data.message;
            }, function(err) {
                $scope.addReportCardFormSubmitError = err.message;
                console.log('Error while Adding  data:', err)
            })
        } else {
            angular.forEach(form.$error.required, function(field) {
                field.$setDirty();
            });
        }
    }

    //$scope.myFile = {};
    /*  $scope.uploadFile = function(myFile) {
           //console.log(myFile);
           var file = myFile;
           console.log(file);
           console.log("hi.........");
           reportCardService.uploadFileToUrl(file);

       };*/


    $scope.uploadFile = function(myFile) {
        //var file = $scope.myFile;
        console.log("myFile", myFile);
        console.dir(myFile);

        //var uploadFileUrl = config.baseUrl + '/reportCard/excelToDB';
        reportCardService.uploadFileToUrl(myFile);
    }


    $scope.update = function(event, form) {
        event.preventDefault();
        // $log.debug('in side update (controller).....')
        if (form.$valid) {
            reportCardService.update($scope.reportCard).then(function(data) {
                if (data.message == "success") {
                    $scope.reportCard = {};
                    //getReportCardz(1);
                    $location.path('/reportcard/view');
                } else $scope.editReportCardFormSubmitError = data.message;
            }, function(err) {
                $scope.editReportCardFormSubmitError = err.data.message;
                $log.debug('Error while updating hierarchy :', err);
            })
        } else {
            angular.forEach(form.$error.required, function(field) {
                field.$setDirty();
            });
        }
    }

    $scope.getInventoryByHierarchy = function(hierarchy) {
        console.log("selected hierarchy is : ", hierarchy);
        $scope.chosenHierarchy = hierarchy.hid;
        $location.path('/reportcard/view');
    }

    $scope.getStudents = function() {
        console.log("in controller ");
        reportCardService.getStudentsList().then(function(data) {
            $scope.StudentList = data;
            console.log("student-List:", data);
        }, function(err) {
            $log.debug(err);
        });
    }


    $scope.selectStudent = function(student) {
        $log.debug('search_student:', student);
        $scope.reportCard.studentName = student.firstName + " " + student.middleName + " " + student.lastName;
        $scope.reportCard.studentId = student.studentId;
        $scope.reportCard.smartId = student.smartId;
        $scope.reportCard.section = student.section;
        $scope.reportCard.standard = student.standard;
        //$('#myModal').modal('hide');
    }


    $scope.printInfo = function(invoice) {
        var printContents = document.getElementById(invoice).innerHTML;
        var popupWin = window.open('', '_blank', 'width=900,height=900');
        popupWin.document.open();
        popupWin.document.write('<html><head><meta charset="utf-8"> <meta name="viewport" content="width=device-width, initial-scale=1"> <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.css" /><link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap-theme.css" /><link rel="stylesheet" href="bower_components/bootstrap-sidebar/dist/css/sidebar.css" /><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"><link rel="stylesheet" type="text/css" href="css/style.css" /></head><body onload="window.print()">' + printContents + '</body></html>');
        popupWin.document.close();
    };

    $scope.showAcademicYearList = function() {
        var year = new Date().getFullYear();
        var currYear;
        $scope.yearList = [];
        for (var i = year; i > year - 10; i--) {
            currYear = i;
            $scope.yearList.push(currYear + "-" + (currYear + 1));

        }
    }

    $scope.showAcademicYearList();

});
