angular.module('reportCard').service('reportCardService', function($resource, User, config, $http) {

    console.log("service");
    var rcard = $resource(config.baseUrl + '/reportCard/:task', { task: '@task' }, {
        'update': { method: 'PUT' }
    });

    var getReportCardForTeacher = $resource(config.baseUrl + '/reportCard/forTeacher/:min/:max/:academicYear/:exam', { min: '@min', max: '@max', academicYear: '@academicYear', exam: '@exam' }, {
        'update': { method: 'PUT' }
    });


    this.uploadFile = function(data) {
        return rcard.save(data).$promise;
    }

    this.getStudentsList = function() {
        console.log("in side getStudentsList service");
        return rcard.query().$promise;
    }

    this.getting = function(params, academicYear, exam) {
        console.log(params, academicYear, exam)
        return getReportCardForTeacher.get(params, { academicYear: academicYear, exam: exam }).$promise;
    }

    this.save = function(data) {
        return rcard.save(data).$promise;
    }

    this.update = function(data) {
        return rcard.update({ task: "edit" }, data).$promise;
    }

    this.delete = function(data) {
        return rcard.update({ task: "delete" }, data).$promise;
    }

    var getYear = $resource(config.baseUrl + '/reportCard/academicYear', {}, {});

    this.getAcademicYear = function() {
        return getYear.get().$promise;
    }

    var getExamName = $resource(config.baseUrl + '/reportCard/examName/:academincYear', { academincYear: '@academincYear' }, {});

    this.getExam = function(academincYear) {
        console.log("in side service getExam methode..", academincYear);

        return getExamName.get({ academincYear: academincYear }).$promise;
    }

    var getReportCard = $resource(config.baseUrl + '/reportCard/:academincYear/:exam', { academincYear: '@academincYear', exam: '@exam' }, {});

    this.getReport = function(academincYear, exam) {
        console.log("in side service getExam methode..", academincYear);
        return getReportCard.get({ academincYear: academincYear, exam: exam }).$promise;
    }

    var getReportCardPDF = $resource(config.baseUrl + '/reportCard/download/:academincYear/:exam', { academincYear: '@academincYear', exam: '@exam' }, {});

    this.getReportPDF = function(academincYear, exam) {
            console.log("in side service getExam methode..", academincYear);
            return getReportCardPDF.get({ academincYear: academincYear, exam: exam }).$promise;
        }
        /*
            this.uploadFileToUrl = function(file) {
            console.log(file);
            var uploadUrl = config.baseUrl + '/reportCard/excelToDB';
            console.log('uploadUrl', uploadUrl);
            var fd = new FormData();
            fd.append('file', file);
            console.log(fd);
            $http.post(uploadUrl, fd, {
                    transformRequest: angular.identity,
                    headers: { 'Content-Type': undefined }
                })
                .success(function() {
                    alert('file uploaded successfully');
                })
                .error(function() {
                    alert("file upload failure");
                });
        }
        */
    var uploadUrl = $resource(config.baseUrl + '/reportCard/excelToDB', {}, {});

    this.uploadFileToUrl = function(file) {
        console.log(file);

        return uploadUrl.save(file).$promise;

    }

    var getTeacherList = $resource(config.baseUrl + '/reportCard/childTecher/:academincYear', { academincYear: '@academincYear' }, {});

    this.getTeacher = function(academincYear) {
        console.log("in side service getExam methode..", academincYear);

        return getTeacherList.get({ academincYear: academincYear }).$promise;
    }

    var getExamNameForTeacher = $resource(config.baseUrl + '/reportCard/examForTeacher/:academicYear/:smartId', { academicYear: '@academicYear', smartId: '@smartId' }, {});

    this.getexamsForTeacher = function(academicYear, smartId) {
        console.log("in side service getExam methode..", academicYear, smartId);

        return getExamNameForTeacher.get({ academicYear: academicYear, smartId: smartId }).$promise;
    }

    var getReportCardForHOD = $resource(config.baseUrl + '/reportCard/reportCardForHOD/:academincYear/:exam/:smartId', { academincYear: '@academincYear', exam: '@exam', smartId: '@smartId' }, {});

    this.getReportForHOD = function(academincYear, exam, smartId) {
        console.log("in side service getExam methode..", academincYear);
        return getReportCardForHOD.get({ academincYear: academincYear, exam: exam, smartId: smartId }).$promise;
    }

});
