angular.module('attendance').service('attendanceService', function($resource, config, $q, $log) {


    $log.debug("attendanceService");

    var attendance = $resource(config.baseUrl + '/attendance/calendar/:month/:year/:smartId', { smartId: '@smartId', month: '@month', year: '@year' }, {
        'update': { method: 'PUT' }
    });

    this.getAttendance = function(month, year, smartId) {
        var defer = $q.defer();
        var params = {
            smartId: smartId,
            year: year,
            month: month
        };
        attendance.get(params).$promise.then(function(data) {
            $log.debug("getAttendance response data is : ", data);
            defer.resolve(data);
        }, function(err) {
            defer.reject(err);
        });

        return defer.promise;
    }

    var attendance1 = $resource(config.baseUrl + '/attendance', {});

    this.saveattendance = function(data) {
        $log.debug(data);
        $log.debug('entered into attendance service');
        return attendance1.save(data).$promise;
    }

    var attendance2 = $resource(config.baseUrl + '/attendance/:task', { task: '@task' }, {
        'update': { method: 'PUT' }
    });

    this.editAttendance = function(data) {
        return attendance2.update({ task: "edit" }, data).$promise;
    }

    var attendanceOrg = $resource(config.baseUrl + '/attendance/:smartId', { smartId: '@smartId' }, {
        'update': { method: 'PUT' }
    });


    /*this.getAttendance= function(smartId) {
        var defer = $q.defer();
        var params = {
            smartId: smartId,  
        };
    }*/


    this.getAttendanceOrg = function(smartId) {
        var defer = $q.defer();
        var params = {
            smartId: smartId,
        };

        attendanceOrg.get(params).$promise.then(function(data) {
            $log.debug("getAttendance response data is : ", data);
            defer.resolve(data);
        }, function(err) {
            defer.reject(err);
        });


        return defer.promise;
    }



});
