angular.module('attendance').controller('MyattendanceCtrl', function($log, $scope, $location, User, $stateParams, $cookies, attendanceService) {

    console.log("controller for myattendance");

    $scope.childNotices = {};
    $scope.attendance = {};
    $scope.datesData = [];

    var parentId = $stateParams.empId ? $stateParams.empId : User.userInfo.profile.smartId;
    $log.debug("user ID :", parentId);
    var todayDate = new Date();
    getdates(todayDate.getMonth() + 1, todayDate.getFullYear(), parentId);

    var count = 0;

    $scope.showOrganizationTree = function(smartId) {
        console.log("employee id is" + smartId);
        $location.path('/attendance/myattendance' + smartId);
    }
    $scope.clear = function() {
        $scope.dt = null;
    };

    $scope.options = {
        dateDisabled: disabled,
        customClass: getDayClass,
        showWeeks: true

    };

    function disabled(data) {
        var date = data.date,
            mode = data.mode;
        return mode === 'day' && (date.getDay() === 0);
    }

    $scope.holidayDates = [];
    $scope.presentDates = [];

    function getdates(month, year, smartId) {
        $log.debug("myattendance month:", month);
        $log.debug("myattendance year:", year);
        $log.debug("myattendance smartId:", smartId);

        attendanceService.getAttendance(month, year, smartId).then(function(data) {
            $log.debug("myAttendance controller response data : ", data);
            $scope.presentDates = data.attendanceList;
            $log.debug("attendanceList:", data.attendanceList);
            $scope.holidayDates = data.holidayList;
            $log.debug('holidayList:', $scope.holidayDates);
            ++count;
            $scope.showCalendar = true;
        }, function(err) {
            $log.debug("myAttendance controller error block : ", err);
        });

    }

    $scope.editAttendance = function(dt) {
        $log.debug("editAttendance is edittable!", dt);
        $scope.dt = dt;
        var longDate = {};
        var longDate = new Date(dt);
        /*longDate.getTime()/1000;*/
        console.log(longDate.getTime() / 1000);
        $log.debug(longDate.getTime() / 1000);
        $scope.attendance.inDate = longDate.getTime() / 1000;
        $scope.attendance.smartId = $stateParams.empId ? $stateParams.empId : User.userInfo.profile.smartId;

        $log.debug('parentId', $scope.attendance.smartId);
        $log.debug($scope.attendance);
        attendanceService.editAttendance($scope.attendance).then(function(data) {
            $log.debug("editAttendance controller response data : ", data);

            if (data.message === 'success') {
                $scope.status = 'absent';

            }
        });
    }

    function getDayClass(data) {
        $scope.editDateObj = { 'date': $scope.dt, 'mode': 'day' };
        /* console.log("$scope.dt : ", $scope.dt);*/
        var date = data.date;
        var mode = data.mode;

        /* console.log("getDayClass data is : ", data);*/
        $scope.datesData.push(data);
        if (data.date.getDate() === 1) {
            getdates(data.date.getMonth() + 1, data.date.getFullYear(), parentId);
            setTimeout(function() {}, 600);
        }
        if ($scope.editDateObj && (new Date($scope.editDateObj.date).setHours(0, 0, 0, 0)) === new Date(date).setHours(0, 0, 0, 0)) {
            $log.debug("editing present to absent for : ", $scope.editDateObj.date);
            return $scope.status;
        }
        if ($scope.presentDates && $scope.presentDates.length > 0) {
            var dayToCheck = new Date(date).setHours(0, 0, 0, 0);
            if (mode === 'day' & date.getDay() != 0 & dayToCheck < new Date()) {
                var result = '';
                for (var i = 0; i < $scope.presentDates.length; i++) {
                    var currentDay = new Date($scope.presentDates[i].InDate * 1000).setHours(0, 0, 0, 0);
                    var isHoliday = false;
                    for (var j = 0; j < $scope.holidayDates.length; j++) {
                        var holidayDatess = new Date($scope.holidayDates[j].holidayDate).setHours(0, 0, 0, 0);
                        if (dayToCheck == holidayDatess) {
                            isHoliday = true;
                            result = 'holiday';
                            break;
                        }
                    }
                    if (dayToCheck == currentDay) {
                        result = 'present';
                        break;
                    } else if (!isHoliday) {
                        result = 'absent';
                    }
                }
                return result;
            }
        }
    }
});
