angular.module('registration').controller('attendanceCtrl', function($scope,template, User,HierarchyService, $filter, $stateParams, $location,$state, $rootScope)
 {
	$scope.template = template;

	function getHierarchys() {
        HierarchyService.get().then(function(data) {
            $scope.HierarchyInfo = data.hierarchyList;
            $scope.Permission = data.modulePermission;
            console.log(data)
            console.log($scope.HierarchyInfo)
        }, function(err) {
            console.log(err);
        });
    }
    getHierarchys();
     
     $scope.getInventoryByHierarchy = function(hierarchy) {
        console.log("selected hierarchy is : ", hierarchy);
        $scope.chosenHierarchy = hierarchy.hid;
        $location.path('/attendance/view');
    }
});