angular.module('attendance').controller('MyteamattendanceCtrl', function($log, User, $timeout, $scope, $location, User, $stateParams, attendanceService, $state) {

    $log.debug("controller for myteamattendance");
    $scope.present = true;
    $scope.childNotices = {};
    $scope.stateParams = {};
    $scope.people = [];
    $scope.childOfChildList = {};
    $scope.datesData = [];
    $scope.attendance = {};

    var organizationTree = null;

    var parentId = $stateParams.empId ? $stateParams.empId : User.id;

    $log.debug("parentId:", parentId);

    attendanceService.getAttendanceOrg(parentId).then(function(data) {
        $log.debug(parentId);

        $scope.currentEmp = data.selfProfile;
        $scope.people = data.childList;
        $scope.attendanceCountList = data.attendanceCount;
        console.log(data.attendanceCount)
        for (var i = 0; i < $scope.attendanceCountList.childList.length; i++) {

            for (var j = 0; j < $scope.people.length; j++) {

                if ($scope.attendanceCountList.childList[i].smartId === $scope.people[j].smartId) {
                    $scope.people[j].status = 'Present';
                }

            }

        }


        console.log('orgdaqta', data)
        $log.debug($scope.currentEmp);

        $log.debug($scope.people);
    }, function(err) {
        console.log('Error while fetching myteamattendance :', err);
    });

    var count = 0;

    $scope.showOrganizationTree = function(empId) {
        $log.debug("employee id is" + empId);
        $location.path('/attendance/myteamattendance' + empId);
    }


    $scope.getdates2 = function(smartId) {
        var parentId = smartId ? smartId : User.id;
        $state.go('attendance.myattendance', { empId: parentId });
    }





});
