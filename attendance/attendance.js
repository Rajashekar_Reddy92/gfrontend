angular.module('attendance', ['ui.bootstrap','ui.utils','ui.router','ngAnimate']);

angular.module('attendance').config(function($stateProvider) {


  $stateProvider.state('attendance', {
            url: '/attendance',
            parent: 'app',
            templateUrl: 'attendance/home.html'
            /*controller: 'attendanceCtrl'*/
        //  redirectTo: 'inventoryAssignments.list'
    /*resolve: {
                template: function($q, User) {
                    var defer = $q.defer();
                    User.getUserInfo(function(user) {
                        var template = null;*/
                        /*if (user.profile.role.toLowerCase() === 'student') {
                            
                            template = 'attendance/partial/inventory.html';
                        } 
                        else if(user.profile.role.toLowerCase() === 'teacher') {
                            
                            template = 'attendance/partial/inventory.html';
                        }
                        else*/ /*if (user.profile.role.toLowerCase() ==='admin' || user.profile.role.toLowerCase() ==='director')
                        {
                            console.log('user is admin')
                            template = 'attendance/partial/landing.html';
                        }
                        else
                        {
                            template='attendance/partial/landing.html';
                        }

                        defer.resolve(template);
                    });
                    return defer.promise;
                }   
            }*/



    })
    /*.state('attendance.view', {
        url: '/view',
        templateUrl: 'attendance/home.html'
    })*/
      .state('attendance.myattendance', {
            url: '/myattendance',
            templateUrl: 'attendance/partial/myattendance/myattendance.html',
            params: {
                empId: null
            },
            controller:'MyattendanceCtrl'
        })

      .state('attendance.myteamattendance', {
            url: '/myteamattendance',
            templateUrl: 'attendance/partial/myteamattendance/myteamattendance.html',
            controller:'MyteamattendanceCtrl'
        }) 
});

