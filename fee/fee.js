angular.module('fee', ['ui.bootstrap', 'ui.utils', 'ui.router']);

 angular.module('fee').config(function($stateProvider) {
 
     $stateProvider.state('fee', {
         url: '/fee',
        parent: 'app',
         templateUrl: 'fee/partial/home.html',
         controller: 'hierarchyDeciderCtrl',
        resolve: {
            template: function($q, User) {
                var defer = $q.defer();
                User.getUserInfo(function(user) {
                    var template = null;
                    if (user.profile.role.toLowerCase() === 'student') {
 
                        template = 'fee/partial/fee.html';
    
                    } else if(user.profile.role.toLowerCase()==='admin' || user.profile.role.toLowerCase()==='director') {
                        template = 'fee/partial/Landing.html';
                    }else{

                        template = 'fee/partial/decider.html';

                    }
 
                    defer.resolve(template);
               });
                return defer.promise;
            }
        }
    })

    .state('fee.decider', {
             url: '/decider',
             templateUrl: 'fee/partial/decider.html'
             
         })
    
        .state('fee.FeeStatus', {
             url: '/FeeStatus',
             templateUrl: 'fee/partial/fee-status.html',
             controller:'FeeCtrl'
         })
        .state('fee.FeeStatus.totalfee', {
             url: '/totalfee',
             templateUrl: 'fee/partial/totalfee.html'
        })
        .state('fee.FeeStatus.edit', {
             url: '/edit',
             templateUrl: 'fee/partial/edit-fee.html'
         })
        .state('fee.FeeStatus.add', {
             url: '/add',
             templateUrl: 'fee/partial/add-feestructure.html'
        })

        


    $stateProvider.state('fee.org', {
             url: '/org',
             params: {
               data: null
             },
             templateUrl: 'fee/partial/feeOrgStructure.html',
           controller: 'FeeOrgCtrl'
         })
        .state('fee.employee', {
             url: '/:empId',
             templateUrl: 'fee/partial/feeOrgStructure.html',
            controller: 'FeeOrgCtrl'
         })
 
 

});
