angular.module('fee').controller('FeeCtrl', function($scope, $log, $cookies, User, HierarchyService, $stateParams, FeeMasterService, template, FeeService, $location, $state) {


    $scope.fee = {};
    $scope.template = template;
    /* $scope.fee = {};*/
    $scope.feespayment = {};
    $scope.activeTab = 1;
    $scope.setActiveTab = function(tabToSet) {
        $log.debug(tabToSet);
        $scope.activeTab = tabToSet;
    };

    /*
        FeeMasterService.get({"min":0,"max":50,"hierarchy":$rootScope.chosenHierarchy}).then(function(data) {
            $scope.FeeInfo = data.feeList;
        }, function(err) {
            $log.debug(err);
        });*/
    /*
        function getTotalFeeDetails() {

            FeeService.feedetails().then(function(data) {
                $log.debug(data);
            }, function(err) {
                $log.debug(err);

            });
        }
        getTotalFeeDetails();*/


    $scope.totalFee = 0;
    $scope.feePerPage = 10;
    $scope.pageIndex = 1;
    $scope.currentPage = 1;
    $scope.pageSize = 5;
    $scope.isPageChanged = true;
    $scope.maxVal = 10;
    $scope.minVal = 5;
    $scope.maxSize = 5;

    /*Page change on click on the page number*/
    $scope.pagination = {
        current: 1
    };
    $scope.paidfeePageChange = function(newPage) {
        if($scope.feePerPage){
        console.log("newPage : ", newPage);
        $scope.isPageChanged = true;
        $scope.pageIndex = newPage;
        console.log("page number ", newPage);
        getPaidFee(newPage);
    }
    };


    function getPaidFee(pageNumber) {
        var minVal = ($scope.feePerPage * (pageNumber - 1));
        var maxVal = pageNumber * $scope.feePerPage;
        console.log("Get function called from controller")
        FeeService.getPaidFee({ "min": minVal, "max": maxVal, "hierarchy": $cookies.get("currenthierarchy") }).then(function(data) {

            console.log(data)
            $scope.PaidFeeInfo = data.data.PaidStudentsList.paidStudentsList;
            $scope.Permission = $cookies.getObject("modulePermission");
            $scope.totalpaidlist = data.data.PaidStudentsList.totalpaidlist;
            console.log($scope.PaidFeeInfo);
        }, function(err) {
            console.log("error block : ", err);
        });
    }
    getPaidFee(1);

    $scope.size = function(data){
        $scope.feePerPage = data;
    }

    $scope.unpaidfeePageChange = function(newPage) {
        if($scope.feePerPage){
        console.log("newPage : ", newPage);
        $scope.isPageChanged = true;
        $scope.pageIndex = newPage;
        console.log("page number ", newPage);
        getUnPaidFee(newPage);
    }
    };
    $scope.UnPaidFee = [];

    function getUnPaidFee(pageNumber) {
        var minVal = ($scope.feePerPage * (pageNumber - 1));
        var maxVal = pageNumber * $scope.feePerPage;
        console.log("Get function called from controller")
        FeeService.getUnPaidFee({ "min": minVal, "max": maxVal, "hierarchy": $cookies.get("currenthierarchy") }).then(function(data) {

            console.log(data)
            $scope.UnPaidFee = data.unpaidList.unpaidStudentsList;
            $scope.Permission = $cookies.getObject("modulePermission");
            console.log("dfghjkjgf", $scope.UnPaidPermission)
            $scope.totalunpaidlist = data.unpaidList.totalunpaidlist;
            console.log($scope.UnPaidFee);

            for (var i = 0; i < $scope.UnPaidFee.length; i++) {
                $scope.UnPaidFee[i].paidFee = $scope.UnPaidFee[i].totalFee - $scope.UnPaidFee[i].balanceFee;
            }

        }, function(err) {
            console.log("error block : ", err);
        });
    }

    getUnPaidFee(1);

    $scope.size1 = function(data){
        $scope.feePerPage = data;
    }



    $scope.PayFees = function(fee) {
        $scope.fee = angular.extend({}, fee);
        $scope.fee.paidFee = null;
        $scope.maxAmount = null;
        $scope.amountMsg = null;
        $log.debug("PayFees data : ", $scope.fee);
        $location.path('/fee/FeeStatus/edit');
    }

    $scope.goBack = function() {
        $scope.setActiveTab(1)
        $location.path('/fee/FeeStatus');
    }



    $scope.addFee = function() {
        $scope.fee = {};
        $scope.addFeeFormError = null;


        $location.path('/fee/add');
    }


    $scope.setFees = function(argument) {


        $scope.feespayment = _.find($scope.FeeInfo, { standard: argument.standard });
        $scope.fee.sportsFee = $scope.feespayment.sportsFee
        $scope.fee.tuitionFee = $scope.feespayment.tuitionFee
        $scope.fee.transportationFee = $scope.feespayment.transportationFee
        $scope.fee.miscellaneousFee = $scope.feespayment.miscellaneousFee
        $scope.fee.idCardFee = $scope.feespayment.idCardFee
        $scope.fee.totalFee = $scope.feespayment.totalFee

    }

    $scope.save = function(event, form) {
        event.preventDefault();
        /*  $scope.fee.smartId = "DPS1022"*/
        $log.debug('save called');
        if (form.$valid) {

            console.log($scope.fee);

            FeeService.savefees($scope.fee).then(function(data) {

                $scope.fee = {};
                alert("Success");
                $scope.getFees();
                $location.path('/fee')
            }, function(err) {
                $scope.addFeeFormError = err.data.message;
                console.log('Error while adding Fee :', err);
            })
        } else {
            angular.forEach(form.$error.required, function(field) {
                field.$setDirty();
            });
        }
    }

    $scope.update = function(event, feespay) {


        $log.debug("$scope.fee : ", $scope.fee);
        $log.debug("$scope.PaidFeeInfo : ", $scope.PaidFeeInfo);
        if ($scope.fee.paidFee != 0) {
            $scope.amountMsg = false;
            if ($scope.fee.paidFee <= $scope.fee.balanceFee) {
                $scope.maxAmount = false;
                if (feespay.$valid) {
                    //  console.log($scope.fee);
                    FeeService.editpaidfee($scope.fee).then(function(data) {
                        $log.debug("after edit function");

                        $scope.setActiveTab(1);
                        getPaidFee(1);
                        $location.path('/fee/FeeStatus');

                    });
                } else {
                    angular.forEach(feespay.$error.required, function(field) {
                        field.$setDirty();
                    });
                }
            } else {
                $scope.maxAmount = true;
            }

        } else {
            $scope.amountMsg = true;
            //alert('please enter valid amount');
        }

    }

    /*$scope.setCurrentFee = function(Pfee) {
        console.log(Pfee);
        $scope.currentfee = Pfee;
    }*/



});
