angular.module('fee').controller('hierarchyDeciderCtrl', function($scope, template, User, HierarchyService, $log, FeeService, $state, $cookies) {
    $scope.template = template;
    $scope.fee = {};

    $scope.user = User.userInfo.profile.role;
    console.log($scope.user)

    function getHierarchys() {
        HierarchyService.get().then(function(data) {
            $scope.HierarchyInfo = data.hierarchyList;
            console.log(data)
            console.log($scope.HierarchyInfo)
        }, function(err) {
            console.log(err);
        });
    }
    getHierarchys();

    if (User.userInfo.profile.role.toLowerCase() === "admin" || User.userInfo.profile.role.toLowerCase() === "director") {


    } else {
        $cookies.put("currenthierarchy", angular.toJson(0));

    }


    $scope.getFeeByHierarchy = function(hierarchy) {
        console.log("selected hierarchy is : ", hierarchy);
        $cookies.put("currenthierarchy", angular.toJson(hierarchy.hid));
        $state.go('fee.decider');
    }


    $scope.getFees = function(year) {
        getFee(year);
    }

    function getFee(year) {
        $scope.fee.smartId = User.id;
        $scope.fee.academicYear = year;
        console.log('feee', $scope.fee)
        FeeService.getStudentFees({ "smartId": $scope.fee.smartId, "academicYear": $scope.fee.academicYear }).then(function(data) {
            $scope.FeeStructureInfo = data.result;
            console.log('student Fee', $scope.FeeStructureInfo.length);
            var paidFees=0;
           $scope.paidFee={}

            for (var i = 0; i < $scope.FeeStructureInfo.length; i++) {
                

               
            paidFees=paidFees+parseInt($scope.FeeStructureInfo[i].paidFee);
             
            }
            $scope.paidFee=paidFees;
             $scope.totalFee=data.result[0].totalFee;
             $scope.balancefee= $scope.totalFee - $scope.paidFee;
          console.log($scope.paidFee);
          console.log($scope.balancefee);
          console.log($scope.totalFee);
        }, function(err) {
            $log.debug(err);
        });


    };

    getFee(new Date().getFullYear() + "-" + (new Date().getFullYear() + 1));

    $scope.showAcademicYearList = function() {
        var year = new Date().getFullYear();
        var currYear;
        $scope.yearList = [];
        for (var i = year; i > year - 10; i--) {
            currYear = i;
            $scope.yearList.push(currYear + "-" + (currYear + 1));

        }
    }

    $scope.showAcademicYearList();
});
