 angular.module('fee').controller('FeeOrgCtrl', function($scope,$cookies, User, $stateParams, FeeService, $location) {
 
     $scope.childNotices = {};

    $scope.totalBands = 0;
    $scope.feePerPage = 12;
    $scope.pageIndex = 1;
    $scope.currentPage = 1;
    $scope.pageSize = 5;
    $scope.isPageChanged = true;
    $scope.maxVal = 10;
    $scope.minVal = 5;
    $scope.maxSize = 5;

    /*Page change on click on the page number*/
    $scope.pagination = {
        current: 1
    };
    $scope.feePageChange = function(newPage) {
        console.log("newPage : ", newPage);
        if($scope.feePerPage){
        $scope.isPageChanged = true;
        $scope.pageIndex = newPage;
        console.log("page number ", newPage);
        // getCurrentAcademicData(newPage);
    }
    };
 
     var parentId = $stateParams.empId ? $stateParams.empId : User.userInfo.profile.smartId;
 
     $scope.getCurrentAcademicData = function(year) {
         console.log("currYear : ", year);
         console.log('hierarchy',$cookies.get("currenthierarchy"))
         FeeService.getOrg(parentId, year,$cookies.get("currenthierarchy")).then(function(data) {
             console.log("Data is :",data);
             $scope.currentEmp = data.selfProfile;
               console.log( "currentEmp is :",$scope.currentEmp );

               for (var i = 0; i < $scope.currentEmp.length; i++) {
                $scope.currentEmp[i].paidAmount = $scope.currentEmp[i].totalAmount - $scope.currentEmp[i].balanceAmount;
            }
             $scope.people = data.childProfile;
                console.log( $scope.people );
                for (var i = 0; i < $scope.people.length; i++) {
                $scope.people[i].paidAmount = $scope.people[i].totalAmount - $scope.people[i].balanceAmount;
            }

         }, function(err) {
             console.log('Error while fetching FeeOrg :', err);
         });
     }
 
     $scope.getCurrentAcademicData(new Date().getFullYear() + "-" + (new Date().getFullYear() + 1));
 
 
     $scope.showOrganizationTree = function(empId) {
         console.log($location.path('/fee/' + empId));
         $location.path('/fee/' + empId);
     }
 
 
     
 
 
 });