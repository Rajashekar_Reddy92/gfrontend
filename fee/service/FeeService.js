angular.module('fee').service('FeeService', function($resource, config,  $q, $log) {

    var Fee = $resource(config.baseUrl + '/fee/viewFee/:smartId/:academicYear', {smartId:'@smartId',academicYear:'@academicYear'},{
    });


    var getPaidFee = $resource(config.baseUrl +'/fee/paidfee/:min/:max/:hierarchy', {min: '@min', max: '@max',hierarchy: '@hierarchy'}, {
         'update': { method: 'PUT'}});

    this.getPaidFee = function(params) {
        console.log(params);
        return getPaidFee.get(params).$promise;
    }


    var getUnPaidFee = $resource(config.baseUrl +'/fee/unpaidfee/:min/:max/:hierarchy', {min: '@min', max: '@max',hierarchy: '@hierarchy'}, {
         'update': { method: 'PUT'}});

    this.getUnPaidFee = function(params) {
            console.log(params);
        return getUnPaidFee.get(params).$promise;
    }



    this.getStudentFees = function(params) {
        return Fee.get(params).$promise;
    }


    var Fee1 = $resource(config.baseUrl + '/fee', {});

    this.savefees = function(data) {
        $log.debug(data);
        $log.debug('entered into fee service');
        return Fee1.save(data).$promise;
    }


   
    


    this.getOrg = function(smartId, academicYear,hierarchy) {
        var defer = $q.defer();
        var params = {
            smartId: smartId,
            academicYear : academicYear,
            hierarchy:hierarchy
        };

        $log.debug("getOrg data : ", academicYear, smartId,hierarchy);

        var Fee2 = $resource(config.baseUrl + '/fee/feeOrg/'+smartId+'/'+academicYear+'/'+hierarchy);

        Fee2.get().$promise.then(function(data) {
            $log.debug(data);
            defer.resolve(data);
        }, function(err) {
            defer.reject(err);
        });

        return defer.promise;
    }
   


    var Fee5 = $resource(config.baseUrl + '/fee/:task/', { task: '@task' }, {
        'update': { method: 'PUT' }
    });
    this.editpaidfee = function(data) {
       // console.log(data);
        return Fee5.update({ task: "edit" }, data).$promise;
    }

/*var feedetail = $resource(config.baseUrl + '/fee/totalpaidfees', {
    });


    this.feedetails=function(){
        //console.log("hiii");
        return feedetail.get().$promise;
    }*/


});
