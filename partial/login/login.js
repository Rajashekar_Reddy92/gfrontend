angular.module('gsmart').controller('LoginCtrl', function($scope,$timeout, $location, User) {

    $scope.credentials = {};
    $scope.emailErrMsg = "Invalid email address"

    $scope.authenticateUser = function() {

        User.authenticate($scope.credentials.smartId, $scope.credentials.password, function(err, data) {

            $scope.loading = true;
            if (data.result === 0) {
                $scope.loading = false;
                $location.path('/home');
            } else if (data.result === 1) {
                $scope.loginError = data;
                $scope.ShowloginError = true;
                $timeout(function() {
                    $scope.loading = false;
                    $scope.ShowloginError = false;
                }, 4000);

            } else if (data.result === 2) {
                $scope.loginError = data;
                $scope.ShowloginError = true;
                $timeout(function() {
                    $scope.loading = false;
                    $scope.ShowloginError = false;
                }, 4000);
            } else {
                $scope.loginError = data;
                $scope.ShowloginError = true;
                $timeout(function() {
                    $scope.loading = false;
                    $scope.ShowloginError = false;
                }, 4000);
            }


        });

    }

    $scope.sendResetLink = function(email) {
        if(email && !$scope.showInvalidEmail) {
            console.log(email);
            User.sendResetLink(email).then(function(data) {
                if (data.status == 200) {
                    $scope.showValidEmail = true;
                    $scope.showInvalidEmail = false;
                    $scope.emailMsg=data.message;
                    $('#myModal').modal('hide');
                    $location.path('/login');
                    console.log("success response from sendResetLink : ", data);
                } else {
                    $scope.showInvalidEmail = true;
                    $scope.emailErrMsg = data.message;
                    console.log("error response from sendResetLink : ", data);
                }
                $location.path('/login');
            }).then(function(data) {
    });
    } else {
            $scope.showEmailReq = true;
        }
    }
    $scope.$watch('userEmail', function() {
        if ($scope.userEmail) {
            $scope.showEmailReq = false;
            $scope.showInvalidEmail = !(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test($scope.userEmail));
        } else {
            $scope.showInvalidEmail = false;
        }
    });

});
