angular.module('gsmart').controller('ProfileCtrl', function($log, $scope, $location, User, template, registration) {

    $log.debug('profileCtrl called');
    $log.debug(template);
    $log.debug(User);
    $scope.template = template;
    $scope.profile = User.userInfo.profile;
    $scope.profile.dob = new Date($scope.profile.dob);
    $scope.profile.joiningDate = new Date($scope.profile.joiningDate);
    $scope.profile.passportIssueDate = new Date($scope.profile.passportIssueDate);
    $scope.profile.passportExpiryDate = new Date($scope.profile.passportExpiryDate);
    $log.debug($scope.profile);

    $scope.update = function(event, registrationForm) {
        if (registrationForm.$valid) {
            console.log('$scope.profile', $scope.profile);
            registration.editProfile.update($scope.profile, function(data) {
                $scope.profiles = data;
                $location.path('/home')
            });
        } else {
            $log.debug('editProfile err' + registrationForm);
        }
    }

    $scope.imagetobase64 = function(image) {
        console.log('imagetobase64', image);
        $scope.profile.image = image.base64;
    }
});
