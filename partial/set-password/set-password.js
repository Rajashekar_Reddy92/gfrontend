angular.module('gsmart').controller('SetPasswordCtrl', function($scope,$log, $window,$location, $stateParams, SetPasswordService) {


    $scope.form = {};

    var url = $window.location.href;
     var index = url.lastIndexOf('/');
     var encryptedId = url.substring(index + 1, url.length);
     if(encryptedId.length > 0) {
        $scope.form.smartId = encryptedId;
        $log.debug("encryptedId is : ", encryptedId);
        $scope.showCurrPass = false;
     } else {
        $scope.showCurrPass = true;
        $log.debug("encryptedId not found");
     }

    

         $scope.setPassword = function(event, form) {
        if ($scope.setPasswordForm.$valid) {
            var reqObj = {};
            reqObj.smartId = $scope.form.smartId;
            reqObj.password = $scope.form.currentpassword;
            reqObj.confirmPassword = $scope.form.confirmPassword;
            $log.debug("reqObj : ", reqObj);
            SetPasswordService.save(reqObj).then(function(data) {
                $log.debug('success : ', data);
                $scope.success = true;
                $location.path("/");
            }, function(err) {
            $log.debug(err);
                $scope.error = err;
            });
        } else {
            $log.debug('form not valid');
        }
    }




});
