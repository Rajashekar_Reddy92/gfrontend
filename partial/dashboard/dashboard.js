angular.module('gsmart').controller('DashboardCtrl', function($stateParams, $cookies, $rootScope, User, $scope, $log, InventoryMasterService, InventoryService, FeeService, Dashboard, Notice, $timeout, attendanceService) {

    var arr = [];


    google.charts.load('current', { 'packages': ['corechart'] });
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
        var a = $scope.employeeProfiles;
        // console.log('entered into',employeeProfiles.length);
        var data = google.visualization.arrayToDataTable([
            ['Student Attendence', 'per Day'],
            ['Present Students', a],
            ['Absent Students', 100]
        ]);

        var options = {
            title: 'Students Attendence'
        };

        function getFeeDashboard() {
            //$logProvider.debugEnabled(false);
            //$scope.$log = $log;
            //$scope.$log.warn("this is info");

            var date = new Date();
            var academicYear = date.getFullYear() + '-' + (date.getFullYear() + 1);
            Dashboard.getFee(academicYear).then(function(data) {
                $log.debug("Fee dashboard data");
                if (data.status == 200) {
                    $log.debug("Success fee dashboard data : ", data);
                    $scope.feeDashboardList = data.data;
                    setTimeout(function() {
                        for (var i = 0; i < data.data.length; i++) {
                            var totalFees = data.data[i].totalFees;
                            paidFees = data.data[i].totalPaidFees;
                            unpaidFees = totalFees - paidFees;
                            schoolName = data.data[i].hierarchy.school;
                            google.charts.setOnLoadCallback(drawChart);

                            function drawChart() {
                                var data = google.visualization.arrayToDataTable([
                                    ['STATUS', 'per Year'],
                                    ['Paid Fee', paidFees],
                                    ['Unpaid Fee', unpaidFees]
                                ]);

                                var options = {
                                    title: schoolName
                                };

                                var chart = new google.visualization.PieChart(document.getElementById('feeChart_' + i));

                                chart.draw(data, options);
                            }
                        }
                    }, 1000);
                }
            }, function(err) {
                $log.debug(err);

            });
        }




        // function getInventoryDashboard1() {
        //     var date = new Date();
        //     var academicYear = date.getFullYear() + '-' + (date.getFullYear() + 1);
        //     Dashboard.getInventory(academicYear).then(function(data) {
        //         $log.debug("InventoryService data : ", data);
        //         if (data.status == 200) {
        //             $log.debug("InventoryService success block");
        //             $scope.inventoryDashboardList = data.data.inventoryList;
        //             setTimeout(function() {
        //                 for (var i = 0; i < data.data.inventoryList.length; i++) {
        //                     $scope.newArray = [];
        //                     $scope.newArray.push(['Item Type', 'Quantity']);
        //                     for (var j = 0; j < data.data.inventoryList[i].inventoryAssignmentList.length; j++) {
        //                         var itemType
        //                         itemType = [data.data.inventoryList[i].inventoryAssignmentList[j].itemType, data.data.inventoryList[i].inventoryAssignmentList[j].quantity];
        //                         $scope.newArray.push(itemType);
        //                     }
        //                     google.charts.setOnLoadCallback(drawChart);

        //                     function drawChart() {
        //                         $log.debug("newArray : ", $scope.newArray);
        //                         var chartData = google.visualization.arrayToDataTable($scope.newArray);
        //                         var options = {
        //                             title: data.data.inventoryList[i].hierarchy.school
        //                         };
        //                         var chart = new google.visualization.PieChart(document.getElementById('inventoryChart_' + i));
        //                         chart.draw(chartData, options);
        //                     }
        //                 }
        //             }, 1000);
        //         }
        //     }, function(err) {
        //         $log.debug(err);

        //     });
        // }

        function getInventoryDashboard() {
            var date = new Date();
            var academicYear = date.getFullYear() + '-' + (date.getFullYear() + 1);

            Dashboard.getInventory(academicYear).then(function(data) {
                    console.log(data);
                    $log.debug("InventoryService data : ", data);
                    if (data.status == 200) {
                        $scope.inventoryDashboardList = data.data.inventoryList;
                        console.log("in>>>>>>>>>>>>>", $scope.inventoryDashboardList);
                    }
                },
                function(err) {
                    $log.debug(err);

                });
        }
        getInventoryDashboard();


        function getAttendanceDashboard(todayDate) {
            $log.debug("todayDate : ", todayDate);
            Dashboard.getAttendance(todayDate).then(function(data) {
                $log.debug("getAttendanceDashboard : ", data);
                if (data.status == 200) {
                    $log.debug("Success fee dashboard data : ", data);
                    $scope.attendanceDashboardList = data.data;
                    setTimeout(function() {
                        for (var i = 0; i < data.data.length; i++) {
                            var totalCount = data.data[i].totalCount;
                            totalPresent = data.data[i].totalPresent;
                            totalAbsent = totalCount - totalPresent;
                            schoolName = data.data[i].hierarchy.school;

                            google.charts.setOnLoadCallback(drawChart);

                            function drawChart() {
                                var data = google.visualization.arrayToDataTable([
                                    ['ATTENDANCE', 'PER DAY'],
                                    ['PRESENT', totalPresent],
                                    ['ABSENT', totalAbsent]

                                ]);

                                var options = {
                                    title: schoolName
                                };

                                var chart = new google.visualization.PieChart(document.getElementById('attendanceChart_' + i));

                                chart.draw(data, options);
                            }
                        }
                    }, 1000);
                }
            }, function(err) {
                $log.debug(err);
            });
        }

        
        $scope.getDashboardData = function(selectedTab) {
            if (selectedTab == 1) {
                $log.debug("attendance dashboard");
                var today = new Date();
                var myToday = new Date(today.getFullYear(), today.getMonth(), today.getDate(), 0, 0, 0);
                getAttendanceDashboard(myToday.getTime() / 1000);


            } else if (selectedTab == 2) {

                getFeeDashboard();

            } else if (selectedTab == 3) {
                getInventoryDashboard();
            }
        }


        $scope.setActiveTab = function(setTab) {
            $scope.activeTab = setTab;
            $scope.getDashboardData(setTab);
        }

        $scope.setActiveTab(1);

    }

    $scope.tab = 1;

    $scope.setTab = function(newTab) {
        $scope.tab = newTab;
    };

    $scope.isSet = function(tabNum) {
        return $scope.tab === tabNum;
    };



    //Inventory details for student//
    var inventoryTree = null;
    $scope.empId = $stateParams.empId;
    $scope.employee = null;
    $scope.people = [];
    $scope.childOfChildList = {};
    $scope.inventories = {};
    $rootScope.InventoryInfo1 = [];
    $scope.inventoryAssignments = {};
    $scope.inventory = {};
    $scope.totalInventoryassign = 0;
    $scope.inventoryassignPerPage = 5;
    $scope.pageIndex = 1;
    $scope.currentPage = 1;
    $scope.pageSize = 5;
    $scope.isPageChanged = true;
    $scope.maxVal = 10;
    $scope.minVal = 5;
    $scope.maxSize = 5;
    $rootScope.InventoryInfoAssign = [];
    $scope.pagination = {
        current: 1
    };
    $scope.inventoryassignPageChange = function(newPage) {
        console.log("newPage : ", newPage);
        $scope.isPageChanged = true;
        $scope.pageIndex = newPage;
        console.log("page number ", newPage);
        getInventoryAssign(newPage);
    };
    function getInventoryAssign(pageNumber) {
        var minVal = ($scope.inventoryassignPerPage * (pageNumber - 1));
        var maxVal = pageNumber * $scope.inventoryassignPerPage;
        console.log("Get function called from controller")
        InventoryService.get({ "min": minVal, "max": maxVal }).then(function(data) {
                console.log("i am gettig data", data);
                $scope.InventoryInfoAssign = data.data.inventoryList.inventoryList;
                $scope.Permission = $cookies.getObject("modulePermission");
                $scope.totalinventory = data.data.inventoryList.totalinventoryassign;
                console.log($scope.InventoryInfoAssign);
            },
            function(err) {
                console.log(err);
            });
    }
    getInventoryAssign(1);



    //Fee details for student//
    $scope.fee = {};
    getFee(new Date().getFullYear() + "-" + (new Date().getFullYear() + 1));
    $scope.getFees = function(year) {
        getFee(year);
    }

    function getFee(year) {
        $scope.fee.smartId = User.id;
        $scope.fee.academicYear = year;
        console.log('feee', $scope.fee)
        FeeService.getStudentFees({ "smartId": $scope.fee.smartId, "academicYear": $scope.fee.academicYear }).then(function(data) {
            $scope.FeeStructureInfo = data.result;
            console.log('student Fee', $scope.FeeStructureInfo)
        }, function(err) {
            $log.debug(err);
        });

    };
    $scope.showAcademicYearList = function() {
        var year = new Date().getFullYear();
        var currYear;
        $scope.yearList = [];
        for (var i = year; i > year - 10; i--) {
            currYear = i;
            $scope.yearList.push(currYear + "-" + (currYear + 1));

        }
    }

    $scope.showAcademicYearList();

});
