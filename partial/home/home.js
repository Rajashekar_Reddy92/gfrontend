angular.module('gsmart').controller('HomeCtrl', function($scope, $cookies, $timeout, $rootScope, $log, User, $location, NoticeService, registration) {

    $scope.currentPage = $location.path().split('/')[1];

    User.getUserInfo(function(userInfo) {
        $log.debug("Home cntrl");
        $log.debug(userInfo);
        $scope.modules = userInfo.permissions;
        $scope.details = userInfo.profile;

        $timeout(function() { $scope.logout(); }, 1.8e+7);
    });

    /*    NoticeService.get().then(function(data) {
            console.log("notices displaying");
            $scope.noticeInfo = data.selfNotice;
            console.log("$rootScope.NoticeInfo : ", $rootScope.NoticeInfo);
        }, function(err) {
            console.log(err);
        });*/
    $scope.imagetobase64 = function(image) {
        console.log('imagetobase64', image);
        $scope.details.image = image.base64;
        console.log($scope.details)
        registration.changeimage($scope.details).then(function(data) {
            console.log('success', data)
        })
    }

    $scope.logout = function() {
        $log.debug('logout called')
        User.unidentify();
    }

    $scope.setModulePermission = function(module) {
        console.log('hygfdsdtyuiuyfvuy', module)
        $cookies.putObject("modulePermission", module);
        // location.href("#/moduleName")

    }

    $rootScope.$on('$stateChangeSuccess', function() {
        $scope.currentPage = $location.path().split('/')[1];
    });

    /*window.onbeforeunload = function(e) {
    console.log("reload called");
    if (navigator.onLine) {
        $rootScope.online = 'online';
    } else {
        $rootScope.online = 'offline';
    }
};
*/

});
