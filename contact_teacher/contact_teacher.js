angular.module('contactTeacher', ['ui.bootstrap','ui.utils','ui.router']);

angular.module('contactTeacher').config(function($stateProvider) {

    $stateProvider.state('contactteacher', {
    	parent:'app',
        url: '/contactteacher',
        templateUrl: 'contact_teacher/partial/contact-teacher.html',
        controller: 'ContactTeacherCtrl'
    })


    $stateProvider.state('messages', {
        url: '/messages',
        templateUrl: 'contact_teacher/partial/contact.html',
         controller: 'ContactTeacherCtrl'
    })
    .state('messages.list', {
        url: '/',
        templateUrl: 'contact_teacher/partial/message-list.html'
    })
    .state('contactteacher.chat', {
        url: '/chat',
        templateUrl: 'contact_teacher/partial/chat.html'
    })
    .state('messages.studentlist', {
        url: '/studentlist',
        templateUrl: 'contact_teacher/partial/studentlist.html'
    })
     .state('messages.send', {
        url: '/send',
        templateUrl: 'contact_teacher/partial/tinitialchat.html'
    })
    .state('messages.contactstructure', {
        url: '/contactstructure',
        templateUrl: 'contact_teacher/partial/contact-structure.html'       
    }) 
    .state('messages.messagestructure', {
        url: '/messagestructure',
        templateUrl: 'contact_teacher/partial/message-structure.html'        
    })    
    .state('contactteacher.student',{
         url:'/student',
         templateUrl:'contact_teacher/partial/msgListTeacher.html '
    })    
    .state('contactteacher.teacher',{
         url:'/teacher',
         templateUrl:'contact_teacher/partial/msgListStudent.html'
    })
});

