
angular.module('contactTeacher').controller('ContactTeacherCtrl',function($scope,User,$stateParams,$location,$window,$cookies,ContactTeacherService){

$scope.MessageDetails={};

    var organizationTree = null;
    $scope.empId = $stateParams.empId;
    $scope.employee = null;
    $scope.people = [];
    $scope.childOfChildList = {};
    $scope.messagesPerPage = 5;
   /* $scope.totalMsg = 0; 
    $scope.pageIndex = 1;
    $scope.currentPage = 1;
    $scope.pageSize = 5;
    $scope.isPageChanged = true;
    $scope.maxVal = 10;
    $scope.minVal = 5;
    $scope.maxSize = 5;
    $scope.msgDetails= {};
    $scope.form = {};

    $scope.pagination = {
        current: 1
    };*/

    /*$scope.messagePageChange = function(newPage) {
        console.log("newPage : ", newPage);
        $scope.isPageChanged = true;
        $scope.pageIndex = newPage;
        console.log("page number ", newPage);
        studentMsg(newPage);
    };*/

 $scope.MessageDetails.reportingManagerId = User.userInfo.profile.reportingManagerId;
    console.log("userId:",User.id)
    console.log(' Id of message details :', $scope.MessageDetails.reportingManagerId);

    ContactTeacherService.getMsgs.view($scope.MessageDetails, function(data) {
        console.log("reporting ID :",$scope.MessageDetails);
        /*console.log(data);*/
        $scope.msgData = data;
        console.log($scope.msgData);

    });


 $scope.save = function(event,form,MessageDetails) {
        console.log("data saving ",MessageDetails)
       console.log("data is going for save");
          event.preventDefault();
         if (form.$valid){
          console.log("data is validating");
          User.userInfo.profile.smartId;
        $scope.MessageDetails.smartId=$cookies.get('userId');
        /*$scope.MessageDetails.image=$scope.MessageDetails.image.base64;*/
        $scope.MessageDetails.smartId = User.userInfo.profile.smartId;
        if($scope.MessageDetails.image) {
            $scope.MessageDetails.image=$scope.MessageDetails.image.base64;
        } 

        $scope.MessageDetails.reportingManagerId=User.userInfo.profile.reportingManagerId;
        $scope.MessageDetails.studentName=User.userInfo.profile.firstName;
        /*var object = {'info.image','info.pdf'}*/
        /*$scope.MessageDetails.image= $scope.MessageDetails.image.format;*/
           console.log("image data",$scope.MessageDetails);

        ContactTeacherService.postMsg.add($scope.MessageDetails, function(data) {
          console.log("Data is saved",data);
            if (data.result === 'MSG Posted') {
              alert("Message Posted")
              if ($scope.MessageDetails.message !== "") {
                  $scope.MessageDetails.message = "";
                  /*$scope.addMessageForm.$setPristine();*/  
             }
                {{data.result}}
                {{data.role}}
            }
        });
      }
       else {
             angular.forEach(form.$error.required, function(field) {
             field.$setDirty();
          });
    }
  }


    $scope.download=function (file) {

      var image_data = atob(file.split(',')[1]);
      console.log('file',image_data)
    // Use typed arrays to convert the binary data to a Blob
    var arraybuffer = new ArrayBuffer(image_data.length);
    var view = new Uint8Array(arraybuffer);
    for (var i=0; i<image_data.length; i++) {
        view[i] = image_data.charCodeAt(i) & 0xff;
    }
    try {
        // This is the recommended method:
        var blob = new Blob([arraybuffer], {type: 'application/octet-stream'});
    } catch (e) {
        // The BlobBuilder API has been deprecated in favour of Blob, but older
        // browsers don't know about the Blob constructor
        // IE10 also supports BlobBuilder, but since the `Blob` constructor
        //  also works, there's no need to add `MSBlobBuilder`.
        var bb = new (window.WebKitBlobBuilder || window.MozBlobBuilder);
        bb.append(arraybuffer);
        var blob = bb.getBlob('application/octet-stream'); // <-- Here's the Blob
    }

    // Use the URL object to create a temporary URL
    var url = (window.webkitURL || window.URL).createObjectURL(blob);
    location.href = url; // <-- Download!
    //   console.log('file',file);
    // var data = 'some data here...',

    //     blob = new Blob([file], { type: 'text/plain' }),
    //     url = $window.URL || $window.webkitURL;
    // $scope.fileUrl = url.createObjectURL(blob);
    };

     $scope.chatMsgstudent = function(info) {
        ContactTeacherService.schat.view2(info, function(data) {
        $scope.chatData=data;
        console.log("message",data);
        $location.path('/contactteacher/chat');
        
    });
     }

    $scope.chatMsgteacher = function(info){
      ContactTeacherService.tchat.view22(info, function(data){
        $scope.chatData=data;
        console.log("messages",data);
        $location.path('/contactteacher/chat');
      });
    }

//Teacher Message to Student-Dynamic
     $scope.teachMsg = function(event,form) {
      /*var minVal = ($scope.messagePerPage * (pageNumber - 1));
      var maxVal = pageNumber * $scope.messagePerPage;*/
        event.preventDefault();
/*     $scope.MessageDetails.image=$scope.MessageDetails.image.base64;
*/     $scope.chatData = $scope.chatData;
       $scope.smartId = $scope.smartId;
        if($scope.MessageDetails.image) {
            $scope.MessageDetails.image=$scope.MessageDetails.image.base64;
        }
        console.log(form);
        console.log('teachMsg form image', $scope.MessageDetails);
       /* console.log('chatData  cc', $rootScope.chatDatas)*/
        if (form.$valid){
          console.log("teacher Msg");
/*         $scope.MessageDetails.smartId=$scope.chatData.result[0].smartId;
*/         ContactTeacherService.tMsg.view($scope.MessageDetails, function(data) {
          alert("Message Posted")
          console.log("image data",data);
          console.log(data.messages);
           /*$scope.Permission = data.modulePermission;*/
           $scope.data=data.messages;
          if ($scope.messages.messages !== "") {
                  $scope.messages.messages = " ";
                  //$scope.teacherMesForm.$setPristine();  
              }
    });
     }
     else {
             angular.forEach(form.$error.required, function(field) {
             field.$setDirty();
          });
     }
      }


     $scope.studentMsg = function() {
      console.log("we are getting teacher  Data");
      /*var minVal = ($scope.messagePerPage * (pageNumber - 1));
      var maxVal = pageNumber * $scope.messagePerPage;*/
        $scope.MessageDetails.smartId=User.userInfo.profile.smartId;
        $scope.MessageDetails.reportingManagerId=User.userInfo.profile.reportingManagerId;
        $scope.MessageDetails.image =$scope.MessageDetails.image;
        if(User.userInfo.profile.role.toLowerCase() == 'student') {
          console.log("role smartID",User.userInfo.profile.role);
          ContactTeacherService.sMsg.view4($scope.MessageDetails, function(data) {
            console.log("message details",$scope.MessageDetails);
            /*console.log("Student List:",data.result);
            console.log('MessageDetails',data.result.message);
            console.log('message size',data.result.messages);
            $scope.sData = data.result.message;
            $scope.data=data.result.messages;*/
            $scope.sData = data.result;
            $scope.Permission = data.modulePermission;
            console.log("result",data.result);
          });
        } else if(User.userInfo.profile.role.toLowerCase() == 'teacher') {
          console.log("role smartID",User.userInfo.profile.role);
          ContactTeacherService.tMsg.view3($scope.MessageDetails, function(data) {
             console.log("message details",$scope.MessageDetails);
            /*console.log("Teacher List:",data.result);
            console.log('MessageDetails',data.result.message);
            console.log('message size',data.result.messages);
            $scope.sData = data.result.message;
            $scope.data=data.result.messages;*/
            $scope.sData = data.result;
            $scope.Permission = data.modulePermission;
            console.log("result",data.result);
          });
        }
    }
  
   /* $scope.teachMsg = function(){
      console.log("we are getting student Data");
      User.userInfo.profile.smartId;
      $scope.MessageDetails.smartId = $cookies.get('userId');
      $scope.MessageDetails.reportingManagerId=User.userInfo.profile.reportingManagerId;
      ContactTeacherService.tMsg.view4($scope.MessageDetails, function(data1){
        $scope.tData = data1;
      });
    }*/

   /* $scope.studentlist = function() {
        $scope.studentlist = {};
        $location.path('/messages/studentlist');
    }*/

     /*$scope.tMsg = function(p) {
         $scope.MessageDetails.smartId=p.smartId;
        $scope.MessageDetails.reportingManagerId=p.reportingManagerId;
        ContactTeacherService.chat.view2($scope.MessageDetails, function(data) {
        $scope.tData=data;
        $location.path('/messages/send');
    });
     }
*/
      /*$scope.teachinitialMsg = function(event,form,MessageDetails) {
        event.preventDefault();
        if (form.$valid){
          $log.debug('start')
          $log.debug($scope.MessageDetails);
          $log.debug('start')
         ContactTeacherService.tMsg.view3($scope.MessageDetails, function(data) {
          alert("Message Posted")
          if ($scope.MessageDetails.message !== "") {
              $scope.MessageDetails.message = " ";
              }
    });
     }
     else {
             angular.forEach(form.$error.required, function(field) {
             field.$setDirty();
          });
     }
      }*/

      $scope.uploadFile = function(files) {
      console.log('files',files);

           $scope.MessageDetails.format=files[0].type;
           files[0].type; //MIME type
           files[0].size; //File size in bytes
      }; 

//Contact Structure

 /*function loadChildren() {
        organizationTree.get({}, function(data) {
            $scope.currentEmp = data.result.selfProfile;
            $scope.people = data.result.childList;
            $scope.msgDetails = data.result.childMessages;
        });
    }

    if ($stateParams.empId) {
        organizationTree = ContactTeacherService.organisationTree($stateParams.empId);
        loadChildren();
    }
    else {
        organizationTree = ContactTeacherService.organisationTree(User.id);
        loadChildren();
    }

    $scope.showOrganizationTree = function(empId) {
        $log.debug(empId);
        $location.path('messages/contactstructure/' + empId);
    }
*/
});
