angular.module('contactTeacher').service('ContactTeacherService',function($resource,config,$cookies) {

	this.postMsg = $resource('/gsmart/contact/studentToTeacher', {}, {
        add: { method: 'POST' }
    });

   this.getMsgs = $resource('/gsmart/contact/msgListForTeacher', {}, {
        view: { method: 'POST' }
    }); 

	this.schat = $resource('/gsmart/contact/teacherView', {}, {
        view2: { method: 'POST' }
    }); 

    this.tchat = $resource('/gsmart/contact/studentView', {}, {
        view22: { method: 'POST' }
    }); 

    /*this.postMsg = $resource('/gsmart/contact/teacherToStudent', {}, {
        add: { method: 'POST' }
    });*/

    this.tMsg = $resource('/gsmart/contact/teacherView', {}, {
        view3: { method: 'POST' }
    });  

    this.sMsg = $resource('/gsmart/contact/studentView', {}, {
        view4: { method: 'POST' }
    });


    /*var sMsg = $resource(config.baseUrl+'/contact/studentView/', {}, {
        view4: { method: 'POST' }
    });

    this.getStudentMessage = function (data, minMax) {
        console.log("minMax : ", minMax); 
        return sMsg.view4({min : minMax.min, max : minMax.max}, data).$promise;
    }

   var tMsg = $resource(config.baseUrl+'/contact/teacherView/', {}, {
        view3: { method: 'POST' }
    }); 

   this.getTeacherMessage = function (data, minMax) {  
        return tMsg.view3({min : minMax.min, max : minMax.max}, data).$promise;
    }*/


   /* this.organisationTree = function (id) {
        return $resource('/gsmart/contactSearch/info/' + id, {}, {
            get: { method: 'POST', headers: { 'Content-Type': 'application/json' } }
        })
    } */

    /*this.organisationTree = function (id) {
        return $resource('/gsmart/contactSearch/info/' + id, {}, {
            get: { method: 'POST', headers: { 'Content-Type': 'application/json' } }
        })
    } 
        })*/
     
});