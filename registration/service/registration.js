angular.module('registration').service('registration', function($log, $resource, $cookies, config) {

    console.log("service");
    var reg = $resource(config.baseUrl + '/registration', {}, {

    });

    var getEmployees = $resource(config.baseUrl + '/registration/employee/:min/:max/:hierarchy', { min: '@min', max: '@max', hierarchy: '@hierarchy' }, {
        'update': { method: 'PUT' }
    });

    this.getEmp = function(params) {
        console.log()
        return getEmployees.get(params).$promise;
    }

    var getStudents = $resource(config.baseUrl + '/registration/student/:min/:max/:hierarchy', { min: '@min', max: '@max', hierarchy: '@hierarchy' }, {
        'update': { method: 'PUT' }
    });

    this.get = function(params) {
        return getStudents.get(params).$promise;
    }


    var getStaffLists = $resource(config.baseUrl + '/assign/staff', {}, {});

    this.getStaffList = function(data) {
        return getStaffLists.save(data).$promise;
    }

    /*var getStudentProfilesReg = $resource(config.baseUrl + '/registration/student', {}, {});

    this.getStudentProfiles = function() {
        return getStudentProfilesReg.get().$promise;
    }


    var getEmployeeProfilesReg = $resource(config.baseUrl + '/registration/employee', {}, {});

    this.getEmployeeProfiles = function() {
        return getEmployeeProfilesReg.get().$promise;
    }
*/

    var addProfile = $resource(config.baseUrl + '/registration/addProfile/:hierarchy', { hierarchy: '@hierarchy' }, {});

    this.add = function(data, hierarchy) {
        console.log(data)
        console.log(hierarchy)
        var params = {
            hierarchy: hierarchy
        }
        return addProfile.save(params, data).$promise

    }

    this.editProfile = $resource(config.baseUrl + '/registration/updateProfile/' + $cookies.get('userId'), {}, {
        update: { method: 'POST' }

    })
    this.search = $resource(config.baseUrl + '/registration/searchRep/', {}, {
        add: { method: 'POST' }
    })
    var deleterecord = $resource(config.baseUrl + '/registration/delete/:task/', { task: '@task' }, {
        update: { method: 'PUT' }
    })
    this.deleteProfile = function(data) {
        console.log("delete function");
        console.log(data);
        return deleterecord.update({ task: "delete" }, data).$promise;
    }

    var standardfee = $resource(config.baseUrl + '/assign/searchstandardfee/:hierarchy/:standard', { standard: '@standard', hierarchy: '@hierarchy' }, {});

    this.standardfeesearch = function(standard, hierarchy) {
        var params = {
            standard: standard,

            hierarchy: hierarchy
        }
        console.log(params)
        $log.debug('standardfeesearch in registration service :', params);
        return standardfee.get(params).$promise;
    }

    var imagechange = $resource(config.baseUrl + '/registration/changeprofileimage', {}, {
        update: { method: 'PUT' }
    });
    this.changeimage = function(data) {
        console.log(data);
        return imagechange.save(data).$promise;
    }

});
