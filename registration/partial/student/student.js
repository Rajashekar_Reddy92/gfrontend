angular.module('registration').controller('StudentCtrl', function($log, $timeout, $scope, $cookies, registration, $location, BandService, assign, HierarchyService) {
    $scope.profile = {};

    $scope.totalStudents = 0;
    $scope.studentsPerPage = 10;
    $scope.pageIndex = 1;
    $scope.currentPage = 1;
    $scope.pageSize = 5;
    $scope.isPageChanged = true;
    $scope.maxVal = 10;
    $scope.minVal = 5;
    $scope.maxSize = 5;

    /*Page change on click on the page number*/
    $scope.pagination = {
        current: 1
    };
    $scope.studentPageChange = function(newPage) {
        if ($scope.studentsPerPage) {
            console.log("newPage : ", newPage);
            $scope.isPageChanged = true;
            $scope.pageIndex = newPage;
            console.log("page number ", newPage);
            getStudents(newPage);
        }
    };

    function getStudents(pageNumber) {
        var minVal = ($scope.studentsPerPage * (pageNumber - 1));
        var maxVal = pageNumber * $scope.studentsPerPage;
        console.log("Get function called from controller")
        registration.get({ "min": minVal, "max": maxVal, "hierarchy": $cookies.get("currenthierarchy") }).then(function(data) {
            console.log(data)
            $scope.profileInfo = data.result.profileMap1;
            $scope.Permission = $cookies.getObject("modulePermission");
            $scope.totalProfiles = data.result.totalProfiles;
            $scope.loading = false;
            console.log($scope.profileInfo);
        }, function(err) {
            console.log("error block : ", err);
        });
    }
    getStudents(1);

    $scope.size = function(data) {
        $scope.studentsPerPage = data;
    }

    $scope.search2 = function(data) {
        $scope.studentsPerPage = $scope.totalProfiles;
    }


    $scope.currentInstitution = {};
    $scope.currentSchool = {};
    $scope.currentBand = {};
    $scope.currentRole = {};
    $scope.currentDesignation = {};

    $scope.currentClassName = {};
    $scope.currentSection = {};
    $scope.search = {};
    $scope.validDate = true;
    $scope.loading = true;
    $scope.dateformat = "dd/MM/yyyy";
    $scope.showcalendar = function($event) {
        $scope.showdp = true;
        /*setTimeout(function(){
            $scope.dateformat="dd/MM/yyyy";
            $scope.showdp = false;
            $scope.today();
            console.log("$scope.showdp : ",$scope.showdp );
        },5000);*/
    };
    $scope.showdp = false;
    // $scope.today = function () {
    //     $scope.profile.dob= new Date();
    // };
    //  $scope.today();







    BandService.getBand().then(function(data) {
        $scope.BandInfo = [];
        $log.debug("BandInfo student before : ", data);
        for (var i = 0; i < data.bandList.length; i++) {
            if (data.bandList[i].role.toLowerCase() === 'student') {
                console.log('student')
                $scope.BandInfo.push(data.bandList[i]);
            }
        }

        console.log('student band', $scope.BandInfo)
    }, function(err) {
        $log.debug(err);
    });


    // $timeout(function() {
    //     HierarchyService.getAllHierarchys().then(function(data) {
    //         $scope.HierarchyInfo = data.hierarchyList;
    //         $scope.Permission = data.modulePermission;
    //     }, function(err) {
    //         $log.debug(err);
    //     });
    // }, 2000);



    assign.getAsssignList($cookies.get("currenthierarchy")).then(function(data) {
        $log.debug("assignList : ", data);
        $scope.AssisgnInfo = data.assignList;
    }, function(err) {
        $log.debug(err);
    });









    $scope.addProfile = function(Profile) {
        $scope.profile = {};

        $location.path('registration/student/add');
    }



    $scope.editProfile = function(profile) {
        $log.debug('edit called')
        $scope.profile = profile;
        $scope.currentBand = _.find($scope.BandInfo, { bandId: $scope.profile.band });
        $scope.currentInstitution = {
            institution: $scope.profile.institution
        };
        $scope.currentSchool = {
            school: $scope.profile.school
        };
        $scope.currentDesignation = {
            designation: $scope.profile.designation
        };
        $scope.currentRole = {
            role: $scope.profile.role
        };

        $scope.currentClassName = {
            standard: $scope.profile.standard
        };
        $scope.currentSection = {
            section: $scope.profile.section
        };

        $log.debug($scope.profile.reportingManagerId)
        $scope.search.name = $scope.profile.reportingManagerId;
        $scope.profile.dob = new Date($scope.profile.dob);
        //$scope.profile.previousYear = new Date($scope.profile.previousYear);
        $location.path('/registration/student/edit');

    }

    /*
        $scope.searchs = function() {
            console.log('search called');
            console.log($scope.profile.band);
            console.log($scope.search);

            registration.searching($scope.profile.band).repSearch($scope.search, function(data) {
                $scope.repInfo = data;
                console.log(data);
            });
        }
    */


    $scope.searchs = function() {

        $log.debug('search called');
        $scope.search.band = $scope.profile.band;
        $scope.search.school = $scope.currentSchool.school;
        $log.debug($scope.search);
        registration.search.add($scope.search, function(data) {
            $log.debug(data);
            $scope.repInfo = data;
        });
    }

    $scope.editrep = function(rep) {
        $scope.search.name = rep.smartId;
        $scope.profile.reportingManagerName = rep.firstName
        $('#myModals').modal('hide')
    }


    $scope.save = function(event, registrationForm) {

        $log.debug(registrationForm)
        if (registrationForm.$valid) {
            $log.debug("valid form");
            $log.debug("$scope.currentSection : ", $scope.profile.section);
            $log.debug("$scope.currentRole, $scope.currentDesignation : ", $scope.profile.role);
            $log.debug("$scope.profile.band : ", $scope.profile.band);
            $scope.profile.role = $scope.profile.role.role;

            $scope.profile.band = $scope.profile.band.bandId;
            if ($scope.profile.role.toLowerCase() === 'student') {
                $log.debug(' teacherSmartId', $scope.profile.section.teacherSmartId)
                $log.debug(' hodSmartId', $scope.profile.section.hodSmartId)
                $scope.profile.reportingManagerId = $scope.profile.section.teacherSmartId;
                $scope.profile.counterSigningManagerId = $scope.profile.section.hodSmartId;
            } else {
                $scope.profile.reportingManagerId = $scope.search.name;
            }
            $scope.profile.standard = $scope.profile.section.standard;
            $scope.profile.section = $scope.profile.section.section;

            $scope.profile.designation = $scope.profile.designation.designation;
            $log.debug($scope.profile);

            registration.add($scope.profile, $cookies.get("currenthierarchy")).then(function(data) {

                $log.debug('addProfile in student registration:', data)
                if (data.status == 200) {
                    $scope.profiles = data;
                    getStudents($scope.pageIndex);
                    $scope.registrationFormSubmitError = data.message;
                    $location.path('/registration/student')

                } else if (data.status === 500) {
                    $scope.registrationFormSubmitError = data.message;
                    $log.debug(data.message);
                } else {
                    $scope.registrationFormSubmitError = data.message;
                }

            }, function(err) {
                $log.debug(err);
            });
        } else {
            $log.debug('error')
        }
    }

    $scope.updateStudent = function(event, registrationForm) {
        $log.debug('call')
        if (registrationForm.$valid) {
            $log.debug('UPDATE CALLED');
            $log.debug($scope.currentInstitution.institution);
            $scope.profile.reportingManagerId = $scope.search.name;
            $scope.profile.institution = $scope.currentInstitution.institution;
            $scope.profile.school = $scope.currentSchool.school;
            $scope.profile.role = $scope.currentRole.role;
            $scope.profile.band = $scope.currentBand.bandId;
            $scope.profile.designation = $scope.currentDesignation.designation;
            $scope.profile.section = $scope.currentSection.section;
            $scope.profile.standard = $scope.currentClassName.standard;
            $log.debug($scope.profile);
            registration.editProfile.update($scope.profile, function(data) {
                $log.debug(data);
                if (data.status === 200) {
                    //$scope.profiles = data;
                    getStudents($scope.pageIndex);
                    $location.path('/registration/student');
                    $scope.registrationFormMessage = data.result;

                    $log.debug('status== 200 in update ', $scope.registrationFormMessage);
                } else if (data.status === 500) {
                    $scope.registrationFormSubmitError = data.result;
                    $log.debug(data.result);
                } else {
                    $scope.registrationFormSubmitError = data.message;
                }
            }, function(err) {
                $log.debug(err);
            });
        } else {
            $log.debug('editProfile err' + registrationForm);
        }
    }

    $scope.deleteProfile = function() {
        $log.debug("Deleting profile : ", $scope.currentProfile);
        registration.deleteProfile($scope.currentProfile).then(function(data) {
            $log.debug(data);
            for (var i = 0; i < $scope.profileInfo.length; i++) {
                if ($scope.profileInfo[i].entryTime == $scope.currentProfile.entryTime) {
                    $scope.profileInfo.splice(i, 1);
                }
            }
            getStudents($scope.pageIndex);
            $scope.registrationFormMessage = data.result;
        }, function(err) {
            $log.debug(err);
        });
    }

    $scope.setCurrentProfile = function(profile) {
        $scope.currentProfile = profile;

    }

    $scope.standardfeesearch = function(standard) {
        $log.debug('profile.standard in standardfeesearch', standard.standard);
        registration.standardfeesearch(standard.standard, $cookies.get("currenthierarchy")).then(function(data) {
            $log.debug(data);
            if (data.status == 500) {
                $scope.registrationFormSubmitError = data.message;
                $log.debug(data.message);
            } else if (data.status == 200) {
                $scope.registrationFormSubmitError = false;
            }
        }, function(err) {
            $log.debug(err);
        });
    }

    $scope.imagetobase64 = function(image) {
        console.log('imagetobase64', image);
        $scope.profile.image = image.base64;

    }

    $scope.$watch('profile.emailId', function() {
        if ($scope.profile.emailId) {
            $log.debug("$scope.profile.emailId : ", $scope.profile.emailId);
            $scope.showEmailReq = false;
            $scope.showInvalidEmail = !(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test($scope.profile.emailId));
        } else {
            $scope.showInvalidEmail = false;
        }
    });

    $scope.$watch('profile.dob', function() {
        $log.debug("dob watch called : ", $scope.profile);
        var regex = /^[a-zA-Z]*$/;
        $scope.validDate = regex.test($scope.profile.dob);
        $log.debug("$scope.validDate : ", $scope.validDate);
    });

    $scope.validateDate = function() {
        $log.debug("dob watch called : ", $scope.profile);
        var regex = /^[a-zA-Z]*$/;
        $scope.validDate = regex.test($scope.profile.dob);
        $log.debug("$scope.validDate : ", $scope.validDate);
    };

    $scope.showAcademicYearList = function() {
        var year = new Date().getFullYear();
        var currYear;
        $scope.yearList = [];
        for (var i = year; i > year - 10; i--) {
            currYear = i;
            $scope.yearList.push(currYear + "-" + (currYear + 1));
        }
        $scope.profile.academicYear = $scope.yearList[0];
        $log.debug('inside academic yearList:', $scope.profile.academicYear);
    }

    $scope.showAcademicYearList();
    $scope.showPreviousYearList = function() {
        var year = new Date().getFullYear();
        var currYear;
        $scope.previousyearList = [];
        for (var i = year - 1; i > year - 10; i--) {
            currYear = i;
            $scope.previousyearList.push(currYear + "-" + (currYear + 1));
        }
        $scope.profile.previousYear = $scope.previousyearList[0];
        $log.debug('inside academic yearList:', $scope.profile.previousYear);
    }

    $scope.showPreviousYearList();
});
