angular.module('registration').controller('EmployeeCtrl', function($log, $scope, $location, $cookies, $timeout, registration, BandService, HierarchyService) {

    $scope.profile = {};

    $scope.totalEmployees = 0;
    $scope.employeesPerPage = 10;
    $scope.pageIndex = 1;
    $scope.currentPage = 1;
    $scope.pageSize = 5;
    $scope.isPageChanged = true;
    /*$scope.maxVal = 10;
    $scope.minVal = 5;
    $scope.maxSize = 5;*/

    /*Page change on click on the page number*/
    $scope.pagination = {
        current: 1
    };
    $scope.employeePageChange = function(newPage) {
        if ($scope.employeesPerPage) {
            console.log("newPage : ", newPage);
            $scope.isPageChanged = true;
            $scope.pageIndex = newPage;
            console.log("page number ", newPage);
            getEmployees(newPage);
        }
    };

    function getEmployees(pageNumber) {
        var minVal = ($scope.employeesPerPage * (pageNumber - 1));
        var maxVal = pageNumber * $scope.employeesPerPage;
        console.log("Get function called from controller")
        registration.getEmp({ "min": minVal, "max": maxVal, "hierarchy": $cookies.get("currenthierarchy") }).then(function(data) {
            console.log(data)
            $scope.profileInfo = data.result.profileList;
            $scope.Permission = $cookies.getObject("modulePermission");
            $scope.totalProfiles = data.result.totalProfiles;
            $scope.loading = false;
            console.log($scope.profileInfo);
        }, function(err) {
            console.log(err);
        });
    }
    getEmployees(1);

    $scope.size = function(data) {
        $scope.employeesPerPage = data;
    }

    $scope.search1 = function(data) {
        $scope.employeesPerPage = $scope.totalProfiles;
    }


    $scope.currentInstitution = {};
    $scope.currentSchool = {};
    $scope.currentBand = {};
    $scope.currentRole = {};
    $scope.currentDesignation = {};

    $scope.search = {};
    $scope.loading = true;

    $scope.dateformat = "dd/MM/yyyy";
    $scope.showcalendar = function($event) {
        $log.debug('inside in showcalendar');
        $scope.popup1.showdp = true;
    };


    $scope.popup1 = {
        showdp: false
    };
    // $scope.today = function () {
    //            $scope.profile.dob= new Date();
    //        };
    // $scope.today();


    //$scope.dateformat="dd/MM/yyyy";
    $scope.showcalendar1 = function($event) {
        $scope.popup2.showdp1 = true;
    };

    //$scope.showdp1 = false;
    $scope.popup2 = {
        showdp1: false
    };
    // $scope.today = function () {
    //            $scope.profile.joiningDate= new Date();
    //        };
    // $scope.today();
    BandService.getBand().then(function(data) {
        $log.debug("employee.js band data : ", data);
        $scope.BandInfo = data.bandList;
    }, function(err) {
        $log.debug("employee.js BandService error block : ", err);
    });



    // $timeout(function() {
    //     HierarchyService.getAllHierarchys().then(function(data) {
    //         $log.debug("employee.js HierarchyService successblock : ", data);
    //         $scope.HierarchyInfo = data.hierarchyList;
    //     }, function(err) {
    //         $log.debug("employee.js HierarchyService errorblock : ", err);
    //     });
    // }, 1000);


    $scope.addProfile = function(profile) {
        //$scope.profile = {};
        $scope.registrationFormSubmitError = null;
        $location.path('registration/employee/add');
    }

    $scope.editProfile = function(profile) {
        $scope.profile = profile;
        $scope.currentBand = _.find($scope.BandInfo, { bandId: $scope.profile.band });
        $scope.currentInstitution = {
            institution: $scope.profile.institution
        };
        $scope.currentSchool = {
            school: $scope.profile.school
        };
        $scope.currentDesignation = {
            designation: $scope.profile.designation
        };
        $scope.currentRole = {
            role: $scope.profile.role
        };
        $scope.search.name = $scope.profile.reportingManagerId;
        $scope.profile.dob = new Date($scope.profile.dob);
        $scope.profile.joiningDate = new Date($scope.profile.joiningDate);
        $scope.profile.passportIssueDate = new Date($scope.profile.passportIssueDate);
        $scope.profile.passportExpiryDate = new Date($scope.profile.passportExpiryDate);

        $location.path('/registration/employee/edit');
    }

    $scope.searchs = function() {

        $log.debug('search called');
        $scope.search.band = $scope.currentBand.bandId;
        $scope.search.school = $scope.currentSchool.school;
        $log.debug($scope.search);
        registration.search.add($scope.search, function(data) {
            $log.debug(data);
            $scope.repInfo = data;
        });
    }


    $scope.editrep = function(rep) {
        $scope.search.name = rep.smartId
        $scope.profile.reportingManagerName = rep.firstName
        $scope.profile.counterSigningManagerId = rep.reportingManagerId
        $scope.profile.counterSigningManagerName = rep.reportingManagerName
            // $('#myModal').modal('hide');
    }

    $scope.save = function(event, registrationForm) {
        if (registrationForm.$valid) {
            $scope.loading = true;
            $log.debug("valid form and hierarchy is : ", $scope.currentInstitution);
            //$scope.profile.image = $scope.profile.image.base64;
            $scope.profile.reportingManagerId = $scope.search.name;
            $scope.profile.institution = $scope.currentInstitution.institution;
            $scope.profile.school = $scope.currentSchool.school;
            $scope.profile.role = $scope.currentRole.role;
            $scope.profile.designation = $scope.currentRole.designation;
            $scope.profile.band = $scope.currentBand.bandId;
            $scope.profile.hierarchy = $scope.currentSchool;
            //$scope.profile.image = $scope.profile.image.base64;
            /*
                       if ($scope.profile.image.base64;) {
                            $scope.profile.image = 
                            console.log($scope.profile.image);
                       }*/

            $log.debug("$scope.profile : ", $scope.profile);
            registration.add($scope.profile, $cookies.get("currenthierarchy")).then(function(data) {
                if (data.status === 200) {
                    $scope.profiles = data;
                    $log.debug('saved succfully')
                    getEmployees($scope.pageIndex);
                    $scope.loading = false;
                    $scope.registrationFormSubmitError = data.message;
                    $scope.ShowregistrationFormSubmitError = true;
                    $timeout(function() {
                        $scope.ShowregistrationFormSubmitError = false;
                    }, 2000);
                    $location.path('/registration/employee')
                } else if (data.status === 500) {
                    $scope.loading = false;
                    $scope.registrationFormSubmitError = data.message;
                } else {
                    $scope.loading = false;
                    $scope.registrationFormSubmitError = data.message;
                }
                $scope.ShowregistrationFormSubmitError = true;
                $timeout(function() {
                    $scope.ShowregistrationFormSubmitError = false;
                }, 2000);
            }, function(err) {
                console.log('err message', err)
                $scope.registrationFormSubmitError = err.data.message;
                $scope.ShowregistrationFormSubmitError = true;
                $timeout(function() {
                    $scope.loading = false;
                    $scope.ShowregistrationFormSubmitError = false;
                }, 2000);
            });
        } else {
            $log.debug("invalid form, please fill mandatory details");
        }
    }


    $scope.update = function(event, registrationForm) {
        if (registrationForm.$valid) {
            $log.debug('UPDATE CALLED');
            $log.debug($scope.currentInstitution.institution);
            $scope.profile.reportingManagerId = $scope.search.name;
            $scope.profile.institution = $scope.currentInstitution.institution;
            $scope.profile.school = $scope.currentSchool.school;
            $scope.profile.role = $scope.currentRole.role;
            $scope.profile.band = $scope.currentBand.bandId;
            $scope.profile.designation = $scope.currentDesignation.designation;

            registration.editProfile.update($scope.profile, function(data) {
                if (data.status === 200) {
                    $scope.profiles = data;
                    $scope.registrationFormSubmitError = data.message;
                    $location.path('/registration/employee');
                } else if (data.status === 500) {
                    $scope.registrationFormSubmitError = data.message;
                    $log.debug(data.message);
                } else {
                    $scope.registrationFormSubmitError = data.message;
                }
            }, function(err) {
                $log.debug(err);
            });
        } else {
            $log.debug('editProfile err' + registrationForm);
        }
    }


    $scope.setCurrentProfile = function(profile) {
        $scope.currentProfile = profile;

    }

    $scope.deleteProfile = function() {
        $log.debug("Deleting profile : ", $scope.profileInfo);
        registration.deleteProfile($scope.currentProfile).then(function(data) {
            $log.debug(data);
            for (var i = 0; i < $scope.profileInfo.length; i++) {
                if ($scope.profileInfo[i].entryTime == $scope.currentProfile.entryTime) {
                    $scope.profileInfo.splice(i, 1);
                }
            }
            getEmployees($scope.pageIndex);
            $scope.registrationFormMessage = data.result;
        }, function(err) {
            $log.debug(err);
        });
    }

    $scope.imagetobase64 = function(image) {
        console.log('imagetobase64', image);
        $scope.profile.image = image.base64;

    }


    $scope.$watch('profile.dob', function() {
        $log.debug("dob watch called : ", $scope.profile);
        var regex = /^[a-zA-Z]*$/;
        $scope.validDate = regex.test($scope.profile.dob);
        $log.debug("$scope.validDate : ", $scope.validDate);
    });
    $scope.validateDate = function() {
        $log.debug("dob watch called : ", $scope.profile);
        var regex = /^[a-zA-Z]*$/;
        $scope.validDate = regex.test($scope.profile.dob);
        $log.debug("$scope.validDate : ", $scope.validDate);
    };
    $scope.$watch('profile.emailId', function() {
        if ($scope.profile.emailId) {
            $log.debug("$scope.profile.emailId : ", $scope.profile.emailId);
            $scope.showEmailReq = false;
            $scope.showInvalidEmail = !(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test($scope.profile.emailId));
        } else {
            $scope.showInvalidEmail = false;
        }
    });

    $scope.showAcademicYearList = function() {
        var year = new Date().getFullYear();
        var currYear;
        $scope.yearList = [];
        for (var i = year; i > year - 10; i--) {
            currYear = i;
            $scope.yearList.push(currYear + "-" + (currYear + 1));
        }
        $scope.profile.academicYear = $scope.yearList[0];
        $log.debug('inside academic yearList:', $scope.profile.academicYear);
    }
    $scope.showAcademicYearList();

});
