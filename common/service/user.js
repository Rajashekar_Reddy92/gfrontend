angular.module('common').service('User', function($log, $resource, $cookies, $rootScope, config, $q) {

    this.userInfo = null;
    this.identified = false;
    this.id = null;
    var context = this;

    var authAPI = $resource(config.baseUrl + '/login', {}, {});

    this.authenticate = function(id, password, callback) {
        $log.debug("calling authenticate function");
        //$cookies.put("token", $cookies.get('token'));
        $log.debug($cookies.get('token'))
        //$cookies.remove('token');
        authAPI.save({ smartId: id, password: password }, function(response) {
            if (response) {
                context.id = id;
                //$cookies.put('userId', id);
                //$cookies.put('password', password);
                context.userInfo = response;
                $cookies.put('token', response.token);
                $log.debug(response)
                callback(null, response);
            } else {
                callback(response)
            }
        });
    }

    this.getUserInfo = function(callback) {

        $log.debug($cookies.get('token'));

        if (this.userInfo) {
            $log.debug(this.userInfo);
            callback(this.userInfo);
        } else {
            if ($cookies.get('token')) {
                this.authenticate(null, null, function(err, data) {
                    $log.debug(data);
                    context.userInfo = data;
                    callback(data);
                });
            } else {
                callback(null);
            }
        }
    };

    /*this.getUser = function() {
        var defer = $q.defer();
        if (this.userInfo) {
            defer.resolve(this.userInfo);
        } else {
            if ($cookies.get('userId') && $cookies.get('password')) {
                this.id = $cookies.get('userId');
                this.authenticate($cookies.get('userId'), $cookies.get('password'), function(err, data) {
                    console.log(data);
                    context.userInfo = data.data;
                    defer.resolve(data);
                });
            } else {
                defer.resolve(null);
            }
        }
        return defer.promise;
    }*/

      
    var forgotPassword = $resource(config.baseUrl + '/password/email', {});

    this.sendResetLink = function(email) {
        

        return forgotPassword.save(email).$promise;
        
    };

    this.unidentify = function() {

        $cookies.remove('userId');
        $cookies.remove('password');
        this.userInfo = null;
        this.id = null;
        $log.debug("before calling the api")
        authAPI.delete({}, function(response) {
            $log.debug(response);
            $cookies.remove('token');
        });
        
        $rootScope.$broadcast('loginChanged');
    };
    
});
