angular.module('modules').controller('ModulesCtrl', function($log,  $cookies,$timeout, $scope, $location, moduleservice) {

    $scope.module = {};

    function getModules() {
        console.log('getModules')
        moduleservice.get().then(function(data) {
            $scope.ModInfo = data.modulesList;
            $scope.Permission = $cookies.getObject("modulePermission");
        }, function(err) {
            $log.debug(err);
        });
    }
    getModules();


    $scope.addModules = function() {
        $scope.module = {};
        $scope.addModuleFormSubmitError = null;

        $location.path('modules/add');
    }

    $scope.save = function(event, form) {
        event.preventDefault();

        if (form.$valid) {
            moduleservice.save($scope.module).then(function(data) {
                if (data.status == 200) {
                    $log.debug(data);
                    $scope.module = {};

                    $scope.addModuleFormSubmitError = data.message;
                    $scope.ShowModuleFormMessage = true;
                    $timeout(function() {
                        $scope.ShowModuleFormMessage = false;
                    }, 2000);

                    getModules();
                    $location.path('/modules')
                } else if (data.status == 400) {
                    $scope.addModuleFormSubmitError = data.message;

                }

            }, function(err) {
                $scope.addModuleFormSubmitError = err.data.message;
                $log.debug('Error while adding band :', err);
            })
        } else {
            angular.forEach(form.$error.required, function(field) {
                field.$setDirty();
            });
        }
    }



    $scope.editmodule = function(module) {
        $scope.module = angular.extend({}, module);
        console.log($scope.module)
        $scope.editModuleFormSubmitError = null;

        $location.path('modules/edit');
    }

    $scope.cancelmodule = function() {

        $location.path('/modules');
    }
    $scope.update = function(event, form) {
        event.preventDefault();

        if (form.$valid) {

            moduleservice.update($scope.module).then(function(data) {
                console.log(data);
                if (data.status == 200) {
                    $scope.module = {};
                    $scope.ModuleFormSubmitMessage = data.message;
                    $scope.ShowModuleFormMessage = true;
                    $timeout(function() {
                        $scope.ShowModuleFormMessage = false;
                    }, 2000);
                    getModules();
                    $location.path('modules');

                } else if (data.status == 400) {
                    $scope.editModuleFormSubmitError = data.message;

                } else $scope.editModuleFormSubmitError = data.message;

            }, function(err) {
                $log.debug('Error while updating module :', err);
            })
        } else {
            angular.forEach(form.$error.required, function(field) {
                field.$setDirty();
            });
        }
    }

    $scope.delete = function() {

        moduleservice.delete($scope.currentModule).then(function(data) {

            $scope.ModuleFormSubmitMessage = data.message;
            $scope.ShowModuleFormMessage = true;
            /* $timeout(function() {
                 $scope.ShowModuleFormMessage = false;
             }, 2000);*/
            getModules();
            $location.path('modules');

            $scope.currentModule = null;

        }, function(err) {
            $log.debug('Error while deleting record :', err);
        })
    }

    $scope.setCurrentModule = function(module) {
        $scope.currentModule = module;
    }

});
