angular.module('modules', ['appConfig','ui.bootstrap','ui.utils','ui.router','ngAnimate']);

angular.module('modules').config(function($stateProvider) {

    /* Add New States Above */
     $stateProvider.state('modules', {
        url: '/modules',
        parent: 'app',
        templateUrl: 'modules/partial/modules.html',
        controller: 'ModulesCtrl'
    })
        .state('modules.add', {
            url: '/add',
            templateUrl: 'modules/partial/add-modules.html'
        })
       .state('modules.edit', {
            url: '/edit',
            templateUrl: 'modules/partial/edit-modules.html'
        })

});

