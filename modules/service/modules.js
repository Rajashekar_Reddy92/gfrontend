angular.module('modules').service('moduleservice',function($resource, config) {

    var modules = $resource(config.baseUrl +'/modules/:ts' , {ts: '@ts'}, {
         'update': { method: 'PUT' }
     });
 
    this.get= function () {
        return modules.get().$promise;
    }
    this.save = function (data) {
    	console.log(data);
        return modules.save(data).$promise;
    }
     this.update = function (data) {
        console.log(data);
        return modules.update({ts: "edit"}, data).$promise;
    }

    this.delete = function (data) {
        return modules.update({ts: "delete"}, data).$promise;
    }
});