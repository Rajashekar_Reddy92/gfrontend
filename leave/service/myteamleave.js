angular.module('leave').service('myteamleaveService', function($resource, config) {

    var myteamleave = $resource(config.baseUrl + '/myteamleave/:task/', { task: '@task' }, {
        'update': { method: 'PUT' }
    });

    var getLeaves = $resource(config.baseUrl + '/myteamleave/:min/:max/:hierarchy', { min: '@min', max: '@max', hierarchy: '@hierarchy' }, {
        'update': { method: 'PUT' }
    });

    this.get = function(params) {
        return getLeaves.get(params).$promise;
    }

    this.update = function(data) {
        console.log('service of my team leave', data)
        return myteamleave.update({ task: "sanction" }, data).$promise;
    }

    this.delete = function(data) {
        return myteamleave.update({ task: "reject" }, data).$promise;
    }
    this.reject = function(data) {
        return myteamleave.update({ task: "cancelSanction" }, data).$promise;
    }

});
