angular.module('leave').service('LeaveService', function($resource, config) {
    var leave = $resource(config.baseUrl + '/leave/:ts/', { ts: '@task' }, {
        'update': { method: 'PUT' }
    });

    var getLeaves = $resource(config.baseUrl + '/leave/:min/:max', { min: '@min', max: '@max' }, {
        'update': { method: 'PUT' }
    });

    this.get = function(params) {
        return getLeaves.get(params).$promise;
    }


    this.getLeaveList = function(smartId, leaveType) {
        var params = {
            smartId: smartId,
            leaveType: leaveType
        };

        var Leftleave = $resource(config.baseUrl + '/leave/leftleaves' + '/' + smartId + '/' + leaveType, {});

        console.log('left laeves ', smartId, leaveType)
        return Leftleave.get().$promise;
    }

    this.save = function(data) {
        return leave.save(data).$promise;
    }

    /*this.update = function(data) {
    return leave.update({ ts: "edit" }, data).$promise;
}
*/

    this.delete = function(data) {
        console.log("delete service", data);
        return leave.update({ ts: "delete" }, data).$promise;
    }

    var leavemaster = $resource(config.baseUrl + '/leavemaster/:task/', { task: '@task' }, {
        'update': { method: 'PUT' }
    });

    this.getLeaveMasterList = function(params) {
        return leavemaster.get().$promise;
    }



});
