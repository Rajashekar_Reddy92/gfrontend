angular.module('leave', ['appConfig', 'ui.bootstrap', 'ui.router', 'ngAnimate', 'ui.utils', 'ui.router', 'utils']);

angular.module('leave').config(function($stateProvider) {

    $stateProvider.state('leave', {
            url: '/leave',
            parent: 'app',
            templateUrl: 'leave/partial/home.html',
            controller: 'hierarchyDecideCtrl',
            resolve: {
                template: function($q, User) {
                    var defer = $q.defer();
                    User.getUserInfo(function(user) {
                        var template = null;

                        if (user.profile.role.toLowerCase() === 'admin' || user.profile.role.toLowerCase() === 'director') {
                            console.log('user is admin')
                            template = 'leave/partial/landing.html';
                        } else {
                            template = 'leave/partial/home2.html';
                        }

                        defer.resolve(template);
                    });
                    return defer.promise;
                }
            }



        })
        .state('leave.decider', {
            url: '/decider',
            templateUrl: 'leave/partial/home2.html'

        })
        .state('leave.view', {
            url: '/view',
            templateUrl: 'leave/partial/leave.html',
            controller: 'LeaveCtrl'
        })

    .state('leave.view.add', {
            url: '/add',
            templateUrl: 'leave/partial/add-leave.html'
        })
        .state('leave.view.edit', {
            url: '/edit',
            templateUrl: 'leave/partial/edit-leave.html'
        })
        .state('leave.myteamleave', {
            url: '/myteamleave',
            templateUrl: 'leave/partial/myteamleave/myteamleave.html',
            controller: 'MyteamleaveCtrl'
        })

});
