angular.module('leave').controller('MyteamleaveCtrl', function($log, $scope, $state, $cookies, $location, myteamleaveService, utils) {
    $scope.leave = {};

    $scope.totalTeamLeave = 0;
    $scope.teamLeavePerPage = 10;
    $scope.pageIndex = 1;
    $scope.currentPage = 1;
    $scope.pageSize = 5;
    $scope.isPageChanged = false;
    $scope.maxVal = 10;
    $scope.minVal = 5;
    $scope.maxSize = 5;
    $scope.loading = true;

    /*Page change on click on the page number*/
    $scope.pagination = {
        current: 1
    };
    $scope.teamLeavePageChange = function(newPage) {
        console.log("newPage : ", newPage);
        $scope.isPageChanged = true;
        $scope.pageIndex = newPage;
        console.log("page number ", newPage);

        getMyTeamLeaves(newPage);
    };

    function getMyTeamLeaves(pageNumber) {
        var minVal = ($scope.teamLeavePerPage * (pageNumber - 1));
        var maxVal = pageNumber * $scope.teamLeavePerPage;
        console.log("Get function called from controller")
        $scope.loading = true;
        myteamleaveService.get({ "min": minVal, "max": maxVal, "hierarchy": $cookies.get("currenthierarchy") }).then(function(data) {


            console.log("Get function called from controller")
            $scope.LeaveInfo1 = data.myTeamList.myTeamLeaveList;
            $scope.Permission = $cookies.getObject("modulePermission");
            $scope.LeaveInfo1.forEach(function(leave) {
                leave.displaystartDate = utils.getDateString(leave.startDate);
                leave.displayendDate = utils.getDateString(leave.endDate);
                leave.startDate = new Date(leave.startDate);
                leave.endDate = new Date(leave.endDate);
            })

            $scope.totalTeamLeave = data.myTeamList.totalListCount;
            $scope.loading = false;
        }, function(err) {

            console.log(err);
        });
    }

    getMyTeamLeaves(1);

    $scope.size = function(data){
        $scope.teamLeavePerPage = data;
    }

    $scope.search1 = function(data){

        $scope.teamLeavePerPage = $scope.totalTeamLeave
    }

    $scope.view = 'list';

    $scope.today1 = function() {
        $scope.leave.startDate = new Date();
    };
    $scope.today2 = function() {
        $scope.leave.endDate = new Date();
    };

    $scope.mindate = new Date();

    $scope.disabled = function(date, mode) {
        return (mode === 'day' && (date.getDay() === 0));
    };
    $scope.dateformat = "dd/MM/yyyy";
    $scope.today1();
    $scope.today2();

    $scope.showcalendar1 = function($event) {
        $scope.showdp1 = true;
    };
    $scope.showdp1 = false;

    $scope.showcalendar2 = function($event) {
        $scope.showdp2 = true;
    };
    $scope.showdp2 = false;


    $scope.delete = function() {
        var leave = angular.extend({}, $scope.currentLeave);
        delete leave.displayendDate;
        delete leave.displaystartDate;
        $scope.loading = true;
        /* leave.startDate = leave.startDate.toString();
              leave.endDate = leave.endDate.toString();
*/
        myteamleaveService.delete(leave).then(function(data) {
            getMyTeamLeaves($scope.pageIndex);

            $scope.currentLeave = null;
            // $('#deleteConfirmationModal').modal('hide');
            $state.go('leave.myteamleave');
            $scope.loading = false;
        }, function(err) {
            $log.debug('Error while deleting record :', err);
        });
    }


    $scope.setCurrentLeave = function(leave, index) {
        $scope.currentLeave = leave;
        $scope.currentIndex = index;
    }

    $scope.update = function() {
        var leave = angular.extend({}, $scope.currentLeave);
        delete leave.displayendDate;
        delete leave.displaystartDate;
        $scope.loading = true;
        /*leave.startDate = leave.startDate;
leave.endDate = leave.endDate;
*/
        $log.debug('update method in myteam leave', leave)
        myteamleaveService.update(leave).then(function(data) {
            var showdisabled = "isDisabled_" + $scope.currentIndex;
            $scope[showdisabled] = true;
            getMyTeamLeaves($scope.pageIndex);
            $state.go('leave.myteamleave');
            $scope.currentLeave = null;
            $scope.loading = false;

            //   $('#sanctionConfirmationModal').modal('hide');
        }, function(err) {
            $log.debug('Error while sanctioning record :', err);
        });
    }

    $scope.reject = function() {
        var leave = angular.extend({}, $scope.currentLeave);
        delete leave.displayendDate;
        delete leave.displaystartDate;
        $scope.loading = true;
        /*leave.startDate = leave.startDate.toString();
        leave.endDate = leave.endDate.toString();*/

        myteamleaveService.reject(leave).then(function(data) {
            var showdisabled = "isDisabled_" + $scope.currentIndex;
            if ($scope[showdisabled]) $scope[showdisabled] = false;
            getMyTeamLeaves($scope.pageIndex);
            $state.go('leave.myteamleave');
            $scope.currentLeave = null;
            $scope.loading = false;

            // $('#rejectConfirmationModal').modal('hide');
        }, function(err) {
            $log.debug('Error while deleting record :', err);
        });
    }




});
