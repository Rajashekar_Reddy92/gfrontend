angular.module('leave').controller('LeaveCtrl', function($scope, $cookies, User, $timeout, $state, $log, template, User, $location, LeaveMasterService, LeaveService, utils) {

    $scope.leave = {};
    $scope.template = template;
    // $scope.view = 'list';

    $scope.totalLeave = 0;
    $scope.myLeavePerPage = 10;
    $scope.pageIndex = 1;
    $scope.currentPage = 1;
    $scope.pageSize = 5;
    $scope.isPageChanged = false;
    $scope.maxVal = 10;
    $scope.minVal = 5;
    $scope.maxSize = 5;

    /*Page change on click on the page number*/
    $scope.pagination = {
        current: 1
    };
    $scope.myLeavePageChange = function(newPage) {
        console.log("newPage : ", newPage);
        $scope.isPageChanged = true;
        $scope.pageIndex = newPage;
        console.log("page number ", newPage);
        getLeaves(newPage);
    };

    function getLeaves(pageNumber) {
        var minVal = ($scope.myLeavePerPage * (pageNumber - 1));
        var maxVal = pageNumber * $scope.myLeavePerPage;
        $scope.loading = true;
        console.log("Get function called from controller")
        LeaveService.get({ "min": minVal, "max": maxVal }).then(function(data) {
                $log.debug("getting data", data);
                $scope.LeaveInfo = data.leaveList.leave;

                $scope.Permission = $cookies.getObject("modulePermission");

                $log.debug('$scope.LeaveInfo in LeaveCtrl', $scope.LeaveInfo)
                $log.debug('leave Permission info', $scope.Permission);
                $scope.LeaveInfo.forEach(function(leave) {

                    leave.displaystartDate = utils.getDateString(leave.startDate);
                    leave.displayendDate = utils.getDateString(leave.endDate);
                    leave.startDate = new Date(leave.startDate);
                    leave.endDate = new Date(leave.endDate);
                })
                $scope.totalLeave = data.leaveList.totalleavelist;
                $scope.loading = false;
            },

            function(err) {
                $log.debug(err);
            });
    }

    getLeaves(1);

    $scope.size = function(data){
        $scope.myLeavePerPage = data;
    }

    $scope.searchs = function(data){
        $scope.myLeavePerPage = $scope.totalLeave
    }

    $scope.today1 = function() {
        $scope.leave.startDate = new Date();
    };
    $scope.today2 = function() {
        $scope.leave.endDate = new Date();
    };

    $scope.mindate = new Date();

    $scope.disabled = function(date, mode) {
        return (mode === 'day' && (date.getDay() === 0));
    };
    $scope.dateformat = "dd/MM/yyyy";
    $scope.today1();
    $scope.today2();

    $scope.showcalendar1 = function($event) {
        $scope.showdp1.startdate = true;
    };
    $scope.showdp1 = {
        startdate: false
    };

    $scope.showcalendar2 = function($event) {
        $scope.showdp2.enddate = true;
    };
    $scope.showdp2 = {
        enddate: false
    };

    $scope.getLeftLeaves = function(leaveType) {

        LeaveService.getLeaveList(User.id, leaveType).then(function(data) {
            $log.debug('data of leftleave', data)
            $scope.leftLeaveInfo = data;
            if (data.leftLeaves > 0) {
                $scope.leftLeaves = data.leftLeaves;

            } else {
                $scope.leftLeaves = '0';
            }

        }, function(err) {
            $log.debug(err);
        });


    }
    $timeout(function() {
        function getLeaveMaster() {
            LeaveMasterService.getLeaveMasterList().then(function(data) {
                $scope.LeaveMasterInfo = data.leaveMasterList;
                console.log("leaveMasterList information", $scope.LeaveMasterInfo);
            }, function(err) {
                $log.debug(err);
            });
        }

        getLeaveMaster();
    }, 1000);




    $scope.addLeave = function() {

        $scope.leave = {};
        $scope.addLeaveFormSubmitError = null;
        $scope.leftLeaves = null;
        $location.path('leave/view/add');
    }




    $scope.delete = function() {
        $log.debug('in side delete ');
        var leave = angular.extend({}, $scope.currentLeave);
        delete leave.displayendDate;
        delete leave.displaystartDate;
        leave.startDate = leave.startDate;
        leave.endDate = leave.endDate;

        LeaveService.delete(leave).then(function(data) {
            getLeaves($scope.pageIndex);
            $state.go("leave.view");
            $scope.currentLeave = null;
            // $('#deleteConfirmationModal').modal('hide');
        }, function(err) {
            $log.debug('Error while deleting record :', err);
        });
    }


    $scope.setCurrentLeave = function(leave) {
        $log.debug("setCurrentLeave", leave);
        $scope.currentLeave = leave;
    }



    $scope.save = function(event, form) {
        event.preventDefault();
        if (form.$valid) {
            var leave = angular.extend({}, $scope.leave);
            delete leave.displayendDate;
            delete leave.displaystartDate;
            leave.startDate = leave.startDate;
            leave.endDate = leave.endDate;
            $scope.loading = true;
            LeaveService.save($scope.leave).then(function(data) {
                if (data.message == "success") {
                    $scope.leave = {};
                    getLeaves($scope.pageIndex);
                    $state.go("leave.view");
                    $scope.loading = false;
                } else $scope.addLeaveFormSubmitError = data.message;
            }, function(err) {
                $scope.addLeaveFormSubmitError = err.data.message;
                $log.debug('Error while adding leave :', err);
            })
        } else {
            angular.forEach(form.$error.required, function(field) {
                field.$setDirty();
            });
        }
    }

    $scope.update = function(event, form) {
        event.preventDefault();

        if (form.$valid) {
            var leave = angular.extend({}, $scope.leave);
            delete leave.displayendDate;
            delete leave.displaystartDate;
            leave.startDate = utils.getDateString(leave.startDate);
            leave.endDate = utils.getDateString(leave.endDate);
            $scope.loading = true;
            LeaveService.update(leave).then(function(data) {
                $scope.leave = {};
                /*  getLeaves();*/
                $state.go('leave.view');
                $scope.loading = false;
            }, function(err) {
                $scope.editLeaveFormSubmitError = err.data.message;
                $log.debug('Error while updating leave :', err);
            })
        } else {
            angular.forEach(form.$error.required, function(field) {
                field.$setDirty();
            });
        }
    }




});
