angular.module('leave').controller('hierarchyDecideCtrl', function($scope, template, User, HierarchyService, $log, $state, $cookies) {
    $scope.template = template;
    $scope.fee = {};

    $scope.user = User.userInfo.profile.role;
    console.log($scope.user)

    function getHierarchys() {
        HierarchyService.get().then(function(data) {
            $scope.HierarchyInfo = data.hierarchyList;
            $scope.Permission = data.modulePermission;
            console.log(data)
            console.log($scope.HierarchyInfo)
        }, function(err) {
            console.log(err);
        });
    }
    getHierarchys();
    if (User.userInfo.profile.role.toLowerCase() === "admin" || User.userInfo.profile.role.toLowerCase() === "director") {


    } else {
        $cookies.put("currenthierarchy", angular.toJson(0));

    }



    $scope.getLeaveByHierarchy = function(hierarchy) {
        console.log("selected hierarchy is : ", hierarchy);
        $cookies.put("currenthierarchy", angular.toJson(hierarchy.hid));
        $state.go('leave.decider');
    }



});
